@extends('layouts.app') @section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Dashboard</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="#">Dashboard</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        @if(Auth::check()) @if(Auth::user()->role=='admin_tpu')

        <div class="mt-10" hidden>
            <label for=""  style="color: black">ID Pemakaman</label>
            <input type="text" id="pemakaman_id" name="pemakaman_id" value="{{Auth::user()->pemakaman_id}}" disabled hidden>
        </div>

            <div class="col-sm-12 mb-4">
                <div class="card-group">
                    <div class="card col-md-3 no-padding no-shadow" style="margin-right: 20px">
                        <div class="card-body bg-flat-color-3">
                            <div class="h1 text-right mb-4">
                                <i class="fa fa-clock-o text-light"></i>
                            </div>
                            <div class="h4 mb-0 text-light">
                                <span class="count" id="jumlah_pemakaman"></span>
                            </div>
                            <small class="text-light text-uppercase font-weight-bold">Jumlah Pemakaman Hari Ini</small>
                            <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                        </div>
                    </div>
                    <div class="card col-md-3 no-padding no-shadow" style="margin-right: 20px">
                        <div class="card-body bg-flat-color-5">
                            <div class="h1 text-right text-light mb-4">
                                <i class="fa fa-file-text"></i>
                            </div>
                            <div class="h4 mb-0 text-light">
                                <span class="count" id="jumlah_kadaluarsa"></span>
                            </div>
                            <small class="text-uppercase font-weight-bold text-light">Jumlah Izin Kadaluarsa</small>
                            <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                        </div>
                    </div>
                    <div class="card col-md-3 no-padding no-shadow" style="margin-right: 10px">
                        <div class="card-body bg-flat-color-4">
                            <div class="h1 text-light text-right mb-4">
                                <i class="fa fa-edit"></i>
                            </div>
                            <div class="h4 mb-0 text-light">
                                <span class="count" id="jumlah_pemeriksaan"></span>
                            </div>
                            <small class="text-light text-uppercase font-weight-bold">Menunggu Pemeriksaan</small>
                            <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                        </div>
                    </div>
                    <div class="card col-md-3 no-padding no-shadow" style="margin-right: 20px">
                        <div class="card-body bg-flat-color-1">
                            <div class="h1 text-light text-right mb-4">
                                <i class="fa fa-times"></i>
                            </div>
                            <div class="h4 mb-0 text-light">
                                <span class="count" id="jumlah_ditolak"></span>
                            </div>
                            <small class="text-light text-uppercase font-weight-bold">Jumlah Izin Ditolak</small>
                            <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                        <h4 class="mb-3">Status Permohonan </h4>
                        <canvas id="myChart"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                        <h4 class="mb-3">Status Permohonan </h4>
                        <canvas id="myChart2"></canvas>
                    </div>
                </div>
            </div>
        @elseif(Auth::user()->role=='admin_dinas')
        <div class="mt-10" hidden>
            <label for=""  style="color: black">Role Admin</label>
            <input type="text" id="admin_role" name="admin_role" value="{{Auth::user()->role}}" disabled hidden>
        </div>

        <div class="col-sm-12 mb-4">
            <div class="card-group">
                <div class="card col-md-4 no-padding no-shadow" style="margin-right: 20px">
                    <div class="card-body bg-flat-color-3">
                        <div class="h1 text-right mb-4">
                            <i class="fa fa-clock-o text-light"></i>
                        </div>
                        <div class="h4 mb-0 text-light">
                            <span class="count" id="jumlah_tpu"></span>
                        </div>
                        <small class="text-light text-uppercase font-weight-bold">Jumlah TPU</small>
                        <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                    </div>
                </div>
                <div class="card col-md-4 no-padding no-shadow" style="margin-right: 20px">
                    <div class="card-body bg-flat-color-5">
                        <div class="h1 text-right text-light mb-4">
                            <i class="fa fa-file-text"></i>
                        </div>
                        <div class="h4 mb-0 text-light">
                            <span class="count" id="jumlah_permohonan"></span>
                        </div>
                        <small class="text-uppercase font-weight-bold text-light">Jumlah Permohonan</small>
                        <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                    </div>
                </div>
                <div class="card col-md-4 no-padding no-shadow" style="margin-right: 10px">
                    <div class="card-body bg-flat-color-4">
                        <div class="h1 text-light text-right mb-4">
                            <i class="fa fa-edit"></i>
                        </div>
                        <div class="h4 mb-0 text-light">
                            <span class="count" id="jumlah_menunggu"></span>
                        </div>
                        <small class="text-light text-uppercase font-weight-bold">Menunggu Pemeriksaan</small>
                        <div class="progress progress-xs mt-3 mb-0 bg-light" style="width: 40%; height: 5px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                    <h4 class="mb-3">Status Permohonan dalam Bulan</h4>
                    <canvas id="myChart3"></canvas>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card">
                <div class="card-body"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                    <h4 class="mb-3">Status Permohonan </h4>
                    <canvas id="myChart4"></canvas>
                </div>
            </div>
        </div>
        @else
            <div class="col-md-12">
                <div class="col-sm-6 col-lg-3">
                    <div class="card text-white bg-flat-color-1">
                        <div class="card-body pb-0">
                            <div class="dropdown float-right">
                                <button class="btn bg-transparent dropdown-toggle theme-toggle text-light" type="button" id="dropdownMenuButton1" data-toggle="dropdown">
                                    <i class="fa fa-cog"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                    <div class="dropdown-menu-content">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </div>
                            </div>
                            <h4 class="mb-0">
                                <span class="count">10468</span>
                            </h4>
                            <p class="text-light">Members online</p>

                            <div class="chart-wrapper px-0" style="height:70px;" height="70">
                                <canvas id="widgetChart1"></canvas>
                            </div>

                        </div>

                    </div>
                </div>
                <!--/.col-->

                <div class="col-sm-6 col-lg-3">
                    <div class="card text-white bg-flat-color-2">
                        <div class="card-body pb-0">
                            <div class="dropdown float-right">
                                <button class="btn bg-transparent dropdown-toggle theme-toggle text-light" type="button" id="dropdownMenuButton2" data-toggle="dropdown">
                                    <i class="fa fa-cog"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton2">
                                    <div class="dropdown-menu-content">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </div>
                            </div>
                            <h4 class="mb-0">
                                <span class="count">10468</span>
                            </h4>
                            <p class="text-light">Members online</p>

                            <div class="chart-wrapper px-0" style="height:70px;" height="70">
                                <canvas id="widgetChart2"></canvas>
                            </div>

                        </div>
                    </div>
                </div>
                <!--/.col-->

                <!--/.col-->

                <div class="col-sm-6 col-lg-3">
                    <div class="card text-white bg-flat-color-4">
                        <div class="card-body pb-0">
                            <div class="dropdown float-right">
                                <button class="btn bg-transparent dropdown-toggle theme-toggle text-light" type="button" id="dropdownMenuButton4" data-toggle="dropdown">
                                    <i class="fa fa-cog"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton4">
                                    <div class="dropdown-menu-content">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </div>
                            </div>
                            <h4 class="mb-0">
                                <span class="count">10468</span>
                            </h4>
                            <p class="text-light">Members online</p>

                            <div class="chart-wrapper px-3" style="height:70px;" height="70">
                                <canvas id="widgetChart4"></canvas>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6">

                </div>
                <!-- /# column -->
            </div>
        @endif @else
            <hr style="border: solid">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="card-header">
                        <strong>Tata Cara Pemesanan</strong> pemakaman
                    </div>
                    <div class="card-body card-block">
                        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col col-md-12">
                                    {{--<img src="/images/pemakaman/1602.jpg" alt="gfriend">--}}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card-header">
                        <strong>Persyaratan</strong> pemakaman
                    </div>
                    <div class="card-body card-block">
                        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">

                        </form>
                    </div>
                </div>
            </div>
    @endif
    <!--/.col-->

    </div>
    <!-- .content -->

@endsection @section('script')
    <script>
        $(function getCountDataTPU(){
            if($('#pemakaman_id').length){
                var idTPU = document.getElementById("pemakaman_id").value;
                var urlReq = "/json/getCountData?idTPU="+idTPU;

                $.ajax({
                    'url': urlReq,
                    'type':'GET',
                    'dataType': 'json',
                    success:function(result){
                        var stringified = JSON.stringify(result);
                        var parsedObj = JSON.parse(stringified);
                        //console.log(parsedObj);

                        $("#jumlah_pemakaman").text(parsedObj.jumlahPemakaman);
                        $("#jumlah_kadaluarsa").text(parsedObj.makamKadaluarsa);
                        $("#jumlah_pemeriksaan").text(parsedObj.pemeriksaan);
                        $("#jumlah_ditolak").text(parsedObj.penolakan);

                    }
                });
            }
        });

        $(function getCountDataAdminDinas(){

            if($('#admin_role').length){
                var role = document.getElementById("admin_role").value;
                var urlReq = "/json/getCountData?idTPU=0&roleAdmin="+role;

                $.ajax({
                    'url': urlReq,
                    'type':'GET',
                    'dataType': 'json',
                    success:function(result){
                        var stringified = JSON.stringify(result);
                        var parsedObj = JSON.parse(stringified);
                        //console.log(parsedObj);

                        $("#jumlah_tpu").text(parsedObj.jumlahTPU);
                        $("#jumlah_permohonan").text(parsedObj.pemesanan);
                        $("#jumlah_menunggu").text(parsedObj.permohonan);

                        if($('#myChart3').length && $('#myChart4').length){
                            // Bar Chart
                            var ctx = document.getElementById('myChart3').getContext('2d');
                            var chart = new Chart(ctx, {
                                // The type of chart we want to create
                                type: 'bar',

                                // The data for our dataset
                                data: parsedObj.barChart,

                                // Configuration options go here
                                options: {}
                            });

                            // Line Chart
                            var ctx2 = document.getElementById('myChart4').getContext('2d');
                            var chart2 = new Chart(ctx2, {
                                // The type of chart we want to create
                                type: 'line',

                                // The data for our dataset
                                data: parsedObj.lineChart,

                                // Configuration options go here
                                options: {}
                            });
                        }

                    }
                });
            }
        });


        $(function() {
            $.ajax({
                success: function (res) {
                    console.log(res);

                    if($('#myChart').length && $('#myChart2').length){
                        // Bar Chart
                        var ctx = document.getElementById('myChart').getContext('2d');
                        var chart = new Chart(ctx, {
                            // The type of chart we want to create
                            type: 'bar',

                            // The data for our dataset
                            data: res.data.barChart,

                            // Configuration options go here
                            options: {}
                        });

                        // Line Chart
                        var ctx2 = document.getElementById('myChart2').getContext('2d');
                        var chart2 = new Chart(ctx2, {
                            // The type of chart we want to create
                            type: 'line',

                            // The data for our dataset
                            data: res.data.lineChart,

                            // Configuration options go here
                            options: {}
                        });
                    }
                }
            });
        });
    </script>
@endsection
