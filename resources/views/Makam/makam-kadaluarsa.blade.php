@extends('layouts.app')
@section('content')

    <div class="">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Makam Kadaluarsa</strong>
                        </div>
                        <div class="card-body">
                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Nama Almarhum</th>
                                    <th>Nama Ahliwaris</th>
                                    <th>Lokasi Pemakaman</th>
                                    <th>Tanggal Kadaluarsa</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($makam as $m)
                                <tr>
                                        <td>{{ optional($m)->nama_almarhum }}</td>
                                        <td>{{ optional($m)->nama_ahliwaris }}</td>
                                        <td>{{ optional($m)->alamat_pemakaman }}</td>
                                        <td>{{ date('d/m/Y', strtotime($m->masa_berlaku)) }}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
