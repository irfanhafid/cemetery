<div id="main-menu" class="main-menu collapse navbar-collapse">
    <ul class="nav navbar-nav">

        @php $role = Auth::user()->role @endphp
        @if(Auth::check())
            @if($role =='admin_dinas')
                {{--<li class="sub-menu children">
                    <a href="/pendaftaranPemakaman"> <i class="menu-icon fa fa-laptop"></i>Tambah TPU dan PIC TPU </a>
                </li>--}}
                <li class="active">
                    <a href="/"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                </li>
                <li class="sub-menu children">
                    <a href="/pemakaman"> <i class="menu-icon fa fa-gears"></i>Pengelolaan TPU</a>
                </li>
                <li class="sub-menu children">
                    <a href="/user/list"> <i class="menu-icon fa fa-user"></i>Pengelolaan User</a>
                </li>
                <li class="sub-menu children">
                    <a href="/jadwal-pemakaman"> <i class="menu-icon fa fa-calendar"></i>Jadwal Pemakaman</a>
                </li>

            @elseif($role == 'admin_tpu')
                <li class="active">
                    <a href="/"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-file-text"></i>Cetak Surat Pengantar</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li>
                            <i class="fa fa-file-pdf-o"></i>
                            <a href="/IPTM/perpanjangan">IPTM Perpanjangan</a>
                        </li>
                        <li>
                            <i class="fa fa-file-pdf-o"></i>
                            <a href="/IPTM/tumpangan">IPTM Tumpangan</a>
                        </li>
                        <li>
                            <i class="fa fa-file"></i>
                            <a href="/IPTM/riwayat">Riwayat Cetak Surat</a>
                        </li>
                    </ul>
                </li>
                <li class="sub-menu children">

                    <!--<a href="/pemakaman"> <i class="menu-icon fa fa-info"></i>Manage Pemakaman</a>-->
                    <a href="/pemakaman/editpemakaman/{{Auth::user()->pemakaman_id}}"> <i class="menu-icon fa fa-info"></i>Manage Pemakaman</a>

                </li>
                <li class="sub-menu children">
                    <a href="/pemakaman/expired"> <i class="menu-icon fa fa-calendar"></i>Makam Kadaluarsa</a>
                </li>
                <li class="sub-menu children">
                    {{--<a href="/pemakaman/jadwal"> <i class="menu-icon fa fa-calendar"></i>Jadwal Pemakaman</a>--}}
                    <a href="{{ url('jadwal-pemakaman') }}"> <i class="menu-icon fa fa-calendar"></i>Jadwal Pemakaman</a>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-check"></i>Semua Permohonan</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li>
                            <i class="fa fa-file-pdf-o"></i>
                            <a href="/pemakaman/pesanan/perpanjangan">Perpanjangan</a>
                        </li>
                        <li>
                            <i class="fa fa-file-pdf-o"></i>
                            <a href="/pemakaman/pesanan/tumpangan">Tumpangan</a>
                        </li>
                        <li>
                            <i class="fa fa-file"></i>
                            <a href="/pemakaman/pesanan/riwayat">Riwayat</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-folder"></i>Import Data</a>
                    <ul class="sub-menu children dropdown-menu">
                        <li>
                            <i class="fa fa-files-o"></i>
                            <a href="/import/makam">Makam</a>
                        </li>
                        <li>
                            <i class="fa fa-files-o"></i>
                            <a href="/import/ahliwaris">Ahli Waris</a>
                        </li>
                        <li>
                            <i class="fa fa-files-o"></i>
                            <a href="/import/iptm">IPTM</a>
                        </li>
                        <li>
                            <i class="fa fa-files-o"></i>
                            <a href="/import/almarhum">Almarhum</a>
                        </li>
                    </ul>
                </li>

            @elseif(Auth::user()->role=='Member')

                <li class="sub-menu children">
                    <a href="/pemakaman/cari"> <i class="menu-icon fa fa-file"></i>Pemesanan IPTM</a>
                </li>
                <li class="sub-menu">
                    <a href="/pemakaman/pesanan"> <i class="menu-icon fa fa-shopping-cart"></i>Status Pemesanan</a>
                </li>
                <li class="sub-menu">
                    <a href="/pemakaman/pesanan/riwayat"><i class="menu-icon fa fa-shopping-cart"></i>Riwayat Pemesanan</a>
                </li>
                <li class="sub-menu">
                    <a href="/pemakaman/jadwal"><i class="menu-icon fa fa-calendar"></i>Jadwal Pemakaman</a>
                </li>
            @endif
        @endif
    </ul>
</div>
<!-- /.navbar-collapse -->
