@php
    $tumpangan = $data;
@endphp

@extends('layouts.app') @section('content')

    <div class="">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Riwayat Cetak Surat</strong>
                        </div>
                        <div class="card-body">
                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Nomor Surat</th>
                                    <th>Tanggal Surat</th>
                                    <th>Jenis IPTM</th>
                                    <th>Nama TPU</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $d)
                                    <tr>
                                        <td>{{ $d->nomor_surat }}</td>
                                        <td>{{ $d->tanggal_surat }}</td>
                                        <td>{{ ucwords($d->tipe_pesanan) }}</td>
                                        <td>{{ $d->nama_pemakaman }}</td>
                                        <td>
                                            <a class="btn btn-danger" href="{{ url("pemakaman/pesanan/$d->tipe_pesanan/$d->id/detail") }}"><i class="fa fa-print"></i> Cetak</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
