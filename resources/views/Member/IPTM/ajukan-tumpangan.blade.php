@extends('layouts.user.app')
@section('header-class')
    {{"main-header-area-sticky"}}
@endsection
@section('content')
    <link rel="stylesheet" href="/css/nice-select.css">
    <script src="/js/nice-select.min.js"></script>
    <div class="slider_area">
        <div class="single_slider d-flex align-items-center slider_bg_1" style="height: 100vh;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section_title text-center mb-50">
                            <span class="wow lightSpeedIn" data-wow-duration="1s" data-wow-delay=".1s"></span>
                            <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".2s" style="color: white">
                                PENGAJUAN TUMPANGAN</h3>
                            <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s"
                               style="color: white; font-size: 25px">Cari nomor IPTM yang sudah terdaftar disini</p>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center justify-content-center">
                    <div class="col-sm-12">
                        <div class="wow fadeInUp" data-wow-duration="1.2s" data-wow-delay=".4s">
                            <div class="mt-10">
                                <input type="text" id="noIPTM" name="no_iptm" placeholder="Masukan Nomor IPTM Lama"
                                       onfocus="this.placeholder = ''" onblur="this.placeholder = 'Masukan Nomor IPTM'"
                                       required
                                       style="text-align: center"
                                       class="single-input-primary">
                            </div>
                            <br>
                            <div class="text-center">
                                <button class="boxed-btn3" type="button" id="cekIPTM"><i id="load-icon"
                                                                                         class="fa fa-refresh"></i> Cari
                                    IPTM
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="detailIPTM" style="display: none;padding-top: 150px;padding-bottom: 100px; background-color: lightgrey">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section_title text-center mb-50">
                        <span class="wow lightSpeedIn" data-wow-duration="1s" data-wow-delay=".1s"></span>
                        <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".2s" style="color: black">
                            Formulir Pengajuan Tumpangan</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4" id="prefillData">
                    <h4>Informasi Makam Tumpangan</h4>
                    <hr>
                    <div class="mt-10">
                        <label style="color: black">Nomor IPTM Lama</label>
                        <input type="text" id="nomor_iptm" name="no_iptm" placeholder="Nomor IPTM"
                               onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nomor IPTM'" disabled
                               class="single-input">
                    </div>
                    <div class="mt-10">
                        <label for="">Nama Almarhum/ah Lama</label>
                        <input type="text" id="nama_almarhum" name="nama_almarhum" placeholder="Nama Almarhum"
                               onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nama Almarhum'" disabled
                               class="single-input">
                    </div>
                    <div class="mt-10">
                        <label style="color: black">Tanggal Wafat</label>
                        <input type="date" id="tanggal_wafat" name="tanggal_wafat" placeholder="Tanggal Wafat"
                               onfocus="this.placeholder = ''" onblur="this.placeholder = 'Tanggal Wafat'" disabled
                               class="single-input">
                    </div>
                    <div class="mt-10">
                        <label style="color: black">Pemakaman</label>
                        <input type="text" id="nama_pemakaman" name="nama_pemakaman" placeholder="Nama Pemakaman"
                               onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nama Pemakaman'" disabled
                               class="single-input">
                    </div>
                    <div class="mt-10">
                        <label style="color: black">Blok Makam</label>
                        <input type="text" id="blok" name="blok" placeholder="Blok"
                               onfocus="this.placeholder = ''" onblur="this.placeholder = 'Blok'" disabled
                               class="single-input">
                    </div>
                    <div class="mt-10">
                        <label style="color: black">Blad Makam</label>
                        <input type="text" id="blad" name="blad" placeholder="Blad"
                               onfocus="this.placeholder = ''" onblur="this.placeholder = 'Blad'" disabled
                               class="single-input">
                    </div>
                    <div class="mt-10">
                        <label style="color: black">Petak Makam</label>
                        <input type="text" id="petak" name="petak" placeholder="Petak"
                               onfocus="this.placeholder = ''" onblur="this.placeholder = 'Petak'" disabled
                               class="single-input">
                    </div>
                    <div class="mt-10">
                        <label style="color: black">Tanggal Kadaluarsa</label>
                        <input type="date" id="masa_berlaku" name="masa_berlaku" placeholder="Tanggal Kadaluwarsa"
                               onfocus="this.placeholder = ''" onblur="this.placeholder = 'Tanggal Kadaluwarsa'"
                               disabled
                               class="single-input">
                    </div>
                </div>
                <div class="col-sm-8">
                    <form id="formTumpangan" action="{{url('/IPTM/tumpangan/submit')}}" method="post"
                          enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="text" id="iptm_id" name="iptm_id" hidden>
                        <div class="formStep">
                            <div class="step" data-step="1">
                                <h4>Informasi Almarhum/ah Baru</h4>
                                <hr>
                                <h5>Data dengan tanda (<label style="color: red">*</label>) tidak boleh kosong</h5>
                                <hr>
                                <div class="mt-10">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label style="color: black">
                                                Nama Almarhum/ah
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="text" name="nama_almarhum" placeholder="Nama Almarhum"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'Nama Almarhum'" required=""
                                                   oninvalid="this.setCustomValidity('Nama tidak boleh kosong')"
                                                   oninput="setCustomValidity('')"
                                                   class="single-input-primary">
                                        </div>
                                        <div class="col-sm-6">
                                            <label style="color: black">
                                                Tanggal Wafat
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="date" name="tanggal_wafat" placeholder="Tanggal Wafat"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'Tanggal Wafat'" required=""
                                                   oninvalid="this.setCustomValidity('Tanggal wafat tidak boleh kosong')"
                                                   oninput="setCustomValidity('')"
                                                   class="single-input-primary">
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-10">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label style="color: black">
                                                Foto KTP Almarhum/ah (Fotocopy/Asli)
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="file" name="file_ktp_almarhum"
                                                   placeholder="Foto KTP Almarhum (Fotocopy/Asli)"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'Foto KTP Almarhum (Fotocopy/Asli)'"
                                                   required=""
                                                   oninvalid="this.setCustomValidity('Foto KTP tidak boleh kosong')"
                                                   oninput="setCustomValidity('')"
                                                   class="single-input-primary">
                                        </div>
                                        <div class="col-sm-6">
                                            <label style="color: black">
                                                Foto KK Almarhum/ah (Fotocopy/Asli)
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="file" name="file_kk_almarhum"
                                                   placeholder="Foto KK Almarhum (Fotocopy/Asli)"
                                                   required=""
                                                   oninvalid="this.setCustomValidity('Foto KK tidak boleh kosong')"
                                                   oninput="setCustomValidity('')"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'Foto KK Almarhum (Fotocopy/Asli)'"
                                                   class="single-input-primary">
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-10">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label style="color: black">
                                                Nomor KTP Almarhum/ah
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="text" name="nomor_ktp_almarhum"
                                                   placeholder="Nomor KTP Almarhum"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'Nomor KTP Almarhum'" required=""
                                                   oninvalid="this.setCustomValidity('Nomor KTP tidak boleh kosong')"
                                                   oninput="setCustomValidity('')"
                                                   class="single-input-primary">
                                        </div>
                                        <div class="col-sm-6">
                                            <label style="color: black">
                                                Nomor KK Almarhum/ah (NIK)
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="text" name="nomor_kk_almarhum" placeholder="No KK Almarhum"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'No KK Almarhum'" required=""
                                                   oninvalid="this.setCustomValidity('Nomor KK tidak boleh kosong')"
                                                   oninput="setCustomValidity('')"
                                                   class="single-input-primary">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <h4>Surat Keterangan & Lampiran</h4>
                                <br>
                                <div class="mt-10">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label for="" style="color: black">
                                                Foto IPTM
                                            </label>
                                            <input type="file" name="file_iptm_asli" class="single-input-primary">
                                        </div>
                                        <div class="col-sm-6">
                                            <label style="color: black">
                                                Foto Surat Kehilangan Kepolisian
                                            </label>
                                            <input type="file" name="file_sk_kehilangan_kepolisian"
                                                   class="single-input-primary">
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-10">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label style="color: black">
                                                Nomor Surat Kehilangan Kepolisian
                                            </label>
                                            <input type="text" name="nomor_kehilangan_kepolisian"
                                                   placeholder="No Surat Kehilangan Kepolisian"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'No Surat Kehilangan Kepolisian'"
                                                   class="single-input-primary">
                                        </div>
                                        <div class="col-sm-6">
                                            <label style="color: black">
                                                Tanggal SK Kehilangan Kepolisian
                                            </label>
                                            <input type="date" name="tanggal_kehilangan_kepolisian"
                                                   placeholder="Tanggal Surat Kehilangan Kepolisian"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'Tanggal Surat Kehilangan Kepolisian'"
                                                   class="single-input-primary">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="mt-10">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label style="color: black">Nomor Surat Pengantar RT/RW</label>
                                            <input type="text" name="nomor_sp_rtrw"
                                                   placeholder="No Surat Pengantar RT/RW"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'No Surat Pengantar RT/RW'"
                                                   class="single-input-primary">
                                        </div>
                                        <div class="col-sm-4">
                                            <label style="color: black">Tanggal Surat Pengantar RT/RW</label>
                                            <input type="date" name="tanggal_sp_rtrw"
                                                   placeholder="Tanggal Surat Pengantar RT/RW"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'Tanggal Surat Pengantar RT/RW'"
                                                   class="single-input-primary">
                                        </div>
                                        <div class="col-sm-4">
                                            <label style="color: black">Foto Surat Pengantar RT/RW</label>
                                            <input type="file" name="file_sp_rtrw" placeholder="Foto Surat Pengantar"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'Foto Surat Pengantar'"
                                                   class="single-input-primary">
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-10">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label style="color: black">
                                                Nomor Surat Kematian RS/Puskesmas
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="text" name="nomor_sk_kematian_rs"
                                                   placeholder="No Surat Kematian"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'No Surat Kematian'" required=""
                                                   oninvalid="this.setCustomValidity('Nomor SK Kematian tidak boleh kosong')"
                                                   oninput="setCustomValidity('')"
                                                   class="single-input-primary">
                                        </div>
                                        <div class="col-sm-4">
                                            <label style="color: black">
                                                Tanggal Surat Kematian RS/Puskesmas
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="date" name="tanggal_sk_kematian_rs"
                                                   placeholder="Tanggal Surat Kematian"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'Tanggal Surat Kematian'" required=""
                                                   oninvalid="this.setCustomValidity('Tanggal Surat Kematian tidak boleh kosong')"
                                                   oninput="setCustomValidity('')"
                                                   class="single-input-primary">
                                        </div>
                                        <div class="col-sm-4">
                                            <label style="color: black">
                                                Foto Surat Keterangan Kematian RS/Puskesmas
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="file" name="file_sk_kematian_rs"
                                                   placeholder="Foto Surat Kematian RS"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'Foto Surat Kematian RS'"
                                                   required=""
                                                   oninvalid="this.setCustomValidity('Foto SK Kematian RS tidak boleh kosong')"
                                                   oninput="setCustomValidity('')"
                                                   class="single-input-primary">
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-10">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label style="color: black">
                                                Nomor Surat Keterangan Kematian Kelurahan
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="text" name="nomor_sk_kematian_kelurahan"
                                                   placeholder="No Surat Kelurahan"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'No Surat Kematian'" required=""
                                                   oninvalid="this.setCustomValidity('Nomor SK Kelurahan tidak boleh kosong')"
                                                   oninput="setCustomValidity('')"
                                                   class="single-input-primary">
                                        </div>
                                        <div class="col-sm-4">
                                            <label style="color: black">
                                                Tanggal Surat Keterangan Kematian Kelurahan
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="date" name="tanggal_sk_kematian_kelurahan"
                                                   placeholder="Tanggal Surat Kelurahan"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'Tanggal Surat Kelurahan'" required=""
                                                   oninvalid="this.setCustomValidity('Tanggal SK Kelurahan tidak boleh kosong')"
                                                   oninput="setCustomValidity('')"
                                                   class="single-input-primary">
                                        </div>
                                        <div class="col-sm-4">
                                            <label style="color: black">
                                                Foto Surat Keterangan Kematian Kelurahan
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="file" name="file_sk_kematian_kelurahan"
                                                   placeholder="Foto Surat Kematian Kelurahan"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'Foto Surat Kematian Kelurahan'"
                                                   required=""
                                                   oninvalid="this.setCustomValidity('Foto SK Kelurahan tidak boleh kosong')"
                                                   oninput="setCustomValidity('')"
                                                   class="single-input-primary">
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                <div class="row">
                                    <div class="text-right col-sm-12">
                                        <button class="boxed-btn3 nextStep" type="button">Data Ahliwaris</button>
                                    </div>
                                </div>
                            </div>
                            <div class="step" data-step="2">
                                <h4>Informasi Ahli Waris</h4>
                                <hr>
                                <h5>Data dengan tanda (<label style="color: red">*</label>) tidak boleh kosong</h5>
                                <hr>
                                <div class="mt-10">
                                    <label style="color: black">
                                        Nama Ahli Waris
                                        <label style="color: red">*</label>
                                    </label>
                                    <input type="text" name="nama_ahliwaris" placeholder="Nama Ahli Waris"
                                           onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nama Ahli Waris'"
                                           required=""
                                           oninvalid="this.setCustomValidity('Nama Ahliwaris tidak boleh kosong')"
                                           oninput="setCustomValidity('')"
                                           class="single-input-primary">
                                </div>
                                <div class="mt-10">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label style="color: black">
                                                Nomor KTP Ahli Waris
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="text" name="nomor_ktp_ahliwaris"
                                                   placeholder="No KTP Ahli Waris"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'No KTP Ahli Waris'" required=""
                                                   oninvalid="this.setCustomValidity('Nomor KTP Ahliwaris tidak boleh kosong')"
                                                   oninput="setCustomValidity('')"
                                                   class="single-input-primary">
                                        </div>
                                        <div class="col-sm-6">
                                            <label style="color: black">
                                                Foto KTP Ahli Waris
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="file" name="file_ktp_ahliwaris" class="single-input-primary"
                                                   required=""
                                                   oninvalid="this.setCustomValidity('Foto KTP tidak boleh kosong')"
                                                   oninput="setCustomValidity('')">
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-10">
                                    <label style="color: black">
                                        Nomor Telepon/HP Ahli Waris
                                        <label style="color: red">*</label>
                                    </label>
                                    <input type="text" name="telepon_ahliwaris" placeholder="Telepon Ahli Waris"
                                           onfocus="this.placeholder = ''"
                                           onblur="this.placeholder = 'Telepon Ahli Waris'" required=""
                                           oninvalid="this.setCustomValidity('Nomor Telp/HP tidak boleh kosong')"
                                           oninput="setCustomValidity('')"
                                           class="single-input-primary">
                                </div>
                                <div class="mt-10">
                                    <label style="color: black">
                                        Hubungan dengan almarhum/ah
                                        <label style="color: red">*</label>
                                    </label>
                                    <input type="text" name="hubungan_ahliwaris"
                                           placeholder="Hubungan dengan almarhum/ah"
                                           onfocus="this.placeholder = ''" onblur="this.placeholder = 'Hubungan'"
                                           required=""
                                           oninvalid="this.setCustomValidity('Data hubungan dengan almarhum tidak boleh kosong')"
                                           oninput="setCustomValidity('')"
                                           class="single-input-primary">
                                </div>
                                <div class="mt-10">
                                    <label style="color: black">
                                        Alamat Ahliwaris
                                        <label style="color: red">*</label>
                                    </label>
                                    <textarea class="single-textarea" name="alamat_ahliwaris" placeholder="Alamat"
                                              onfocus="this.placeholder = ''"
                                              onblur="this.placeholder = 'Alamat'" required=""
                                              oninvalid="this.setCustomValidity('Alamat Ahliwaris tidak boleh kosong')"
                                              oninput="setCustomValidity('')"></textarea>
                                </div>
                                <div class="mt-10">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label style="color: black">
                                                RT
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="number" min="1" name="rt_ahliwaris" placeholder="001"
                                                   required=""
                                                   oninvalid="this.setCustomValidity('RT tidak boleh kosong')"
                                                   oninput="setCustomValidity('')"
                                                   class="single-input-primary">
                                        </div>
                                        <div class="col-sm-2">
                                            <label style="color: black">
                                                RW
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="number" min="1" name="rw_ahliwaris" placeholder="001"
                                                   required=""
                                                   oninvalid="this.setCustomValidity('RW tidak boleh kosong')"
                                                   oninput="setCustomValidity('')"
                                                   class="single-input-primary">
                                        </div>
                                        <div class="col-sm-8">
                                            <label style="color: black">
                                                Kelurahan
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="text" name="kelurahan_ahliwaris" placeholder="Kelurahan"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'Kelurahan'"
                                                   required=""
                                                   oninvalid="this.setCustomValidity('Kelurahan tidak boleh kosong')"
                                                   oninput="setCustomValidity('')" class="single-input-primary">
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-10">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label style="color: black">
                                                Kecamatan
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="text" name="kecamatan_ahliwaris" placeholder="kecamatan"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'Kecamatan'"
                                                   required=""
                                                   oninvalid="this.setCustomValidity('Kecamatan tidak boleh kosong')"
                                                   oninput="setCustomValidity('')" class="single-input-primary">
                                        </div>
                                        <div class="col-sm-6">
                                            <label style="color: black">
                                                Kota Administrasi
                                                <label style="color: red">*</label>
                                            </label>
                                            <input type="text" name="kota_administrasi" placeholder="Kota Administrasi"
                                                   onfocus="this.placeholder = ''"
                                                   onblur="this.placeholder = 'Kota Administrasi'"
                                                   required=""
                                                   oninvalid="this.setCustomValidity('Kota tidak boleh kosong')"
                                                   oninput="setCustomValidity('')" class="single-input-primary">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="text-left col-sm-6">
                                        <button class="boxed-btn3 prevStep" type="button">Data Almarhum</button>
                                    </div>
                                    <div class="text-right col-sm-6">
                                        <button class="boxed-btn3" id="SubmitAndSaveBtn" type="button">Ajukan
                                            Tumpangan
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="submitConfirmModal" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Pengajuan Tumpangan
                                            Izin</h5>
                                    </div>
                                    <div class="modal-body">
                                        <h4>Kirim permohonan tumpangan izin?</h4>
                                        <small>Pastikan anda telah mengisi formulir dengan lengkap dan benar.</small>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal
                                        </button>
                                        <button type="submit" id="submitButton" class="btn btn-primary">Kirim</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" id="validateForm" hidden></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            CustomFormStep();

            $("#cekIPTM").click(function () {
                var loadIcon = $("#load-icon");
                loadIcon.addClass("do-spin");
                $(".btn").attr("disabled", "disabled");

                setTimeout(function () {
                    CheckIPTM("#noIPTM");
                }, 1000);
            });

            $("#cancelSubmit").click(function () {
                $(".dataSection").removeAttr("hidden");
                $(".submitSection").attr("hidden", "hidden");
            });


            $("#SubmitAndSaveBtn").click(function () {

                var nullRequiredField = false;

                $("#formTumpangan").find(':input').each(function () {
                    if ($(this).attr("required")) {
                        if ($(this).val() === null || $(this).val() === undefined || $(this).val() === "") {
                            nullRequiredField = true;
                            return false;
                        }
                    }
                });

                if (nullRequiredField) {
                    $([document.documentElement, document.body]).animate({
                        scrollTop: $("#detailIPTM").offset().top
                    }, 800);
                    $("#validateForm").click();
                } else {
                    $("#submitConfirmModal").modal("show");
                }
//
            });
        });

        function CheckIPTM(noIptmField) {
            var noIptm = $(noIptmField).val();

            if (noIptm === "") {
                alert("IPTM tidak ditemukan");
            } else {
                var urlReq = "/json/iptm-by-no?noiptm=" + noIptm + "&jenis_pemesanan=tumpangan";

                $.ajax({
                    'url': urlReq,
                    'type': 'GET',
                    success: function (result) {
                        let data = JSON.parse(result);
                        if (data.length > 0) {
                            /*alert("IPTM ditemukan");*/

                            $("#prefillData").find(':input').each(function () {
                                var id = this.id;
                                $(this).val(data[0][id]);
                            });
                            $("#iptm_id").val(data[0]["id"]);

                            $("#detailIPTM").show();
                            $([document.documentElement, document.body]).animate({
                                scrollTop: $("#detailIPTM").offset().top
                            }, 800);
                        } else {
                            alert("IPTM tidak ditemukan");
                            /*Swal.fire(
                                {
                                    'title': 'Pencarian Gagal',
                                    'text': 'Nomor IPTM tidak ditemukan',
                                    'type': 'error'
                                }
                            )*/
                        }
                    }
                });
            }
            var loadIcon = $("#load-icon");
            loadIcon.removeClass("do-spin");
            $(".btn").removeAttr("disabled");
        }

        function CustomFormStep() {
            var mainForm = $(".formStep");
            mainForm.children(".step").each(function () {
                if ($(this).attr("data-step") !== "1") {
                    $(this).hide();
                } else {
                    $(this).addClass("activeStep");
                }
            });

            $(".nextStep").click(function () {


                var nullRequiredField = false;

                mainForm.children(".activeStep").find(':input').each(function () {
                    if ($(this).attr("required")) {
                        if ($(this).val() === null || $(this).val() === undefined || $(this).val() === "") {
                            nullRequiredField = true;
                            return false;
                        }
                    }
                });

                if (nullRequiredField) {
                    $([document.documentElement, document.body]).animate({
                        scrollTop: $("#detailIPTM").offset().top
                    }, 800);
                    $("#validateForm").click();
                } else {
                    var currentStep = mainForm.children(".activeStep").attr("data-step");
                    var nextStep = parseInt(currentStep) + 1;
                    console.log(nextStep);
                    mainForm.children(".activeStep").addClass("fadeOutDown animated");

                    setTimeout(function () {
                        mainForm.children(".activeStep").hide();
                        mainForm.children(".activeStep").removeClass("activeStep fadeOutDown animated");

                        mainForm.children(".step").each(function () {
                            if ($(this).attr("data-step") === nextStep.toString()) {
                                $(this).show();
                                $(this).addClass("activeStep fadeInDown animated");
                            }
                        });
                    }, 800);

                    $([document.documentElement, document.body]).animate({
                        scrollTop: $("#detailIPTM").offset().top
                    }, 800);
                }

            });

            $(".prevStep").click(function () {
                var currentStep = mainForm.children(".activeStep").attr("data-step");
                var prevStep = parseInt(currentStep) - 1;
                console.log(prevStep);
                mainForm.children(".activeStep").addClass("fadeOutUp animated");

                setTimeout(function () {
                    mainForm.children(".activeStep").hide();
                    mainForm.children(".activeStep").removeClass("activeStep fadeOutUp animated");

                    mainForm.children(".step").each(function () {
                        if ($(this).attr("data-step") === prevStep.toString()) {
                            $(this).show();
                            $(this).addClass("activeStep fadeInUp animated");
                        }
                    });
                }, 800);

                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#detailIPTM").offset().top
                }, 800);
            });
        }

    </script>
@endsection
