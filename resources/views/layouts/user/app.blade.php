<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>IREMIA</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="/images/assets/favicon.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/magnific-popup.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/themify-icons.css">
    {{--<link rel="stylesheet" href="/css/nice-select.css">--}}
    <link rel="stylesheet" href="/css/flaticon.css">
    <link rel="stylesheet" href="/css/gijgo.css">
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/slick.css">
    <link rel="stylesheet" href="/css/slicknav.css">
    <link rel="stylesheet" href="/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="/vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

    <link rel="stylesheet" href="/css/style.css">
    <link rel=”stylesheet” href=”https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">

    <style>
        .dataTables_length {
            display: none;
        }
    </style>
    <!-- <link rel="stylesheet" href="css/responsive.css"> -->

    <script src="/js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/owl.carousel.min.js"></script>
    <script src="/js/isotope.pkgd.min.js"></script>
    <script src="/js/ajax-form.js"></script>
    <script src="/js/waypoints.min.js"></script>
    <script src="/js/jquery.counterup.min.js"></script>
    <script src="/js/imagesloaded.pkgd.min.js"></script>
    <script src="/js/scrollIt.js"></script>
    <script src="/js/jquery.scrollUp.min.js"></script>
    <script src="/js/wow.min.js"></script>
    {{--<script src="/js/nice-select.min.js"></script>--}}
    <script src="/js/jquery.slicknav.min.js"></script>
    <script src="/js/jquery.magnific-popup.min.js"></script>
    <script src="/js/plugins.js"></script>
    <script src="/js/gijgo.min.js"></script>
    <script src="/js/slick.min.js"></script>

    <!--contact j/s-->
    <script src="/js/contact.js"></script>
    <script src="/js/jquery.ajaxchimp.min.js"></script>
    <script src="/js/jquery.form.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/mail-script.js"></script>

    <script src="/js/main.js"></script>
    <script src=”https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</head>

<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade
    your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- header-start -->
<header class="page-header">
    <div class="header-area">
        <div class="@yield('header-class')" style="">
            <div class="container-fluid">
                <div class="header_bottom_border">
                    <div class="row align-items-center">
                        <div class="col-xl-5 col-lg-2">
                            <div class="logo">
                                <a href="{{ url('/') }}">
                                    <img src="/images/Logo_IREMIA_New.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-7">
                            <div class="main-menu  d-none d-lg-block">
                                <ul id="navigation">
                                    <li><a href="/">Beranda</a></li>
                                    <li><a href="#">Panduan <i class="ti-angle-down"></i></a>
                                        <ul class="submenu">
                                            @if(Auth::check())
                                                <li><a href="/pemakaman/cari">Semua Pemakaman</a></li>
                                                <li><a href="/jadwal-pemakaman">Jadwal Pemakaman</a></li>
                                            @endif
                                            <li><a href="/pemakaman/tata-cara">Tata Cara Pengajuan</a></li>
                                        </ul>
                                    </li>

                                    @if(Auth::user())
                                        <li><a href="#">IPTM <i class="ti-angle-down"></i></a>
                                            <ul class="submenu">
                                                <li><a href="{{ url('pemakaman/pesanan/details') }}">Status IPTM
                                                        Saya</a></li>
                                            </ul>
                                        </li>
                                    @endif
                                    @if(Auth::user())
                                        <li><a href="#">{{Auth::user()->fullname}}</a>
                                            <ul class="submenu">
                                                <li>
                                                    <a href="{{url('/profile')}}">Profil Saya</a>
                                                </li>
                                                <li>
                                                    <a href="/logout">Logout</a>
                                                </li>
                                            </ul>
                                        </li>
                                    @else
                                        <li><a href="{{ url('login') }}">Masuk</a></li>
                                        <li><a href="{{ url('register') }}">Daftar</a></li>
                                    @endif
                                </ul>
                                @if(Session::has('success'))
                                    <div class="alert alert-success alert-dismissible fade show" role="alert"
                                         style="top: 10px; left: 750px; position: fixed; text-align: center; width: 20%">
                                        {{ Session::get('success') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                <nav></nav>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header-end -->
<div class="modal fade" id="LoginModal" data-backdrop="false" tabindex="-1" role="dialog"
     aria-labelledby="LoginModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document"
         style="box-shadow: 0px 50px 90px 0 rgba(0, 0, 0, 0.2), 0px 0px 80px 90px rgba(0, 0, 0, 0.4);">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="LoginModalLabel">Masuk</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('login') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div>
                        <label for="">Email</label>
                        <input type="email" name="email" placeholder="Input Email" onfocus="this.placeholder = ''"
                               onblur="this.placeholder = 'Masukan Email'" required class="single-input">
                    </div>
                    <div class="mt-10">
                        <label for="">Password</label>
                        <input type="password" name="password" placeholder="Input Password"
                               onfocus="this.placeholder = ''" onblur="this.placeholder = 'Masukan Password'" required
                               class="single-input">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="genric-btn danger radius" data-dismiss="modal">Tutup</button>
                    <button class="genric-btn success radius" type="submit">Masuk</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="RegisterModal" data-backdrop="false" tabindex="-1" role="dialog"
     aria-labelledby="RegisterModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document"
         style="box-shadow: 0px 50px 90px 0 rgba(0, 0, 0, 0.2), 0px 0px 80px 90px rgba(0, 0, 0, 0.4);">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="RegisterModalLabel">Daftar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/user/register" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <img src="/images/no-image-available.jpg" width="100%" alt="">
                            <input type="file" name="photo_pemakaman" class="form-control">
                        </div>
                        <div class="col-sm-8">
                            <div>
                                <label for="">Nama Lengkap</label>
                                <input type="text" name="fullname" placeholder="Masukan nama lengkap"
                                       onfocus="this.placeholder = ''"
                                       onblur="this.placeholder = 'Masukan nama lengkap'" required class="single-input">
                            </div>
                            <div>
                                <label for="">Jenis Kelamin</label>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <input type="radio" name="jenis_kelamin" value="male"> Laki-laki
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="radio" name="jenis_kelamin" value="female">Perempuan
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div>
                                <label for="">Alamat Email</label>
                                <input type="email" name="email" placeholder="Masukan email"
                                       onfocus="this.placeholder = ''" onblur="this.placeholder = 'Masukan email'"
                                       required class="single-input">
                            </div>
                            <div class="mt-10">
                                <label for="">Password</label>
                                <input type="password" name="password" placeholder="Masukan password"
                                       onfocus="this.placeholder = ''" onblur="this.placeholder = 'Masukan password'"
                                       required class="single-input">
                            </div>
                            <div class="mt-10">
                                <label for="">Konfirmasi Password</label>
                                <input type="password" name="confirm" placeholder="Ketik ulang password"
                                       onfocus="this.placeholder = ''"
                                       onblur="this.placeholder = 'Ketik ulang password'" required class="single-input">
                            </div>
                            <div>
                                <label for="">Alamat</label>
                                <textarea name="address" class="single-input" cols="30" rows="3" required></textarea>
                            </div>
                            <div class="switch-wrap d-flex">
                                <div class="primary-checkbox">
                                    <input type="checkbox" name="confirm-checkbox" id="confirm-checkbox" required>
                                    <label for="confirm-checkbox"></label>
                                </div>
                                <p class="radio-label">Saya menyetujui syarat dan ketentuan yang berlaku</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="genric-btn danger radius" data-dismiss="modal">Tutup</button>
                    <button class="genric-btn success radius" type="submit">Daftar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- slider_area_start -->


@yield('content');


<!-- footer start -->
<footer class="footer">
    <div class="copy-right_text wow fadeInUp" data-wow-duration="1.4s" data-wow-delay=".3s">
        <div class="container">
            <div class="footer_border"></div>
            <div class="row">
                <div class="col-xl-12" style="margin-top: 50px;">
                    <p class="copy_right text-center">
                        Copyright &copy; 2020 IREMIA. All rights reserved.
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/ footer end  -->

<!-- link that opens popup -->


<script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
<script src="/assets/js/init-scripts/data-table/datatables-init.js"></script>
@yield('script')
@include('sweetalert::alert')
</body>

</html>
