@extends('layouts.app')
@section('content')

    <style>
        .do-spin{
            animation-name: spin;
            animation-duration: 1000ms;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
            /* transform: rotate(3deg); */
            /* transform: rotate(0.3rad);/ */
            /* transform: rotate(3grad); */
            /* transform: rotate(.03turn);  */
        }

        @keyframes spin {
            from {
                transform:rotate(0deg);
            }
            to {
                transform:rotate(360deg);
            }
        }

        .wordwrap {
            width: 450px;
            white-space: pre-wrap;      /* CSS3 */
            white-space: -moz-pre-wrap; /* Firefox */
            white-space: -o-pre-wrap;   /* Opera 7 */
            word-wrap: break-word;      /* IE */
        }
    </style>

    <div class="sufee-login d-flex align-content-center flex-wrap" style="padding-top: 10px">
        <div class="container">
            <!-- The Modal -->
            <div class="col-md-12">
                @if($errors->first())
                    <div class="alert  alert-danger alert-dismissible fade show" role="alert">
                        <span class="badge badge-pill badge-danger">Error</span> {{$errors->first()}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                @if(count($cetakPemakamanName)>0)
                    @foreach($cetakPemakamanName as $pemakamanName)
                        <div class="col-md-12">
                            <h2> Surat Pengantar Izin Tumpang {{$pemakamanName->nama_pemakaman}}</h2>
                            <hr style="border: solid">
                            <div class="row">
                                <div class="col-sm-4">
                                    <h5>Cari IPTM Pemberi Tumpangan</h5>
                                    <hr>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">Nomor IPTM</div>
                                            <input type="text" id="filterNomor" name="nomor_iptm" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">Nama Almarhum</div>
                                            <input type="text" id="filterNama" name="nama_almarhum" class="form-control">
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <button class="btn btn-primary" id="cekIPTM" type="button"><i id="load-icon" class="fa fa-refresh"></i> Cari</button>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <h5>&nbsp;</h5>
                                    <hr>
                                    <div class="dataSection">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Nomor IPTM</th>
                                                    <th>Nama Almarhum</th>
                                                    <th>Nama Ahli Waris</th>
                                                    <th>Informasi Makam</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="jsonResult">
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="submitSection" hidden>
                                        <form action="/IPTM/tumpangan/submit" method="POST" enctype="multipart/form-data" id="formTumpangan" target="_blank">
                                            @csrf
                                            <h5>Data yang memiliki tanda ( <label style="color: red">*</label> ) tidak boleh kosong</h5>
                                            <hr>
                                            <h6>Data Almarhum Pemberi Tumpangan</h6>
                                            <hr>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon" style="color: black">Nomor IPTM Lama</div>
                                                    <input type="text" id="nomor_iptm" class="form-control" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon" style="color: black">Tanggal Kadaluarsa IPTM</div>
                                                    <input type="date" id="masa_berlaku" class="form-control" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon" style="color: black">Nama Almarhum/ah</div>
                                                    <input type="text" id="nama_almarhum" placeholder="Nama Almarhum" class="form-control" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon" style="color: black">Tanggal Wafat Almarhum/ah</div>
                                                    <input type="date" id="tanggal_wafat" class="form-control" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon" style="color: black">No KTP Almarhum/ah</div>
                                                    <input type="text" id="nomor_ktp_almarhum" placeholder="No KTP Almarhum" class="form-control" readonly>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-group col-sm-4">
                                                    <div class="input-group-addon" style="color: black">Blok Makam</div>
                                                    <input type="text" id="blok" placeholder="Blok Makam" class="form-control" readonly>
                                                </div>
                                                <div class="input-group col-sm-4">
                                                    <div class="input-group-addon" style="color: black">Blad Makam</div>
                                                    <input type="text" id="blad" placeholder="Blad Makam" class="form-control" readonly>
                                                </div>
                                                <div class="input-group col-sm-4">
                                                    <div class="input-group-addon" style="color: black">Petak Makam</div>
                                                    <input type="text" id="petak" placeholder="Petak Makam" class="form-control" readonly>
                                                </div>
                                            </div>
                                            <br>
                                            <h6>Surat Keterangan Kehilangan Kepolisian (Jika ada)</h6>
                                            <hr>
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">Nomor Surat</div>
                                                        <input type="text" id="nomor_sk_kehilangan_kepolisian" name="nomor_sk_kehilangan_kepolisian" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group col-sm-6">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">Tanggal Surat</div>
                                                        <input type="date" id="tanggal_sk_kehilangan_kepolisian" name="tanggal_sk_kehilangan_kepolisian" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <h6>Surat Rekomendasi Izin Tumpang</h6>
                                            <hr>
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Nomor Surat
                                                            <label style="color: red">*</label>
                                                        </div>
                                                        <input type="text" id="nomor_surat" name="nomor_surat" class="form-control" required=""
                                                               oninvalid="this.setCustomValidity('Nomor Surat tidak boleh kosong')"
                                                               oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Tanggal Surat
                                                            <label style="color: red">*</label>
                                                        </div>
                                                        <input type="date" id="tanggal_surat" name="tanggal_surat" class="form-control" required=""
                                                               oninvalid="this.setCustomValidity('Tanggal Surat tidak boleh kosong')"
                                                               oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Daftar Jenazah Lama (Sesuai dengan surat IPTM)
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group col-sm-12">
                                                    <div class="input-group">
                                                        <input type="text" id="daftar_jenazah" name="daftar_jenazah" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Berlaku Dari
                                                            <label style="color: red">*</label>
                                                        </div>
                                                        <input type="date" id="tanggal_berlaku_dari" name="tanggal_berlaku_dari" class="form-control" required=""
                                                               oninvalid="this.setCustomValidity('Tanggal berlaku tidak boleh kosong')"
                                                               oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Berlaku Sampai
                                                            <label style="color: red">*</label>
                                                        </div>
                                                        <input type="date" id="tanggal_berlaku_sampai" name="tanggal_berlaku_sampai" class="form-control" required=""
                                                               oninvalid="this.setCustomValidity('Tanggal berlaku tidak boleh kosong')"
                                                               oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <h6>Data Almarhum Baru</h6>
                                            <hr>
                                            <div class="form-group" hidden>
                                                <div class="input-group">
                                                    <div class="input-group-addon">Id almarhum <span class="field-required">*</span></div>
                                                    <input type="text" id="almarhum_id" name="almarhum_id" class="form-control" required readonly hidden>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon" style="color: black">
                                                        Nama Almarhum
                                                        <label style="color: red">*</label>
                                                    </div>
                                                    <input type="text" name="nama_almarhum" class="form-control" required=""
                                                           oninvalid="this.setCustomValidity('Nama Almarhum tidak boleh kosong')"
                                                           oninput="setCustomValidity('')">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Tanggal Wafat
                                                            <label style="color: red">*</label>
                                                        </div>
                                                        <input type="date" name="tanggal_wafat" class="form-control" required=""
                                                               oninvalid="this.setCustomValidity('Tanggal wafat tidak boleh kosong')"
                                                               oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Nomor KTP (NIK)
                                                            <label style="color: red">*</label>
                                                        </div>
                                                        <input type="text" name="nomor_ktp_almarhum" class="form-control" required=""
                                                               oninvalid="this.setCustomValidity('Nomor KTP tidak boleh kosong')"
                                                               oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Nomor Kartu Keluarga
                                                            <label style="color: red">*</label>
                                                        </div>
                                                        <input type="text"name="nomor_kk_almarhum" class="form-control" required=""
                                                               oninvalid="this.setCustomValidity('Nomor KK tidak boleh kosong')"
                                                               oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Nomor SK Kematian Kelurahan
                                                            <label style="color: red">*</label>
                                                        </div>
                                                        <input type="text" name="nomor_sk_kematian_kelurahan" class="form-control" required=""
                                                               oninvalid="this.setCustomValidity('Nomor Surat Kematian tidak boleh kosong')"
                                                               oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Nomor SK Kematian Rumah Sakit / Puskesmas
                                                            <label style="color: red">*</label>
                                                        </div>
                                                        <input type="text"name="nomor_sk_kematian_rs" class="form-control" required=""
                                                               oninvalid="this.setCustomValidity('Nomor Surat Kematian tidak boleh kosong')"
                                                               oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Nomor Surat Pengantar RT/RW
                                                        </div>
                                                        <input type="text" name="nomor_sp_rtrw" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Tanggal Surat Pengantar RT/RW
                                                        </div>
                                                        <input type="date" name="tanggal_sp_rtrw" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <h6>Data Ahli Waris</h6>
                                            <hr>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon" style="color: black">
                                                        Nama Ahli Waris
                                                        <label style="color: red">*</label>
                                                    </div>
                                                    <input type="text" name="nama_ahliwaris" class="form-control" required=""
                                                           oninvalid="this.setCustomValidity('Nama tidak boleh kosong')"
                                                           oninput="setCustomValidity('')">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Hubungan dengan Almarhum/ah
                                                            <label style="color: red">*</label>
                                                        </div>
                                                        <input type="text" name="hubungan_ahliwaris" class="form-control" required=""
                                                               oninvalid="this.setCustomValidity('Data hubungan tidak boleh kosong')"
                                                               oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Nomor KTP (NIK)
                                                            <label style="color: red">*</label>
                                                        </div>
                                                        <input type="text"  name="nomor_ktp_ahliwaris" class="form-control" required=""
                                                               oninvalid="this.setCustomValidity('Nomor KTP tidak boleh kosong')"
                                                               oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Nomor Telepon/HP
                                                            <label style="color: red">*</label>
                                                        </div>
                                                        <input type="text" name="telepon_ahliwaris" class="form-control" required=""
                                                               oninvalid="this.setCustomValidity('Nomor telp tidak boleh kosong')"
                                                               oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Alamat Ahliwaris
                                                            <label style="color: red">*</label>
                                                        </div>
                                                        <input type="text" name="alamat_ahliwaris" class="form-control" required=""
                                                               oninvalid="this.setCustomValidity('Alamat tidak boleh kosong')"
                                                               oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            RT Ahliwaris
                                                            <label style="color: red">*</label>
                                                        </div>
                                                        <input type="number" min="1" name="rt_ahliwaris" class="form-control" required=""
                                                               oninvalid="this.setCustomValidity('RT tidak boleh kosong')"
                                                               oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            RW Ahliwaris
                                                            <label style="color: red">*</label>
                                                        </div>
                                                        <input type="number" min="1" name="rw_ahliwaris" class="form-control" required=""
                                                               oninvalid="this.setCustomValidity('RW tidak boleh kosong')"
                                                               oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Kelurahan Ahli Waris
                                                            <label style="color: red">*</label>
                                                        </div>
                                                        <input type="text" name="kelurahan_ahliwaris" class="form-control" required=""
                                                               oninvalid="this.setCustomValidity('Kelurahan tidak boleh kosong')"
                                                               oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Kecamatan Ahli Waris
                                                            <label style="color: red">*</label>
                                                        </div>
                                                        <input type="text" name="kecamatan_ahliwaris" class="form-control" required=""
                                                               oninvalid="this.setCustomValidity('Kecamatan tidak boleh kosong')"
                                                               oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-12">
                                                    <div class="input-group">
                                                        <div class="input-group-addon" style="color: black">
                                                            Kota Ahli Waris
                                                            <label style="color: red">*</label>
                                                        </div>
                                                        <input type="text" name="kota_ahliwaris" class="form-control" required=""
                                                               oninvalid="this.setCustomValidity('Kota tidak boleh kosong')"
                                                               oninput="setCustomValidity('')">
                                                    </div>
                                                </div>
                                            </div>
                                            <div hidden>
                                                <input type="text" value="" id="iptm_id" name="iptm_id">
                                            </div>
                                            <div class="text-right">
                                                <button class="btn btn-secondary" id="cancelSubmit" type="button"><i class="fa fa-times"></i> Batal</button>
                                                <button class="btn btn-success" id="SubmitAndSaveBtn" type="button"><i class="fa fa-save"></i> Simpan dan Cetak Surat</button>
                                                <button type="button" id="ShowModalConfirm" data-toggle="modal" data-target="#ModalConfirmData" hidden></button>
                                            </div>

                                            <div class="modal fade" id="ModalConfirmData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Cetak Surat Tumpangan</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            Apakah anda yakin akan mencetak dokumen tumpangan ini? <br>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <input type="text" id="operation_type" name="operation_type" value="cetak_iptm_tumpangan" hidden>
                                                            <input type="text" id="id_pemakaman" name="id_pemakaman" value="{{$pemakamanName->id}}" hidden>
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                                                            <button type="submit" id="confirmPrint" class="btn btn-primary">Ya, Cetak</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <br/>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script>
        $("#cekIPTM").click(function () {
            var loadIcon = $("#load-icon");
            loadIcon.addClass("do-spin");
            $(".btn").attr("disabled", "disabled");

            setTimeout(function(){
                CheckIPTM("#filterNomor", "#filterNama");
            }, 1000);
        });

        $("#cancelSubmit").click(function(){
            $(".dataSection").removeAttr("hidden");
            $(".submitSection").attr("hidden", "hidden");
        });

        $("#SubmitAndSaveBtn").click(function(){

            var nullRequiredField = false;

            $("#formTumpangan").find(':input').each(function(){
                if($(this).attr("required")){
                    if($(this).val() === null || $(this).val() === undefined || $(this).val() === "" ){
                        nullRequiredField = true;
                        return false;
                    }
                }
            });

            if(nullRequiredField){
                $("#confirmPrint").click();
            }else{
                $("#ShowModalConfirm").click();
            }
//            $("#ModalConfirmData").modal("hide");
//
        });

        function CheckIPTM(noIptmField, namaAlmarhumField){
            var noIptm = $(noIptmField).val();
            var namaAlmarhum = $(namaAlmarhumField).val();

            if(noIptm === "" && namaAlmarhum === ""){
                $(".dataSection").removeAttr("hidden");
                $(".submitSection").attr("hidden", "hidden");
                $("#iptm_id").val(null);

                $("#jsonResult").html("<tr>" +
                    "<td class='text-center' colspan='5'>Nomor IPTM Tidak Ditemukan. mohon diperiksa kembali nomor IPTM anda</td>" +
                    "</tr>");
            }
            else
            {
                var urlReq = "/json/iptm?noiptm=" + noIptm + "&namaAlmarhum=" + namaAlmarhum;

                $.ajax({'url': urlReq,
                    'type' : 'GET',
                    success:function(result){
                        let data = JSON.parse(result);
                        if(data.length > 0){

                            $("#jsonResult").html(function () {
                                var tRow = "";
                                for(var i=0; i<data.length; i++){
                                    tRow +=
                                        "<tr>"+
                                        "<td>"+data[i].nomor_iptm+"</td>"+
                                        "<td>"+data[i].nama_almarhum+"</td>"+
                                        "<td>"+data[i].nama_ahliwaris+"</td>"+
                                        /*"<td>" +
                                        "<a class='btn btn-secondary' href='"+ '{{ url('lihatMakam') }}' + '/' + data[i].makam_id +"' target='_blank'>Lihat Detail Makam</a>" +
                                        "</td>"+*/
                                        "<td>" +
                                        "<button class='btn btn-success' onclick='ProsesIPTM("+JSON.stringify(data[i])+")'><i class='fa fa-plus'></i> Proses</button>" +
                                        "</td>"+
                                        "</tr>";
                                }
                                return tRow;
                            });

                            $(".dataSection").removeAttr("hidden");
                            $(".submitSection").attr("hidden", "hidden");
                            $("#iptm_id").val(data[0].id);
                        }
                        else{
                            $(".dataSection").removeAttr("hidden");
                            $(".submitSection").attr("hidden", "hidden");
                            $("#iptm_id").val(null);

                            $("#jsonResult").html("<tr>" +
                                "<td class='text-center' colspan='5'>Nomor IPTM Tidak Ditemukan. mohon diperiksa kembali nomor IPTM anda</td>" +
                                "</tr>");
                        }
                    }
                });
            }
            var loadIcon = $("#load-icon");
            loadIcon.removeClass("do-spin");
            $(".btn").removeAttr("disabled");
        }

        function ProsesIPTM(obj){
            console.log(obj);
            var keyLength = Object.keys(obj).length;
            var keyCollection = Object.keys(obj);

            for(var i = 0; i<keyLength; i++){
                $("#"+ keyCollection[i]).val(obj[keyCollection[i]]);
            }

            $(".dataSection").attr("hidden", "hidden");
            $(".submitSection").removeAttr("hidden");
        }
    </script>
@endsection
