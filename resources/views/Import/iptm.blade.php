@extends('layouts.app') @section('content')
    <div class="">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Pengelolaan IPTM</strong>
                        </div>
                        <div class="card-body">

                          {{-- notifikasi form validasi --}}
                          	@if ($errors->has('file'))
                          		<span class="invalid-feedback" role="alert">
                          			<strong>{{ $errors->first('file') }}</strong>
                          		</span>
                          		@endif

                          		{{-- notifikasi sukses --}}
                          	@if ($sukses = Session::get('sukses'))
                          		<div class="alert alert-success alert-block">
                          			<button type="button" class="close" data-dismiss="alert">×</button>
                          			<strong>{{ $sukses }}</strong>
                          		</div>
                          	@endif

                            <div>
                              <button type="button" class="btn btn-primary mr-5" data-toggle="modal" data-target="#importExcel" style="position: absolute; right: 10px; top: 5px;">
                                IMPORT EXCEL
                              </button>
                            </div>

                          <!-- <a href="/siswa/export_excel" class="btn btn-success my-3" target="_blank">EXPORT EXCEL</a> -->

                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <td>Nomor IPTM</td>
                                    <td>Tanggal IPTM</td>
                                    <td>Masa Berlaku</td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($iptm as $d)
                                    <tr>
                                        <td>{{ $d->nomor_iptm }}</td>
                                        <td>{{ $d->tanggal_iptm }}</td>
                                        <td>{{ $d->masa_berlaku }}</td>
                                        <td>
                                            <a href="#" data-target="#editData-{{$d->id}}" data-toggle="modal" class="btn-edit btn btn-primary"><i class="fa fa-edit"> Edit</i></a>
                                           {{-- <a href="#" data-target="#deleteData-{{$d->id}}" data-toggle="modal" class="btn-danger btn btn-primary"><i class="fa fa-trash-o"></i></a>--}}
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="editData-{{$d->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false">
                                      <div class="modal-dialog" role="document">
                                        <form method="post" action="/import/iptm/edit/{{$d->id}}" enctype="multipart/form-data">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="exampleModalLabel">Edit Data IPTM</h5>
                                            </div>
                                            <div class="modal-body">
                                              {{method_field('PATCH')}}
                                              {{ csrf_field() }}

                                              <div class="form-group">
                                                  <label for="nomor_iptm" class=" form-control-label">Nomor IPTM</label>
                                                  <input type="text" name="nomor_iptm" placeholder="nomor_iptm" class="form-control" value="{{ $d->nomor_iptm }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="tanggal_iptm" class=" form-control-label">Tanggal IPTM</label>
                                                  <input type="date" name="tanggal_iptm" placeholder="tanggal_iptm" class="form-control" value="{{ $d->tanggal_iptm }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="masa_berlaku" class=" form-control-label">Petak</label>
                                                  <input type="date" name="masa_berlaku" placeholder="masa_berlaku" class="form-control" value="{{ $d->masa_berlaku }}">
                                              </div>

                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                              <button type="submit" class="btn btn-primary">Ubah</button>
                                            </div>
                                          </div>
                                        </form>
                                      </div>
                                    </div>
                                    <div class="modal fade" id="deleteData-{{$d->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false">
                                			<div class="modal-dialog" role="document">
                                				<form method="post" action="/import/iptm/delete/{{$d->id}}" enctype="multipart/form-data">
                                					<div class="modal-content">
                                						<div class="modal-header">
                                							<h5 class="modal-title" id="exampleModalLabel">Hapus Data AhliWaris</h5>
                                						</div>
                                						<div class="modal-body">
                                              {{method_field('DELETE')}}
                                							{{ csrf_field() }}
                                              <h4>Apa anda yakin ingin menghapus data ini?</h4>
                                						</div>
                                						<div class="modal-footer">
                                							<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                							<button type="submit" class="btn btn-primary">Hapus</button>
                                						</div>
                                					</div>
                                				</form>
                                			</div>
                                		</div>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Import Excel -->
		<div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="post" action="/import/iptm" enctype="multipart/form-data">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
						</div>
						<div class="modal-body">
              {{method_field('PUT')}}
							{{ csrf_field() }}

              <p>Berikut ini adalah bentuk format excel untuk pemasukan data <a href="{{url('download\format_excel_untuk_import.xlsx')}}" class="fa fa-download">Download</a></p>

							<label>Pilih file excel</label>
							<div class="form-group">
								<input type="file" name="file" required="required">
							</div>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Import</button>
						</div>
					</div>
				</form>
			</div>
		</div>
@endsection
