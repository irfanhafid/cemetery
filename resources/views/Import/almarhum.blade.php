@extends('layouts.app') @section('content')
    <div class="">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Pengelolaan Almarhum</strong>
                        </div>
                        <div class="card-body">

                          {{-- notifikasi form validasi --}}
                          	@if ($errors->has('file'))
                          		<span class="invalid-feedback" role="alert">
                          			<strong>{{ $errors->first('file') }}</strong>
                          		</span>
                          		@endif

                          		{{-- notifikasi sukses --}}
                          	@if ($sukses = Session::get('sukses'))
                          		<div class="alert alert-success alert-block">
                          			<button type="button" class="close" data-dismiss="alert">×</button>
                          			<strong>{{ $sukses }}</strong>
                          		</div>
                          	@endif

                            <div>
                              <button type="button" class="btn btn-primary mr-5" data-toggle="modal" data-target="#importExcel" style="position: absolute; right: 10px; top: 5px;">
                                IMPORT EXCEL
                              </button>
                            </div>

                          <!-- <a href="/siswa/export_excel" class="btn btn-success my-3" target="_blank">EXPORT EXCEL</a> -->

                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <td>Nama AhliWaris</td>
                                    <td>IPTM</td>
                                    <td>Nama Almarhum</td>
                                    <td>Tanggal Wafat</td>
                                    <td>Nomor KTP Almarhum</td>
                                    <td>Nomor KK Almarhum</td>
                                    <td>Nomor SP RT RW</td>
                                    <td>Tanggal SP RT RW</td>
                                    <td>Nomor SK RS</td>
                                    <td>Tanggal SK RS</td>
                                    <td>Nomor SK Kelurahan</td>
                                    <td>Tanggal SK Kelurahan</td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($almarhum as $d)
                                    <tr>
                                        <td>{{ !empty($d->nama_ahliwaris) ? $d->nama_ahliwaris:'' }}</td>
                                        <td>{{ !empty($d->nomor_iptm) ? $d->nomor_iptm:'' }}</td>
                                        <td>{{ !empty($d->nama_almarhum) ? $d->nama_almarhum:'' }}</td>
                                        <td>{{ !empty($d->tanggal_wafat) ? $d->tanggal_wafat:''}}</td>
                                        <td>{{ !empty($d->nomor_ktp_almarhum) ? $d->nomor_ktp_almarhum:'' }}</td>
                                        <td>{{ !empty($d->nomor_kk_almarhum) ? $d->nomor_kk_almarhum:'' }}</td>
                                        <td>{{ !empty($d->nomor_sp_rtrw) ? $d->nomor_sp_rtrw:'' }}</td>
                                        <td>{{ !empty($d->tanggal_sp_rtrw) ? $d->tanggal_sp_rtrw:'' }}</td>
                                        <td>{{ !empty($d->nomor_sk_kematian_rs) ? $d->nomor_sk_kematian_rs:'' }}</td>
                                        <td>{{ !empty($d->tanggal_sk_kematian_rs) ? $d->tanggal_sk_kematian_rs:'' }}</td>
                                        <td>{{ !empty($d->nomor_sk_kematian_kelurahan) ? $d->nomor_sk_kematian_kelurahan:'' }}</td>
                                        <td>{{ !empty($d->tanggal_sk_kematian_kelurahan) ? $d->tanggal_sk_kematian_kelurahan:'' }}</td>
                                        <td>
                                            <a href="#" data-target="#editData-{{$d->id}}" data-toggle="modal" class="btn-edit btn btn-primary"><i class="fa fa-edit"> Edit</i></a>
                                            {{--<a href="#" data-target="#deleteData-{{$d->id}}" data-toggle="modal" class="btn-danger btn btn-primary"><i class="fa fa-trash-o"></i></a>--}}
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="editData-{{$d->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false">
                                      <div class="modal-dialog" role="document">
                                        <form method="post" action="/import/almarhum/edit/{{$d->id}}" enctype="multipart/form-data">
                                          <div class="modal-content">
                                            <div class="modal-header">
                                              <h5 class="modal-title" id="exampleModalLabel">Edit Data Almarhum</h5>
                                            </div>
                                            <div class="modal-body">
                                              {{method_field('PATCH')}}
                                              {{ csrf_field() }}

                                              <div class="form-group">
                                                  <label for="nama_ahliwaris" class=" form-control-label">Nama AhliWaris</label>
                                                  <input type="text" name="nama_ahliwaris" placeholder="nama_ahliwaris" class="form-control" value="{{ $d->nama_ahliwaris }}" readonly>
                                              </div>
                                              <div class="form-group">
                                                  <label for="nomor_iptm" class=" form-control-label">Nomor IPTM</label>
                                                  <input type="text" name="nomor_iptm" placeholder="nomor_iptm" class="form-control" value="{{ $d->nomor_iptm }}" readonly>
                                              </div>
                                              <div class="form-group">
                                                  <label for="nama_almarhum" class=" form-control-label">Nama Almarhum</label>
                                                  <input type="text" name="nama_almarhum" placeholder="nama_almarhum" class="form-control" value="{{ $d->nama_almarhum }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="tanggal_wafat" class=" form-control-label">Tanggal Wafat</label>
                                                  <input type="date" name="tanggal_wafat" placeholder="tanggal_wafat" class="form-control" value="{{ $d->tanggal_wafat }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="nomor_ktp_almarhum" class=" form-control-label">Nomor KTP Almarhum</label>
                                                  <input type="text" name="nomor_ktp_almarhum" placeholder="nomor_ktp_almarhum" class="form-control" value="{{ $d->nomor_ktp_almarhum }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="nomor_kk_almarhum" class=" form-control-label">Nomor KK Almarhum</label>
                                                  <input type="text" name="nomor_kk_almarhum" placeholder="nomor_kk_almarhum" class="form-control" value="{{ $d->nomor_kk_almarhum }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="nomor_sp_rtrw" class=" form-control-label">Nomor SP RT RW</label>
                                                  <input type="text" name="nomor_sp_rtrw" placeholder="nomor_sp_rtrw" class="form-control" value="{{ $d->nomor_sp_rtrw }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="tanggal_sp_rtrw" class=" form-control-label">Tanggal SP RT RW</label>
                                                  <input type="date" name="tanggal_sp_rtrw" placeholder="tanggal_sp_rtrw" class="form-control" value="{{ $d->tanggal_sp_rtrw }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="nomor_sk_kematian_rs" class=" form-control-label">Nomor SK Kematian RS</label>
                                                  <input type="text" name="nomor_sk_kematian_rs" placeholder="nomor_sk_kematian_rs" class="form-control" value="{{ $d->nomor_sk_kematian_rs }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="tanggal_sk_kematian_rs" class=" form-control-label">Tanggal SK Kematian RS</label>
                                                  <input type="date" name="tanggal_sk_kematian_rs" placeholder="tanggal_sk_kematian_rs" class="form-control" value="{{ $d->tanggal_sk_kematian_rs }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="nomor_sk_kematian_kelurahan" class=" form-control-label">Nomor SK Kematian Kelurahan</label>
                                                  <input type="text" name="nomor_sk_kematian_kelurahan" placeholder="nomor_sk_kematian_kelurahan" class="form-control" value="{{ $d->nomor_sk_kematian_kelurahan }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="tanggal_sk_kematian_kelurahan" class=" form-control-label">Tanggal SK Kematian Kelurahan</label>
                                                  <input type="date" name="tanggal_sk_kematian_kelurahan" placeholder="tanggal_sk_kematian_kelurahan" class="form-control" value="{{ $d->tanggal_sk_kematian_kelurahan }}">
                                              </div>
                                            </div>
                                            <div class="modal-footer">
                                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                              <button type="submit" class="btn btn-primary">Ubah</button>
                                            </div>
                                          </div>
                                        </form>
                                      </div>
                                    </div>
                                    <div class="modal fade" id="deleteData-{{$d->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false">
                                			<div class="modal-dialog" role="document">
                                				<form method="post" action="/import/almarhum/delete/{{$d->id}}" enctype="multipart/form-data">
                                					<div class="modal-content">
                                						<div class="modal-header">
                                							<h5 class="modal-title" id="exampleModalLabel">Hapus Data AhliWaris</h5>
                                						</div>
                                						<div class="modal-body">
                                              {{method_field('DELETE')}}
                                							{{ csrf_field() }}
                                              <h4>Apa anda yakin ingin menghapus data ini?</h4>
                                						</div>
                                						<div class="modal-footer">
                                							<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                							<button type="submit" class="btn btn-primary">Hapus</button>
                                						</div>
                                					</div>
                                				</form>
                                			</div>
                                		</div>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Import Excel -->
		<div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="post" action="/import/almarhum" enctype="multipart/form-data">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
						</div>
						<div class="modal-body">
              {{method_field('PUT')}}
							{{ csrf_field() }}

              <p>Berikut ini adalah bentuk format excel untuk pemasukan data <a href="{{url('download\format_excel_untuk_import.xlsx')}}" class="fa fa-download">Download</a></p>

							<label>Pilih file excel</label>
							<div class="form-group">
								<input type="file" name="file" required="required">
							</div>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Import</button>
						</div>
					</div>
				</form>
			</div>
		</div>
@endsection
