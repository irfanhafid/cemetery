@extends('layouts.app') @section('content')
    <div class="">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Pengelolaan Almarhum</strong>
                        </div>
                        <div class="card-body">

                          {{-- notifikasi form validasi --}}
                          	@if ($errors->has('file'))
                          		<span class="invalid-feedback" role="alert">
                          			<strong>{{ $errors->first('file') }}</strong>
                          		</span>
                          		@endif

                          		{{-- notifikasi sukses --}}
                          	@if ($sukses = Session::get('sukses'))
                          		<div class="alert alert-success alert-block">
                          			<button type="button" class="close" data-dismiss="alert">×</button>
                          			<strong>{{ $sukses }}</strong>
                          		</div>
                          	@endif

                          <div>
                            <button type="button" class="btn btn-primary mr-5" data-toggle="modal" data-target="#importExcel" style="position: absolute; right: 10px; top: 5px;">
                              IMPORT EXCEL
                            </button>
                          </div>

                          <!-- <a href="/siswa/export_excel" class="btn btn-success my-3" target="_blank">EXPORT EXCEL</a> -->

                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <td>Nomor KTP AhliWaris</td>
                                    <td>Nama AhliWaris</td>
                                    <td>Alamat AhliWaris</td>
                                    <td>RT AhliWaris</td>
                                    <td>RW AhliWaris</td>
                                    <td>Kelurahan AhliWaris</td>
                                    <td>Kecamatan AhliWaris</td>
                                    <td>Kota AhliWaris</td>
                                    <td>Telepon AhliWaris</td>
                                    <td>Hubungan AhliWaris</td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($ahliwaris as $d)
                                    <tr>
                                        <td>{{ $d->nomor_ktp_ahliwaris }}</td>
                                        <td>{{ $d->nama_ahliwaris }}</td>
                                        <td>{{ $d->alamat_ahliwaris }}</td>
                                        <td>{{ $d->rt_ahliwaris }}</td>
                                        <td>{{ $d->rw_ahliwaris }}</td>
                                        <td>{{ $d->kelurahan_ahliwaris }}</td>
                                        <td>{{ $d->kecamatan_ahliwaris }}</td>
                                        <td>{{ $d->kota_ahliwaris }}</td>
                                        <td>{{ $d->telepon_ahliwaris }}</td>
                                        <td>{{ $d->hubungan_ahliWaris }}</td>
                                        <td>
                                            <a href="#" data-target="#editData-{{$d->id}}" data-toggle="modal" class="btn-edit btn btn-primary"><i class="fa fa-edit"> <Edit></Edit></i></a>
                                            {{--<a href="#" data-target="#deleteData-{{$d->id}}" data-toggle="modal" class="btn-danger btn btn-primary"><i class="fa fa-trash-o"></i></a>--}}
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="editData-{{$d->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false">
                                			<div class="modal-dialog" role="document">
                                				<form method="post" action="/import/ahliwaris/edit/{{$d->id}}" enctype="multipart/form-data">
                                					<div class="modal-content">
                                						<div class="modal-header">
                                							<h5 class="modal-title" id="exampleModalLabel">Edit Data AhliWaris</h5>
                                						</div>
                                						<div class="modal-body">
                                              {{method_field('PATCH')}}
                                							{{ csrf_field() }}

                                              <div class="form-group">
                                                  <label for="nomor_ktp_ahliwaris" class=" form-control-label">Nomor KTP AhliWaris</label>
                                                  <input type="text" name="nomor_ktp_ahliwaris" placeholder="nomor_ktp_ahliwaris" class="form-control" value="{{ $d->nomor_ktp_ahliwaris }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="nama_ahliwaris" class=" form-control-label">Nama AhliWaris</label>
                                                  <input type="text" name="nama_ahliwaris" placeholder="nama_ahliwaris" class="form-control" value="{{ $d->nama_ahliwaris }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="alamat_ahliwaris" class=" form-control-label">Alamat Ahliwaris</label>
                                                  <input type="text" name="alamat_ahliwaris" placeholder="alamat_ahliwaris" class="form-control" value="{{ $d->alamat_ahliwaris }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="rt_ahliwaris" class=" form-control-label">RT Ahliwaris</label>
                                                  <input type="text" name="rt_ahliwaris" placeholder="rt_ahliwaris" class="form-control" value="{{ $d->rt_ahliwaris }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="rw_ahliwaris" class=" form-control-label">RW Ahliwaris</label>
                                                  <input type="text" name="rw_ahliwaris" placeholder="rw_ahliwaris" class="form-control" value="{{ $d->rw_ahliwaris }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="kelurahan_ahliwaris" class=" form-control-label">Kelurahan Ahliwaris</label>
                                                  <input type="text" name="kelurahan_ahliwaris" placeholder="kelurahan_ahliwaris" class="form-control" value="{{ $d->kelurahan_ahliwaris }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="kecamatan_ahliwaris" class=" form-control-label">Kecamatan Ahliwaris</label>
                                                  <input type="text" name="kecamatan_ahliwaris" placeholder="kecamatan_ahliwaris" class="form-control" value="{{ $d->kecamatan_ahliwaris }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="kota_ahliwaris" class=" form-control-label">Kota Ahliwaris</label>
                                                  <input type="text" name="kota_ahliwaris" placeholder="kota_ahliwaris" class="form-control" value="{{ $d->kota_ahliwaris }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="telepon_ahliwaris" class=" form-control-label">Telepon Ahliwaris</label>
                                                  <input type="text" name="telepon_ahliwaris" placeholder="telepon_ahliwaris" class="form-control" value="{{ $d->telepon_ahliwaris }}">
                                              </div>
                                              <div class="form-group">
                                                  <label for="hubungan_ahliwaris" class=" form-control-label">Hubungan Ahliwaris</label>
                                                  <input type="text" name="hubungan_ahliWaris" placeholder="hubungan_ahliwaris" class="form-control" value="{{ $d->hubungan_ahliWaris }}">
                                              </div>
                                						</div>
                                						<div class="modal-footer">
                                							<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                							<button type="submit" class="btn btn-primary">Ubah</button>
                                						</div>
                                					</div>
                                				</form>
                                			</div>
                                		</div>
                                    <div class="modal fade" id="deleteData-{{$d->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false">
                                			<div class="modal-dialog" role="document">
                                				<form method="post" action="/import/ahliwaris/delete/{{$d->id}}" enctype="multipart/form-data">
                                					<div class="modal-content">
                                						<div class="modal-header">
                                							<h5 class="modal-title" id="exampleModalLabel">Hapus Data AhliWaris</h5>
                                						</div>
                                						<div class="modal-body">
                                              {{method_field('DELETE')}}
                                							{{ csrf_field() }}
                                              <h4>Apa anda yakin ingin menghapus data ini?</h4>
                                						</div>
                                						<div class="modal-footer">
                                							<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                							<button type="submit" class="btn btn-primary">Hapus</button>
                                						</div>
                                					</div>
                                				</form>
                                			</div>
                                		</div>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Import Excel -->
		<div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="post" action="/import/ahliwaris" enctype="multipart/form-data">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
						</div>
						<div class="modal-body">
              {{method_field('PUT')}}
							{{ csrf_field() }}

							<label>Pilih file excel</label>
							<div class="form-group">
								<input type="file" name="file" required="required">
							</div>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Import</button>
						</div>
					</div>
				</form>
			</div>
		</div>
@endsection
