@extends((( Auth::user()->role == 'admin_tpu'|| Auth::user()->role =='admin_dinas') ? 'layouts.app' : 'layouts.user.app' ))
@if(Auth::user()->role == 'member')
@section('header-class')
    {{"main-header-area"}}
@endsection
@endif
@section('content')

@if(Auth::user()->role == 'member')
<div class="slider_area">
    <div class=" d-flex align-items-center "style="background-color: #2952a3;height: 200px; background-size: cover;background-repeat: no-repeat"></div>
</div>
    <br>
    <div class="container well">
        @if(session('alert_update'))
            <div class="alert alert-success text-center">
                <br>
                <h4>{{session('alert_update')}}</h4>
            </div>
        @endif
            <br>
        <div class="text-md-left">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <form action="{{url('/profile/edit')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            @if(Auth::user()->images != "")
                                <img src="/images/profile/{{Auth::user()->images}}" width="100%" alt="">
                            @else
                                <img src="/images/no-image-available.jpg" width="100%" alt="">
                            @endif
                            <input type="file" id="images" name="images" class="form-control" >
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label style="color: black">
                                NAMA LENGKAP
                                <label style="color: red">*</label>
                            </label>
                            <input type="text" class="form-control" name="fullname" value="{{Auth::user()->fullname}}">
                            @if($errors->has('fullname'))
                                <small class="help-block form-text" style="color: red">{{ $errors->first('fullname') }}</small>
                            @endif
                        </div>
                        {{--<div class="form-group">
                            <label>ALAMAT EMAIL</label>
                            <input type="text" class="form-control" name="email" value="{{Auth::user()->email}}">
                        </div>--}}
                        <div class="form-group">
                            <label>ALAMAT</label>
                            <textarea name="address" id="" cols="30" rows="5" name="address" class="form-control">{{Auth::user()->address}}</textarea>
                        </div>
                        <br>
                    </div>
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30"><i class="fa fa-save"></i> Simpan</button>
                        <a href="/profile">
                            <button type="button" class="btn btn-danger">Batal</button>
                        </a>
                        <br><br><br>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @elseif(Auth::user()->role == 'admin_tpu'||'admin_dinas')
    <div class="">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Profil</strong>
                        </div>
                        <div class="card-body">
                          @if(session('alert_update'))
                              <div class="alert alert-success text-center">
                                  <br>
                                  <h4>{{session('alert_update')}}</h4>
                              </div>
                          @endif
                              <br>
                          <div class="text-md-left">
                              @if(session()->has('success'))
                                  <div class="alert alert-success">
                                      {{ session()->get('success') }}
                                  </div>
                              @endif
                              <form action="{{url('/profile/edit')}}" method="POST" enctype="multipart/form-data">
                                  @csrf
                                  <div class="row">
                                      <div class="col-sm-4">
                                          <div class="form-group">
                                              @if(Auth::user()->images != "")
                                                  <img src="/images/profile/{{Auth::user()->images}}" width="100%" alt="">
                                              @else
                                                  <img src="/images/no-image-available.jpg" width="100%" alt="">
                                              @endif
                                              <input type="file" id="images" name="images" class="form-control" >
                                          </div>
                                      </div>
                                      <div class="col-sm-8">
                                          <div class="form-group">
                                              <label style="color: black">
                                                  NAMA LENGKAP
                                                  <label style="color: red">*</label>
                                              </label>
                                              <input type="text" class="form-control" name="fullname" value="{{Auth::user()->fullname}}">
                                              @if($errors->has('fullname'))
                                                  <small class="help-block form-text" style="color: red">{{ $errors->first('fullname') }}</small>
                                              @endif
                                          </div>
                                          {{--<div class="form-group">
                                              <label>ALAMAT EMAIL</label>
                                              <input type="text" class="form-control" name="email" value="{{Auth::user()->email}}">
                                          </div>--}}
                                          <div class="form-group">
                                              <label style="color: black">ALAMAT</label>
                                              <textarea name="address" id="" cols="30" rows="5" name="address" class="form-control">{{Auth::user()->address}}</textarea>
                                          </div>
                                          <br>
                                      </div>
                                      <div class="col-sm-12 text-center">
                                          <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30"><i class="fa fa-save"></i> Simpan</button>
                                          <a href="/profile">
                                              <button type="button" class="btn btn-danger">Batal</button>
                                          </a>
                                          <br><br><br>
                                      </div>
                                  </div>
                              </form>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection
