@extends((( Auth::user()->role == 'admin_tpu'|| Auth::user()->role =='admin_dinas') ? 'layouts.app' : 'layouts.user.app' ))
@if(Auth::user()->role == 'member')
@section('header-class')
    {{"main-header-area"}}
@endsection
@endif
@section('content')

@if(Auth::user()->role == 'member')
<div class="slider_area">
    <div class=" d-flex align-items-center "style="background-color: #2952a3;height: 200px; background-size: cover;background-repeat: no-repeat"></div>
</div>
<br>
    <div class="container well">
        @if(session('alert_update'))
            <div class="alert alert-success text-center">
                <br>
                <h4>{{session('alert_update')}}</h4>
            </div>
        @endif
        <div class="text-md-left">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        @if(Auth::user()->images != "")
                            <img src="/images/profile/{{Auth::user()->images}}" width="100%" alt="">
                        @else
                            <img src="/images/no-image-available.jpg" width="100%" alt="">
                        @endif
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <label>NAMA LENGKAP</label>
                        <input type="text" class="form-control" value="{{Auth::user()->fullname}}" disabled="disabled">
                    </div>
                    <div class="form-group">
                        <label>ALAMAT EMAIL</label>
                        <input type="text" class="form-control" value="{{Auth::user()->email}}" disabled="disabled">
                    </div>
                    <div class="form-group">
                        <label>ALAMAT</label>
                        <textarea name="address" id="" cols="30" rows="5" class="form-control" disabled="disabled">{{Auth::user()->address}}</textarea>
                    </div>
                </div>
                <div class="col-sm-12 text-center">
                    <a href="{{url('/profile/edit')}}"><button class="btn btn-info"><span class="fa fa-gears"></span> Ubah Profil </button></a>
                </div>
            </div>
        </div>
    </div>
    @elseif(Auth::user()->role == 'admin_tpu'||'admin_dinas')
    <div class="">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Profil</strong>
                        </div>
                        <div class="card-body">

                          <div class="container well">
                              @if(session('alert_update'))
                                  <div class="alert alert-success text-center">
                                      <br>
                                      <h4>{{session('alert_update')}}</h4>
                                  </div>
                              @endif
                              <div class="text-md-left">
                                  <div class="row">
                                      <div class="col-sm-4">
                                          <div class="form-group">
                                              @if(Auth::user()->images != "")
                                                  <img src="/images/profile/{{Auth::user()->images}}" width="100%" alt="">
                                              @else
                                                  <img src="/images/no-image-available.jpg" width="100%" alt="">
                                              @endif
                                          </div>
                                      </div>
                                      <div class="col-sm-8">
                                          <div class="form-group">
                                              <label>NAMA LENGKAP</label>
                                              <input type="text" class="form-control" value="{{Auth::user()->fullname}}" disabled="disabled">
                                          </div>
                                          {{--<div class="form-group">
                                              <label>ALAMAT EMAIL</label>
                                              <input type="text" class="form-control" value="{{Auth::user()->email}}" disabled="disabled">
                                          </div>--}}
                                          <div class="form-group">
                                              <label>ALAMAT</label>
                                              <textarea name="address" id="" cols="30" rows="5" class="form-control" disabled="disabled">{{Auth::user()->address}}</textarea>
                                          </div>
                                      </div>
                                      <div class="col-sm-12 text-center">
                                          <a href="{{url('/profile/edit')}}"><button class="btn btn-info"><span class="fa fa-gears"></span> Ubah Profil </button></a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection
