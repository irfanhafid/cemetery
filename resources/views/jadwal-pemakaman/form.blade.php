<div class="col-md-6">
    <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
        <h5>Data yang memiliki tanda ( <label style="color: red">*</label> ) tidak boleh kosong</h5>
        <hr style="solid-color: black">
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="" class=" form-control-label" style="color: black">
                    Nomor IPTM
                    <label style="color: red">*</label>
                </label>
            </div>
            <div class="col-12 col-md-9">
                <select name="iptm_id" class="form-control">
                    <option value="">-Select-</option>
                    @foreach(\App\IPTM::pluck('nomor_iptm', 'id') as $key => $nomor)
                        <option value="{{ $key }}" {{ isset($JadwalPemakaman) && $JadwalPemakaman->iptm_id == $key ? 'selected' : '' }}>{{ $nomor }}</option>
                    @endforeach
                </select>
                @if($errors->has('iptm_id'))
                    <small class="help-block form-text" style="color: red">{{ $errors->first('iptm_id') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="" class=" form-control-label" style="color: black">
                    Tanggal Pemakaman
                    <label style="color: red">*</label>
                </label>
            </div>
            <div class="col-12 col-md-9">
                <input type="date" name="tanggal_pemakaman" class="form-control" value="{{ $JadwalPemakaman->tanggal_pemakaman ?? old('tanggal_pemakaman') }}">
                @if($errors->has('tanggal_pemakaman'))
                    <small class="help-block form-text" style="color: red">{{ $errors->first('tanggal_pemakaman') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="" class=" form-control-label" style="color: black">
                    Jam Pemakaman
                    <label style="color: red">*</label>
                </label>
            </div>
            <div class="col-12 col-md-9">
                <input type="time" name="jam_pemakaman" class="form-control" value="{{ $JadwalPemakaman->jam_pemakaman ?? old('jam_pemakaman') }}">
                @if($errors->has('jam_pemakaman'))
                    <small class="help-block form-text" style="color: red">{{ $errors->first('jam_pemakaman') }}</small>
                @endif
            </div>
        </div>
    </form>
</div>
