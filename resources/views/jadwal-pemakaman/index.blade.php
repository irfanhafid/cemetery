@extends((( Auth::user()->role == 'admin_tpu'|| Auth::user()->role =='admin_dinas') ? 'layouts.app' : 'layouts.user.app' ))
@if(Auth::user()->role == 'member')
@section('header-class')
    {{"main-header-area"}}
@endsection
@endif
@section('content')

@if(Auth::user()->role == 'member')
<div class="slider_area">
    <div class=" d-flex align-items-center "style="background-color: #2952a3;height: 200px; background-size: cover;background-repeat: no-repeat"></div>
</div>

<div class=" tab-pane container" role="tabpanel" aria-labelledby="information" style="margin-top: 20px">
    <div class="tab-content">
        <div class="tab-pane fade show active" role="tabpanel" aria-labelledby="pills-detaik-tab">
            <div class=" tab-pane container" role="tabpanel" aria-labelledby="alldetail">
                <div class="card-header">
                    <strong>JADWAL PEMAKAMAN</strong>
                </div>
                <div class="card-body card-block">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                  <td>Nama Almarhum</td>
                                  <td>Tanggal Wafat</td>
                                  <td>Nama Ahli Waris</td>
                                  <td>Tanggal Pemakaman</td>
                                  <td>Jam</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $d)
                                    <tr>
                                      <td>{{$d->iptm->almarhum->nama_almarhum}}</td>
                                      <td>{{$d->iptm->almarhum->tanggal_wafat}}</td>
                                      <td>{{$d->iptm->almarhum->ahliWaris->nama_ahliwaris}}</td>
                                      <td>{{ date('d/m/Y', strtotime($d->tanggal_pemakaman)) }}</td>
                                      <td>{{ $d->jam_pemakaman }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@elseif(Auth::user()->role == 'admin_tpu'||'admin_dinas')
    <div class="">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Jadwal Pemakaman</strong>
                        </div>
                        <div class="card-body">
                            @if(Auth::user()->role == 'admin_tpu')
                                <div class="col-md-12 text-right">
                                    <a href="{{ url('jadwal-pemakaman/create') }}" class="btn-add btn btn-success" style="margin-bottom: 20px"><i class="fa fa-plus"></i> Tambah</a>
                                </div>
                            @endif

                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <td>Nama Almarhum</td>
                                    <td>Tanggal Wafat</td>
                                    <td>Nama Ahli Waris</td>
                                    <td>Tanggal Pemakaman</td>
                                    <td>Jam</td>
                                    @if(Auth::user()->role == 'admin_tpu')
                                    <td></td>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $d)
                                    <tr>
                                        <td>{{$d->iptm->almarhum->nama_almarhum}}</td>
                                        <td>{{$d->iptm->almarhum->tanggal_wafat}}</td>
                                        <td>{{$d->iptm->almarhum->ahliWaris->nama_ahliwaris}}</td>
                                        <td>{{ date('d/m/Y', strtotime($d->tanggal_pemakaman)) }}</td>
                                        <td>{{ $d->jam_pemakaman }}</td>
                                        @if(Auth::user()->role == 'admin_tpu' && Auth::user()->pemakaman_id ==  $d->iptm->makam->pemakaman_id)
                                        <td>
                                            <a href="{{ url("jadwal-pemakaman/$d->id/edit") }}" class="btn-edit btn btn-primary"><i class="fa fa-edit"> Edit</i></a>
                                            <a href="{{ url("jadwal-pemakaman/$d->id/delete") }}" class="btn-danger btn btn-primary"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection
