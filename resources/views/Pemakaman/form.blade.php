<div class="col-md-6">
    <h5>Data dengan tanda ( <label style="color: red">*</label> ) tidak boleh kosong</h5><br>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="" class=" form-control-label" style="color: black">
                Kepala TPU
                <label style="color: red">*</label>
            </label>
        </div>
        <div class="col-12 col-md-9">
            <input type="text" name="kepala_pemakaman" class="form-control" value="{{ $Pemakaman->kepala_pemakaman ?? old('kepala_pemakaman') }}">
            @if($errors->has('kepala_pemakaman'))
                <small class="help-block form-text" style="color: red">{{ $errors->first('kepala_pemakaman') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="" class=" form-control-label" style="color: black">
                NIP Kepala TPU
                <label style="color: red">*</label>
            </label>
        </div>
        <div class="col-12 col-md-9">
            <input type="text" name="nip_kepala_pemakaman" class="form-control" value="{{ $Pemakaman->nip_kepala_pemakaman ?? old('nip_kepala_pemakaman') }}">
            @if($errors->has('nip_kepala_pemakaman'))
                <small class="help-block form-text" style="color: red">{{ $errors->first('nip_kepala_pemakaman') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="" class=" form-control-label" style="color: black">
                Nama TPU
                <label style="color: red">*</label>
            </label>
        </div>
        <div class="col-12 col-md-9">
            <input type="text" name="nama_pemakaman" class="form-control" value="{{ $Pemakaman->nama_pemakaman ?? old('nama_pemakaman') }}" >
            @if($errors->has('nama_pemakaman'))
                <small class="help-block form-text" style="color: red">{{ $errors->first('nama_pemakaman') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="" class=" form-control-label" style="color: black">
                Alamat TPU
                <label style="color: red">*</label>
            </label>
        </div>
        <div class="col-12 col-md-9">
            <input type="text" name="alamat_pemakaman" class="form-control" value="{{ $Pemakaman->alamat_pemakaman ?? old('alamat_pemakaman') }}" >
            @if($errors->has('alamat_pemakaman'))
                <small class="help-block form-text" style="color: red">{{ $errors->first('alamat_pemakaman') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="" class=" form-control-label" style="color: black">
                Kelurahan TPU
                <label style="color: red">*</label>
            </label>
        </div>
        <div class="col-12 col-md-9">
            <input type="text" name="kelurahan_pemakaman" class="form-control" value="{{ $Pemakaman->kelurahan_pemakaman ?? old('kelurahan_pemakaman') }}" >
            @if($errors->has('kelurahan_pemakaman'))
                <small class="help-block form-text" style="color: red">{{ $errors->first('kelurahan_pemakaman') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="" class=" form-control-label" style="color: black">
                Kecamatan TPU
                <label style="color: red">*</label>
            </label>
        </div>
        <div class="col-12 col-md-9">
            <input type="text" name="kecamatan_pemakaman" class="form-control" value="{{ $Pemakaman->kecamatan_pemakaman ?? old('kecamatan_pemakaman') }}" >
            @if($errors->has('kecamatan_pemakaman'))
                <small class="help-block form-text" style="color: red">{{ $errors->first('kecamatan_pemakaman') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="" class=" form-control-label" style="color: black">
                Kota TPU
                <label style="color: red">*</label>
            </label>
        </div>
        <div class="col-12 col-md-9">
            <input type="text" name="kota_pemakaman" class="form-control" value="{{ $Pemakaman->kota_pemakaman ?? old('kota_pemakaman') }}" >
            @if($errors->has('kota_pemakaman'))
                <small class="help-block form-text" style="color: red">{{ $errors->first('kota_pemakaman') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="" class=" form-control-label" style="color: black">
                Provinsi TPU
                <label style="color: red">*</label>
            </label>
        </div>
        <div class="col-12 col-md-9">
            <input type="text" name="provinsi_pemakaman" class="form-control" value="{{ $Pemakaman->provinsi_pemakaman ?? old('provinsi_pemakaman') }}" >
            @if($errors->has('provinsi_pemakaman'))
                <small class="help-block form-text" style="color: red">{{ $errors->first('provinsi_pemakaman') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="" class=" form-control-label" style="color: black">
                Nomor Telp TPU
                <label style="color: red">*</label>
            </label>
        </div>
        <div class="col-12 col-md-9">
            <input type="text" name="telepon_pemakaman" class="form-control" value="{{ $Pemakaman->telepon_pemakaman ?? old('telepon_pemakaman') }}">
            @if($errors->has('telepon_pemakaman'))
                <small class="help-block form-text" style="color: red">{{ $errors->first('telepon_pemakaman') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="" class=" form-control-label" style="color: black">
                Kodepos TPU
                <label style="color: red">*</label>
            </label>
        </div>
        <div class="col-12 col-md-9">
            <input type="text" name="kodepos_pemakaman" class="form-control" value="{{ $Pemakaman->kodepos_pemakaman ?? old('kodepos_pemakaman') }}" >
            @if($errors->has('kodepos_pemakaman'))
                <small class="help-block form-text" style="color: red">{{ $errors->first('kodepos_pemakaman') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="" class=" form-control-label" style="color: black">
                Email TPU
                <label style="color: red">*</label>
            </label>
        </div>
        <div class="col-12 col-md-9">
            <input type="email" name="email_pemakaman" class="form-control" value="{{ $Pemakaman->email_pemakaman ?? old('email_pemakaman') }}" >
            @if($errors->has('email_pemakaman'))
                <small class="help-block form-text" style="color: red">{{ $errors->first('email_pemakaman') }}</small>
            @endif
        </div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="" class=" form-control-label" style="color: black">Jumlah Makam</label>
        </div>
        <div class="col-12 col-md-9">
            <input type="text" name="jumlah_makam" class="form-control" value="{{ $Pemakaman->jumlah_makam ?? old('jumlah_makam') }}" >
        </div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="" class=" form-control-label" style="color: black">Luas TPU</label>
        </div>
        <div class="col-12 col-md-9">
            <input type="text" name="luas_pemakaman" class="form-control" value="{{ $Pemakaman->luas_pemakaman ?? old('luas_pemakaman') }}" >
        </div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="" class=" form-control-label" style="color: black">Informasi TPU</label>
        </div>
        <div class="col-12 col-md-9">
            <textarea name="deskripsi_pemakaman" class="form-control" >{{ $Pemakaman->deskripsi_pemakaman ?? old('deskripsi_pemakaman') }}</textarea>
        </div>
    </div>
    <div class="row form-group">
        <div class="col col-md-3">
            <label for="" class=" form-control-label" style="color: black">
                Foto TPU
                <label style="color: red">*</label>
            </label>
        </div>
        <div class="col-12 col-md-9">
            @php
                $path = isset($Pemakaman->photo_pemakaman) ? asset("images/pemakaman/$Pemakaman->photo_pemakaman") : '';
            @endphp
            <img src="{{ $path }}" alt="" height="100px" style="margin-bottom: 10px">
            <input type="file" name="photo_pemakaman" class="form-control" value="">
            @if($errors->has('photo_pemakaman'))
                <small class="help-block form-text" style="color: red">{{ $errors->first('photo_pemakaman') }}</small>
            @endif
        </div>
    </div>
</div>
