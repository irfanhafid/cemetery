<!---->
@extends('layouts.user.app')
@section('header-class')
    {{"main-header-area"}}
@endsection
@section('content')
    <div class="slider_area">
        <div class=" d-flex align-items-center slider_bg_1"
             style="height: 200px;background-size: cover;background-repeat: no-repeat">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-lg-7 col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider_area_end -->

    <!-- service_area_start  -->
    <div class="service_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section_title text-center mb-90">
                        <span class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".1s"></span>
                        <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".2s">PILIH LOKASI PEMAKAMAN</h3>
                    </div>
                </div>
            </div>

            @if(count($listPemakaman)>0)


                <div class="row">
                    @foreach($listPemakaman as $daftarPemakaman)
                        <div class="col-md-6">
                            <div class="single_service wow fadeInLeft" data-wow-duration="1.2s" data-wow-delay=".5s">
                                <div class="service_icon_wrap text-center">
                                    <!-- <div class="service_icon ">
                                        <img src="/images/assets/svg_icon/service_1.png" alt="">
                                    </div> -->
                                </div>

                                <div class="info text-center">

                                    <h2 style="color:#fff; padding-top:20px">{{$daftarPemakaman->nama_pemakaman}}</h2>
                                    <h3>=================</h3>
                                </div>
                                <div class="service_content">
                                    <ul>
                                        <li>{{$daftarPemakaman->alamat_pemakaman}}</li>
                                        <li>{{$daftarPemakaman->kota_pemakaman}}</li>
                                        <li>{{$daftarPemakaman->provinsi_pemakaman}}</li>
                                        <li>{{$daftarPemakaman->kodepos_pemakaman}}</li>
                                        <li>{{$daftarPemakaman->email_pemakaman}}</li>
                                    </ul>
                                    <div class="apply_btn" style="position: center">
                                        <a href="/pemakaman/details/{{$daftarPemakaman->id}}"
                                           class="boxed-btn3">Lihat</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif

        </div>
    </div>
    <!-- service_area_end  -->


    <!-- about_area_end  -->


@endsection
