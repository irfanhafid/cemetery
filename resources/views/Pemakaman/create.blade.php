@extends('layouts.app')
@section('content')
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ url("pemakaman/create") }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Tambah Data TPU</strong>
                            <a class="btn btn-primary pull-right" href="{{ url('pemakaman') }}" style="border-radius: 100%; margin-left: 20px"><i class="fa fa-arrow-left"></i></a>
                        </div>
                        <div class="card-body">
                            @include('Pemakaman.form')
                        </div>
                        <div class="card-footer text-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
