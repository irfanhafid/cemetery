@extends('layouts.user.app')
@section('header-class')
    {{"main-header-area"}}
@endsection
@section('content')
    <style>
        * {
            box-sizing: border-box;
        }

        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        /* Float four columns side by side */
        .column {
            float: left;
            width: 25%;
            padding: 0 10px;
        }

        /* Remove extra left and right margins, due to padding */
        .row {margin: 0 -5px;}

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Responsive columns */
        @media screen and (max-width: 600px) {
            .column {
                width: 100%;
                display: block;
                margin-bottom: 20px;
            }
        }

        /* Style the counter cards */
        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            padding: 16px;
            text-align: center;
            background-color: #f1f1f1;
        }
        .font-size{
            font-size: 20px;
            margin: 10px;
        }
    </style>
        @if(count($pemakamanumum)>0)
            @php($tpu = $pemakamanumum[0])
            <div class="slider_area">
                <div class=" d-flex align-items-center "style="background-color: #2952a3;height: 200px; background-size: cover;background-repeat: no-repeat"></div>
            </div>
            @if(Auth::check())
                <div class=" tab-pane container" id="information_pemakaman" role="tabpanel" aria-labelledby="information" style="margin-top: 50px">
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-information" role="tabpanel" aria-labelledby="pills-detaik-tab">
                            <div class=" tab-pane container" id="detail_pemakaman" role="tabpanel" aria-labelledby="alldetail">
                                <div class="card-header">
                                    <h2>
                                        Informasi TPU {{ucwords($tpu->nama_pemakaman)}}
                                    </h2>
                                </div>
                                <div class="card-body card-block">
                                    <div class="row">
                                        <div class="col-md-4">
                                            @if($tpu->photo_pemakaman != "")
                                                <img src="/images/pemakaman/{{$tpu->photo_pemakaman}}" width="100%" alt="">
                                            @else
                                                <img src="/images/no-image-available.jpg" width="100%" alt="">
                                            @endif
                                        </div>
                                        <div class="col-md-8 ">
                                            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                                <div class="row form-group">
                                                    <div class="col col-md-6">
                                                        <label class=" form-control-label font-size">Nama pemakaman</label>
                                                    </div>
                                                    <div class="col col-md-6 ">
                                                        <p class="font-size">{{$tpu->nama_pemakaman}}</p>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-6">
                                                        <label for="text-input" class=" form-control-label font-size">Alamat Pemakaman</label>
                                                    </div>
                                                    <div class="col col-md-6">
                                                        <p class="font-size">{{$tpu->alamat_pemakaman}},{{$tpu->kota_pemakaman}},{{$tpu->provinsi_pemakaman}},{{$tpu->kodepos_pemakaman}}</p>
                                                    </div>

                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-6">
                                                        <label for="textearea-input " class=" form-control-label font-size">Email Pemakaman</label>
                                                    </div>
                                                    <div class="col col-md-6">
                                                        <p class="font-size">{{$tpu->email_pemakaman}}</p>
                                                    </div>

                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-6">
                                                        <label for="textearea-input" class=" form-control-label font-size">Jumlah Makam :</label>
                                                    </div>
                                                    <div class="col col-md-6">
                                                        <p class="font-size">{{$tpu->jumlah_makam}}</p>
                                                    </div>

                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-6">
                                                        <label for="text-input" class=" form-control-label font-size">Luas Pemakaman</label>
                                                    </div>
                                                    <div class="col col-md-6">
                                                        <p class="font-size">{{$tpu->luas_pemakaman}}</p>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-6">
                                                        <label for="text-input" class=" form-control-label font-size">Deskripsi Pemakaman</label>
                                                    </div>
                                                    <div class="col col-md-6">
                                                        <p class="font-size">{{$tpu->deskripsi_pemakaman}}</p>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="service_area" style="padding-bottom: 0;">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="section_title text-center mb-90">
                                    <span class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".1s"></span>
                                    <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".2s">
                                        Pilih Jenis IPTM
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5 col-md-6" style="margin:auto;">
                                <div class="single_service wow fadeInLeft" data-wow-duration="1.2s" data-wow-delay=".5s">
                                    <div class="service_icon_wrap text-center">
                                    </div>
                                    <div class="info text-center" style="padding-top:20px">
                                        <h3 style="color:white">IPTM Perpanjang</h3>
                                    </div>
                                    <div class="service_content">
                                        <ul>
                                            <p style="color: white; text-align: center">
                                                PERSYARATAN
                                            </p>
                                            <p style="color: white; text-align: center">
                                                =========================
                                            </p>
                                            <li>Menyiapkan foto surat IPTM</li>
                                            <li>Menyiapkan foto KTP Ahliwaris</li>
                                            <li>Menyiapkan foto KK Ahliwaris</li>
                                        </ul>
                                        <div class="apply_btn">
                                            <a href="/IPTM/perpanjangan"> <button class="boxed-btn3" >Ajukan Sekarang</button></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-6" style="margin:auto">
                                <div class="single_service wow fadeInRight" data-wow-duration="1.2s" data-wow-delay=".5s">
                                    <div class="service_icon_wrap text-center">
                                    </div>
                                    <div class="info text-center" style="padding-top:20px">
                                        <h3 style="color:white">IPTM Tumpang</h3>
                                    </div>
                                    <div class="service_content">
                                        <ul>
                                            <p style="color: white; text-align: center">
                                                PERSYARATAN
                                            </p>
                                            <p style="color: white; text-align: center">
                                                =========================
                                            </p>
                                            <li>Menyiapkan foto surat IPTM</li>
                                            <li>Menyiapkan foto KTP Ahliwaris</li>
                                            <li>Menyiapkan foto KK Ahliwaris</li>
                                            <li>Menyiapkan foto KTP Almarhum</li>
                                            <li>Menyiapkan foto KK Almarhum</li>
                                            <li>Menyiapkan foto SK Pemeriksaan Jenazah RS/Puskesmas</li>
                                            <li>Menyiapkan foto SK Kematian Kelurahan </li>
                                        </ul>
                                        <div class="apply_btn">
                                            <a href="/IPTM/tumpangan"> <button class="boxed-btn3" >Ajukan Sekarang</button></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class=" tab-pane container" id="information_pemakaman" role="tabpanel" aria-labelledby="information" style="margin-top: 50px">
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-information" role="tabpanel" aria-labelledby="pills-detaik-tab">
                            <div class=" tab-pane container" id="detail_pemakaman" role="tabpanel" aria-labelledby="alldetail">
                                <div class="card-header">
                                    <div class="alert alert-danger">Mohon melakukan login terlebih dahulu</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif


        @endif

@endsection
