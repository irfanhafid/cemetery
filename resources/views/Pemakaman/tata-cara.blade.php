@extends('layouts.user.app')
@section('header-class')
    {{"main-header-area"}}
@endsection
@section('content')
<style>
    * {
        box-sizing: border-box;
    }

    body {
        font-family: Arial, Helvetica, sans-serif;
    }

    /* Float four columns side by side */
    .column {
        float: left;
        width: 25%;
        padding: 0 10px;
    }

    /* Remove extra left and right margins, due to padding */
    .row {margin: 0 -5px;}

    /* Clear floats after the columns */
    .row:after {
        content: "";
        display: table;
        clear: both;
    }

    /* Responsive columns */
    @media screen and (max-width: 600px) {
        .column {
            width: 100%;
            display: block;
            margin-bottom: 20px;
        }
    }

    /* Style the counter cards */
    .card {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        padding: 16px;
        text-align: center;
        background-color: #f1f1f1;
    }
    .font-size{
        font-size: 20px;
        margin: 10px;
    }
    tr{
      margin-top: 20px;
    }
    td{
      margin-top: 20px;
    }
    table{
      margin-top: 20px;
    }
</style>
<div class="slider_area">
    <div class=" d-flex align-items-center "style="background-color: #2952a3;height: 200px; background-size: cover;background-repeat: no-repeat"></div>
</div>
<div class=" tab-pane container" role="tabpanel" aria-labelledby="information" style="margin-top: 20px">
    <div class="tab-content">
        <div class="tab-pane fade show active" role="tabpanel" aria-labelledby="pills-detail-tab">
            <div class=" tab-pane container" role="tabpanel" aria-labelledby="alldetail">
                <div class="card-header">
                    <strong>TATA CARA PEMAKAMAN</strong>
                </div>
                <div class="card-body card-block">
                    <div class="row">
                        <div class="col-md-12">
                          <h3>Prosedur Izin Penggunaan Tanah Makam Tumpang</h3>
                          <img src="/images/ProsedurTumpang.png" alt="Prosedur Izin Penggunaan Tanah Makam Tumpang">
                          <p style="color:black; margin-top:20px">Berikut ini merupakan alur pengisian Izin Penggunaan Tanah Makam (IPTM) Tumpang:</p>
                          <table>
                            <tr>
                              <td>1. Ahli waris menyiapkan:</td>
                            </tr>
                            <tr>
                              <td>
                                <table style="margin-left:20px">
                                  <tr>
                                    <td>a.	Surat IPTM asli atau fotokopi dari makam yang akan ditumpang.</td>
                                  </tr>
                                  <tr>
                                    <td>b.	Surat keterangan meninggal dari rumah sakit atau puskesmas DKI Jakarta.</td>
                                  </tr>
                                  <tr>
                                    <td>c.	KTP dan KK almarhum dari DKI Jakarta.</td>
                                  </tr>
                                  <tr>
                                    <td>d.	KTP dan KK ahli waris.</td>
                                  </tr>
                                  <tr>
                                    <td>e.	Materai Rp 6.000,00.</td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td>2.	Ahli waris mendatangi kantor TPU lalu mendapat surat pengantar ke PTSP Kelurahan.</td>
                            </tr>
                            <tr>
                              <td>3.	Ahli waris mendatangi PTSP Kelurahan dan akan memperoleh Surat Ketetapan Retribusi Daerah (SKRD) sesuai dengan blok petak makam yang akan digunakan.</td>
                            </tr>
                            <tr>
                              <td>4.	Ahli waris mendatangi Bank DKI untuk melakukan pembayaran SKRD dan mendapatkan bukti telah membayar retribusi di bank DKI atau ATM Bank DKI, biaya retribusi adalah Rp 100.000,00 untuk 3 (tiga) tahun.</td>
                            </tr>
                            <tr>
                              <td>5.	Ahli waris mendatangi PTSP Kelurahan untuk menerima Izin Penggunaan Tanah Makam (IPTM) yang berlaku selama 3 (tiga) tahun.</td>
                            </tr>
                          </table>
                        </div>
                    </div>
                    <div class="row" style="margin-top:50px">
                        <div class="col-md-12">
                          <h3>Prosedur Izin Penggunaan Tanah Makam Perpanjang</h3>
                          <img src="/images/ProsedurPerpanjang.png" alt="Prosedur Izin Penggunaan Tanah Makam Perpanjang">
                          <p style="color:black; margin-top:20px">Berikut ini merupakan alur pengisian Izin Penggunaan Tanah Makam (IPTM) Perpanjang:</p>
                          <table>
                            <tr>
                              <td>Jika IPTM sudah tidak berlaku maka:</td>
                            </tr>
                            <tr>
                              <td>
                                <table style="margin-left:20px">
                                  <tr>
                                    <td>1. Ahli waris menyiapkan:</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <table style="margin-left:40px">
                                        <tr>
                                          <td>a.	2 lembar surat IPTM asli atau fotokopi dari makam.</td>
                                        </tr>
                                        <tr>
                                          <td>b.	2 lembar KTP dan KK ahli waris.</td>
                                        </tr>
                                        <tr>
                                          <td>c.	Materai Rp 6.000,00.</td>
                                        </tr>
                                    </table>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>2.	Ahli waris mendatangi kantor TPU lalu mendapat surat pengantar ke PTSP Kelurahan.</td>
                                  </tr>
                                  <tr>
                                    <td>3.	Ahli waris mendatangi PTSP Kelurahan dan akan memperoleh Surat Ketetapan Retribusi Daerah (SKRD) sesuai dengan blok petak makam yang akan digunakan.</td>
                                  </tr>
                                  <tr>
                                    <td>4.	Ahli waris mendatangi Bank DKI untuk melakukan pembayaran SKRD dan mendapatkan bukti telah membayar retribusi di bank DKI atau ATM Bank DKI, biaya retribusi adalah Rp 100.000,00 untuk 3 (tiga) tahun.</td>
                                  </tr>
                                  <tr>
                                    <td>5.	Ahli waris mendatangi PTSP Kelurahan untuk menerima Izin Penggunaan Tanah Makam (IPTM) yang berlaku selama 3 (tiga) tahun.</td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td>Jika IPTM masih berlaku maka:</td>
                            </tr>
                            <tr>
                              <td>
                                <table style="margin-left:20px">
                                  <tr>
                                    <td>1. Ahli waris menyiapkan:</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <table style="margin-left:40px">
                                        <tr>
                                          <td>a.	2 lembar surat IPTM asli atau fotokopi dari makam.</td>
                                        </tr>
                                        <tr>
                                          <td>b.	2 lembar KTP dan KK ahli waris.</td>
                                        </tr>
                                        <tr>
                                          <td>c.	Materai Rp 6.000,00.</td>
                                        </tr>
                                    </table>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>2.	Ahli waris mendatangi PTSP Kelurahan dan akan memperoleh Surat Ketetapan Retribusi Daerah (SKRD) sesuai dengan blok petak makam yang akan digunakan.</td>
                                  </tr>
                                  <tr>
                                    <td>3.	Ahli waris mendatangi Bank DKI untuk melakukan pembayaran SKRD dan mendapatkan bukti telah membayar retribusi di bank DKI atau ATM Bank DKI, biaya retribusi adalah Rp 100.000,00 untuk 3 (tiga) tahun.</td>
                                  </tr>
                                  <tr>
                                    <td>4.	Ahli waris mendatangi PTSP Kelurahan untuk menerima Izin Penggunaan Tanah Makam (IPTM) yang berlaku selama 3 (tiga) tahun.</td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
