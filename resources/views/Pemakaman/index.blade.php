@php
    $role = \Auth::user()->role;
@endphp

@extends('layouts.app') @section('content')
    <div class="">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Data TPU</strong>
                        </div>
                        <div class="card-body">
                            {{--@include('partials.alert')--}}
                            @if($role == 'admin_dinas')
                                <div class="col-md-12 text-right">
                                    <a href="{{ url('pemakaman/create') }}" class="btn-add btn btn-success" style="margin-bottom: 20px"><i class="fa fa-plus"></i> Tambah</a>
                                </div>
                            @endif

                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <td>Nama TPU</td>
                                    <td>Kepala TPU</td>
                                    <td>Alamat</td>
                                    <td>Kota</td>
                                    <td>Kode Pos</td>
                                    <td>Email</td>
                                    <td style="width: 10%"></td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $d)
                                    <tr>
                                        <td>{{ $d->nama_pemakaman }}</td>
                                        <td>{{ $d->kepala_pemakaman }}</td>
                                        <td>{{ $d->alamat_pemakaman }}</td>
                                        <td>{{ $d->kota_pemakaman }}</td>
                                        <td>{{ $d->kodepos_pemakaman }}</td>
                                        <td>{{ $d->email_pemakaman }}</td>
                                        <td>
                                            <a href="{{ url("pemakaman/$d->id/edit") }}" class="btn-edit btn btn-primary" ><i class="fa fa-edit"> EDIT</i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
