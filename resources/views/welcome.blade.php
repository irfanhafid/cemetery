@extends('layouts.user.app')
@section('header-class')
    {{"main-header-area"}}
@endsection
@section('content')
    <link rel="stylesheet" href="/css/nice-select.css">
    <script src="/js/nice-select.min.js"></script>
    <div class="slider_area">
        <div class="single_slider  d-flex align-items-center slider_bg_1">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-lg-7 col-md-6">
                        <div class="slider_text">
                            <h3 class="wow fadeInRight" data-wow-duration="1s" data-wow-delay=".1s" style="color:#fff; font-size: 60px">SELAMAT DATANG</h3>
                            <h4 class="wow fadeInRight" data-wow-duration="1s" data-wow-delay=".1s" style="color:#fff; font-size: 25px">Kami melayani proses pengajuan Izin Penggunaan Tanah Makam (IPTM) pada TPU di daerah Jakarta Pusat</h4>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-6">
                        <div class="payment_form white-bg wow fadeInDown" data-wow-duration="1.2s" data-wow-delay=".2s">
                            <div class="info text-center">
                                <h4>Pilih Lokasi Pemakaman</h4>
                            </div>
                            <div class="form">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="single_input">
                                            <select class="wide" id="pemakaman-id">
                                                <option value="">Pilih Pemakaman</option>
                                                @if(isset($pemakaman))
                                                    @foreach($pemakaman as $pkm)
                                                        <option value="{{$pkm->id}}">{{$pkm->nama_pemakaman}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="submit_btn">
                                <button class="btn-pilih boxed-btn3" type="submit">Pilih</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(function () {
            $('.btn-pilih').click(function () {
                var $id = $('#pemakaman-id').val();
                if($id)

                    window.open('/pemakaman/details/' + $id, '_self');
                else
                    alert('Mohon pilih pemakaman terlebih dahulu.');
            });
        });
    </script>
@endsection
