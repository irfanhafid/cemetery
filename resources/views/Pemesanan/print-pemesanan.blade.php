<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            height: 800px;
            width: 650px;
            /* to centre page on screen*/
            margin-left: auto;
            margin-right: auto;
            font-size: 11.5px;
        }
        .text-center {
            text-align: center;
        }
        .text-left {
            text-align: left;
        }
        .text-right {
            text-align: right;
        }
        .page-header {
            border-bottom: 4px double #999;
        }
        .table-header {
            margin-top: 10px
        }
        .table-ahliwaris {
            margin-left: 15px;
        }
        .table-permohonan {
            margin-left: 15px;
        }
        .table-pemakaman {
            margin-left: 15px;
        }
        .assign-box {
            margin-top: 10px;
            width: 40%; float: right
        }
        .signature-box {
            margin-top: 82px;
            margin-left: 45px; float: left
        }
        p{
            margin-block-start: 0em;
            margin-block-end: 0em;
            margin: 0.25em;
        }
        @page { margin: 0px; }
    </style>
</head>
<body onload="window.print()">
@php
    if($tipe_pesanan == 'tumpangan'){
        $data = $data ?? $Tumpangan;
        $tumpangan = $data;
        $iptm_lama = $data->iptm;
        $almarhum_lama = $iptm_lama->almarhum;
        $almarhum_baru = $data->almarhum;
        $makam = $iptm_lama->makam;
        $pemakaman = $makam->pemakaman;
        $ahli_waris = $almarhum_baru->ahliWaris;
    }
    else{
        $data = $data ?? $Perpanjangan;
        $perpanjangan = $data;
        $iptm_lama = $data->iptm;
        $almarhum_lama = $iptm_lama->almarhum;
        $makam = $iptm_lama->makam;
        $pemakaman = $makam->pemakaman;
        $ahli_waris = $data->ahliWaris;
    }

@endphp
<div class="page-header">
    <h4 class="text-center" style="margin-bottom: 5px; font-size: 16px; font-family: Arial;">
        SATUAN PELAKSANA
        <br>
        TAMAN PEMAKAMAN UMUM ZONA I
        <br>
        @if($pemakaman->nama_pemakaman == 'Karet Bivak')
            <p style="background: black; color: white; text-align: center; font-size: 16px"><b>( TPU KARET BIVAK DAN KARET PASAR BARU BARAT )</b></p>
        @else
            <p style="background: black; color: white; text-align: center; font-size: 16px"><b>( TPU {{ strtoupper($pemakaman->nama_pemakaman) }} )</b></p>
        @endif
    </h4>
    <p class="text-center" style="font-size: 14px; font-family: Arial">
        <b>SUKU DINAS PERTAMANAN DAN HUTAN KOTA</b>
    </p>
    <p class="text-center" style="font-size: 12px; font-family: Arial">
        <b>KOTA ADMINISTRASI JAKARTA PUSAT</b>
    </p>
    <p class="text-center" style="font-style: italic; font-size: 11px">
        <span><b>Alamat : {{ $pemakaman->alamat_pemakaman }} Kel. {{ $pemakaman->kelurahan_pemakaman }} Kec. {{ $pemakaman->kecamatan_pemakaman }}</b></span><br>
        <span><b>Kota Administrasi Jakarta Pusat. Email : {{ $pemakaman->email_pemakaman }}</b></span>
    </p>
</div>
<div class="page-body">
    <div>
        <table class="table-header">
            <tr>
                <td width="80px">Nomor</td>
                <td>:</td>
                @if($tipe_pesanan == 'tumpangan')
                    <td>{{ $tumpangan->nomor_surat }}</td>
                @else
                    <td>{{ $perpanjangan->nomor_surat }}</td>
                @endif

            </tr>
            <tr>
                <td rowspan="2" style="vertical-align: top">Perihal</td>
                <td rowspan="2" style="vertical-align: top">:</td>
                <td>
                    <b>
                        @if($tipe_pesanan == 'tumpangan')
                            Rekomendasi Izin Penggunaan
                        @else
                            Rekomendasi Perpanjangan Izin
                        @endif
                    </b>
                </td>
            </tr>
            <tr>
                <td>
                    <b>
                        @if($tipe_pesanan == 'tumpangan')
                            Tanah Makam Tumpangan
                        @else
                            Penggunaan Tanah Makam
                        @endif
                    </b>
                </td>
            </tr>
        </table>

        <p class="text-right">
            <span style="margin-left: 426px; float: left">Kepada</span><br>
            <span style="margin-left: 400px; float: left">Yth. Kepala Unit Satuan Pelaksana PTSP</span><br>
            <span style="margin-left: 426px; float: left">Kelurahan </span>
            <span style="margin-left: 5px; float: left">{{ $iptm_lama->kelurahan_ptsp }}</span><br>
            <span style="margin-left: 426px; float: left">di</span><br>
            <span style="margin-right: 120px">Jakarta</span><br>
        </p>

        <p>
            Berdasarkan Surat Permohonan dan Pengantar :
        </p>
        <table class="table-permohonan">
            @if($tipe_pesanan == 'tumpangan')
                <tr>
                    <td>1.</td>
                    <td>Pengantar RT/RW Nomor : {{ $almarhum_baru->nomor_sp_rtrw ? $almarhum_baru->nomor_sp_rtrw : '-' }}</td>
                    <td>
                        tanggal : {{$almarhum_baru->tanggal_sp_rtrw ? date('d/m/Y', strtotime($almarhum_baru->tanggal_sp_rtrw )) : '-'}}
                    </td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>Surat Keterangan Kematian dari Puskesmas/Rumah Sakit Nomor : {{ $almarhum_baru->nomor_sk_kematian_rs }}</td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td>Surat Keterangan Kematian dari Kelurahan Nomor : {{ $almarhum_baru->nomor_sk_kematian_kelurahan }}</td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td>Surat Izin Penggunaan Tanah Makam Nomor : {{ $iptm_lama->nomor_iptm }}</td>
                    <td>tanggal : {{ $iptm_lama->tanggal_iptm ? date('d/m/Y', strtotime($iptm_lama->tanggal_iptm)) : '-' }}</td>
                </tr>
                <tr>
                    <td>5.</td>
                    <td>
                        Surat Kehilangan Dari Kepolisian Nomor : {{ $tumpangan->nomor_surat_kehilangan ? $tumpangan->nomor_surat_kehilangan : '-' }}
                    </td>
                    <td>
                        tanggal : {{ $tumpangan->tanggal_surat_kehilangan ? date('d/m/Y', strtotime($tumpangan->tanggal_surat_kehilangan)) : '-' }}
                    </td>
                </tr>
                <tr>
                    <td>6.</td>
                    <td>Foto Copy KTP Ahliwaris Nomor : {{ $ahli_waris->nomor_ktp_ahliwaris }}</td>
                </tr>
                <tr>
                    <td>7.</td>
                    <td>Foto Copy KTP Almarhum Nomor : {{ $almarhum_baru->nomor_ktp_almarhum }}</td>
                </tr>
                <tr>
                    <td>8.</td>
                    <td>Foto Copy KK Nomor : {{ $almarhum_baru->nomor_kk_almarhum }}</td>
                </tr>
            @else
                <tr>
                    <td>1.</td>
                    <td>Foto Copy KTP Ahliwaris Nomor : {{ $ahli_waris->nomor_ktp_ahliwaris }}</td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>
                        Surat Izin Penggunaan Tanah Makam Nomor : {{ $iptm_lama->nomor_iptm }}
                    </td>
                    <td>
                        <p style="margin-left: 90px; margin-top: 0px; margin-bottom: 0px">
                            tanggal : {{ $iptm_lama->tanggal_iptm ? date('d/m/Y', strtotime($iptm_lama->tanggal_iptm)) : '-' }}
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td>
                        Surat Kehilangan Dari Kepolisian Nomor : {{ $perpanjangan->nomor_surat_kehilangan ? $perpanjangan->nomor_surat_kehilangan : '-' }}
                    </td>
                    <td>
                        <p style="margin-left: 90px; margin-top: 0px; margin-bottom: 0px">
                            tanggal : {{ $perpanjangan->tanggal_surat_kehilangan ? date('d/m/Y', strtotime($perpanjangan->tanggal_surat_kehilangan)) : '-' }}
                        </p>
                    </td>
                </tr>
            @endif
        </table>
        <p>
            Bahwa nama tersebut dibawah ini :
        </p>
        <table class="table-ahliwaris">
            <tr>
                <td rowspan="2" style="vertical-align: top">Nama</td>
                <td rowspan="2" style="vertical-align: top">:</td>
                <td>{{ $ahli_waris->nama_ahliwaris }}</td>
            </tr>
            <tr>
                <td>(ahli waris / pihak yang bertanggung jawab)</td>
            </tr>
            <tr>
                <td rowspan="4" style="vertical-align: top">Alamat</td>
                <td rowspan="4" style="vertical-align: top">:</td>
                <td>{{ $ahli_waris->alamat_ahliwaris }}</td>
                <td><p style="margin-left: 78px; margin-top: 0px; margin-bottom: 0px">RT : {{ $ahli_waris->rt_ahliwaris }}</p></td>
                <td><p style="margin-left: 10px; margin-top: 0px; margin-bottom: 0px">RW : {{ $ahli_waris->rw_ahliwaris }}</p></td>
            </tr>
            <tr>
                <td>Kelurahan {{ $ahli_waris->kelurahan_ahliwaris }}</td>
            </tr>
            <tr>
                <td>Kecamatan {{ $ahli_waris->kecamatan_ahliwaris }}</td>
            </tr>
            <tr>
                <td>Kota Administrasi {{ $ahli_waris->kota_ahliwaris }}</td>
            </tr>
            <tr>
                <td>No.Telp / Hp</td>
                <td>:</td>
                <td>{{ $ahli_waris->telepon_ahliwaris }}</td>
            </tr>
            <tr>
                <td colspan="3">Hubungan Keluarga dengan yang meninggal sebagai : {{ strtolower($ahli_waris->hubungan_ahliwaris) }}</td>
            </tr>
        </table>
        <p>
            Dengan ini memberikan pengantar permohonan izin untuk menggunakan petak makam Tumpangan di :
        </p>
        <table class="table-pemakaman">
            @if($tipe_pesanan == 'tumpangan')
                <tr>
                    <td>1.</td>
                    <td>Lokasi</td>
                    <td>:</td>
                    <td>TPU {{ $pemakaman->nama_pemakaman }}</td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>Nama Jenazah Baru</td>
                    <td>:</td>
                    <td>{{ $almarhum_baru->nama_almarhum }}</td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td>Tanggal Wafat</td>
                    <td>:</td>
                    <td>{{ date('d/m/Y', strtotime($almarhum_baru->tanggal_wafat)) }}</td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td>Nama Jenazah Lama</td>
                    <td>:</td>
                    <td>{{ $tumpangan->daftar_jenazah ? $tumpangan->daftar_jenazah : $almarhum_lama->nama_almarhum}}</td>
                </tr>
                <tr>
                    <td>5.</td>
                    <td>Blok</td>
                    <td>:</td>
                    <td>{{ $makam->blok }}</td>
                </tr>
                <tr>
                    <td>6.</td>
                    <td>Blad</td>
                    <td>:</td>
                    <td>{{ $makam->blad }}</td>
                </tr>
                <tr>
                    <td>7.</td>
                    <td>Petak</td>
                    <td>:</td>
                    <td>{{ $makam->petak }}</td>
                </tr>
                <tr>
                    <td>8.</td>
                    <td>Masa berlaku</td>
                    <td>:</td>
                    <td> {{ date('d/m/Y', strtotime($tumpangan->tanggal_berlaku_dari)) }} s.d {{ date('d/m/Y', strtotime($tumpangan->tanggal_berlaku_sampai)) }}</td>
                </tr>
            @else
                <tr>
                    <td>1.</td>
                    <td>Lokasi</td>
                    <td>:</td>
                    <td>TPU {{ $pemakaman->nama_pemakaman }}</td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>Nama Almarhum/ah</td>
                    <td>:</td>
                    <td>{{ $perpanjangan->daftar_jenazah ? $perpanjangan->daftar_jenazah : $almarhum_lama->nama_almarhum }}</td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td>Tanggal Wafat</td>
                    <td>:</td>
                    <td>{{ date('d/m/Y', strtotime($almarhum_lama->tanggal_wafat)) }}</td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td>Blok</td>
                    <td>:</td>
                    <td>{{ $makam->blok }}</td>
                </tr>
                <tr>
                    <td>5.</td>
                    <td>Blad</td>
                    <td>:</td>
                    <td>{{ $makam->blad }}</td>
                </tr>
                <tr>
                    <td>6.</td>
                    <td>Petak</td>
                    <td>:</td>
                    <td>{{ $makam->petak }}</td>
                </tr>
                <tr>
                    <td>7.</td>
                    <td>Masa Berlaku</td>
                    <td>:</td>
                    <td> {{ date('d/m/Y', strtotime($perpanjangan->tanggal_berlaku_dari)) }} s.d {{ date('d/m/Y', strtotime($perpanjangan->tanggal_berlaku_sampai)) }}</td>
                </tr>
                <tr>
                    <td>8.</td>
                    <td>Perpanjangan Ke</td>
                    <td>:</td>
                    <td>{{ $perpanjangan->perpanjangan_ke }}</td>
                </tr>
            @endif
        </table>
        @if($tipe_pesanan == 'perpanjangan')
            <p>Setelah dilakukan pemeriksaan secara administratif maupun dilapangan, bahwa petak makam tersebut masih ada / sudah tidak ada *) fisiknya, sehingga Izin Penggunaan Tanah Makam tersebut dapat / tidak dapat *) diperpanjang sesuai aturan yang berlaku.</p>
            <p>Surat Rekomendasi ini hanya berlaku 30 hari sejak tanggal dikeluarkan.</p>
            <br>
            <div class="signature-box">
                <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0
                       style='margin-left:12.5pt;border-collapse:collapse;border:none'>
                    <tr style='height:11.5pt'>
                        <td width=120 valign=top style='width:90.15pt;border:solid windowtext 1.0pt;padding:0in 5.4pt 0in 5.4pt;height:11.5pt'>
                            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:normal'><span lang=IN style='font-family:"Arial",sans-serif'>Paraf petugas</span></p>
                        </td>
                    </tr>
                    <tr style='height:41.75pt'>
                        <td width=120 valign=top style='width:90.15pt;border:solid windowtext 1.0pt;border-top:none;padding:0in 5.4pt 0in 5.4pt;height:41.75pt'>
                            <p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:normal'><span lang=IN style='font-family:"Arial",sans-serif'>&nbsp;</span></p>
                        </td>
                    </tr>
                </table><br>
                <span>Keterangan</span><br>
                <span>*) dicoret yang tidak perlu</span>
            </div>
        @else
        <p>Surat Rekomendasi ini hanya berlaku 30 hari sejak tanggal dikeluarkan.</p>
        @endif
        <div class="assign-box">
            @php
                $months = [
                    1 => 'Januari',
                    2 => 'Februari',
                    3 => 'Maret',
                    4 => 'April',
                    5 => 'Mei',
                    6 => 'Juni',
                    7 => 'Juli',
                    8 => 'Agustus',
                    9 => 'September',
                    10 => 'Oktober',
                    11 => 'November',
                    12 => 'Desember',
                ]
            @endphp
            <p class="text-center">
                <span>Jakarta, {{ date('d') .' '. $months[date('n')] .' '. date('Y') }}</span><br>
                <span>An. Kepala Seksi Jalur Hijau dan Pemakaman</span><br>
                <span>Suku Dinas Pertamanan dan Hutan Kota</span><br>
                <span>Kota Administrasi Jakarta Pusat,</span><br>
                <span>Kepala Satuan  Pelaksana</span><br>
                <span>TPU Zona I,</span><br>
                <img src="{{ public_path('images/pemakaman/ttd.png') }}" alt="" style="width:100px;height:100px;margin:0px;"><br>
                <span>Saiman S.Ag.S.AP{{--{{$pemakaman->kepala_pemakaman}}--}}</span><br>
                <span>NIP 197609032008011027{{--{{$pemakaman->nip_kepala_pemakaman}}--}}</span><br>
            </p>
        </div>
    </div>
</div>
</body>
</html>
