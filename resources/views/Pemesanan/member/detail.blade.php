@extends('layouts.user.app')
@section('header-class')
    {{"main-header-area"}}
@endsection
@section('content')
    <style>
        * {
            box-sizing: border-box;
        }

        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        /* Float four columns side by side */
        .column {
            float: left;
            width: 25%;
            padding: 0 10px;
        }

        /* Remove extra left and right margins, due to padding */
        .row {margin: 0 -5px;}

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Responsive columns */
        @media screen and (max-width: 600px) {
            .column {
                width: 100%;
                display: block;
                margin-bottom: 20px;
            }
        }

        /* Style the counter cards */
        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            padding: 16px;
            text-align: center;
            background-color: #f1f1f1;
        }
        .font-size{
            font-size: 20px;
            margin: 10px;
        }
    </style>
    @if(Auth::check())

        <div class="slider_area">
            <div class=" d-flex align-items-center "style="background-color: #2952a3;height: 200px; background-size: cover;background-repeat: no-repeat"></div>
        </div>

        <div class=" tab-pane container" id="information_pemakaman" role="tabpanel" aria-labelledby="information" style="margin-top: 20px">
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-information" role="tabpanel" aria-labelledby="pills-detaik-tab">
                    <div class=" tab-pane container" id="detail_pemakaman" role="tabpanel" aria-labelledby="alldetail">
                        <div class="card-header">
                            <strong>INFORMASI STATUS IPTM</strong>
                        </div>
                        <div class="card-body card-block">
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Nomor Permohonan</th>
                                            <th>Tanggal Permohonan</th>
                                            <th>Jenis IPTM</th>
                                            <th>Nama Pemakaman</th>
                                            <th>Status Permohonan</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data as $d)
                                            <tr>
                                                <td>{{ $d->nomor_permohonan }}</td>
                                                <td>{{ $d->tanggal_permohonan}}</td>
                                                <td>{{ ucwords($d->tipe_pesanan) }}</td>
                                                <td>{{ $d->nama_pemakaman }}</td>
                                                <td>
                                                    @if($d->status == 1)
                                                        <span class="badge badge-pill badge-warning">Waiting</span>
                                                    @elseif($d->status == 2)
                                                        <span class="badge badge-pill badge-success">Approved</span>
                                                    @else
                                                        <span class="badge badge-pill badge-danger">Reject</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a class="btn btn-info" href="{{ url("pemakaman/pesanan/$d->tipe_pesanan/$d->id/detail") }}"><i class="fa fa-eye"></i> Lihat Detail</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <h2>Mohon untuk login terlebih dahulu</h2>
    @endif
@endsection
