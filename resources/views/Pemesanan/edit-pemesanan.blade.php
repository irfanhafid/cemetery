@push('style')
    <style>
        .card-body .card {
            border: none;
        }
        .card-body .card .card-header{
            border: none;
        }
        .modal-dialog {
            width: 100%;
            max-width: 800px;
            max-height: 800px;
        }
    </style>
@endpush

@php $user = Auth::user(); @endphp
@extends((( Auth::user()->role == 'admin_tpu'|| Auth::user()->role =='admin_dinas') ? 'layouts.app' : 'layouts.user.app' ))
@if(Auth::user()->role == 'member')
@section('header-class')
    {{"main-header-area"}}
@endsection
@endif
@section('content')

@if(Auth::user()->role == 'member')
<div class="slider_area">
    <div class=" d-flex align-items-center "style="background-color: #2952a3;height: 200px; background-size: cover;background-repeat: no-repeat"></div>
</div>

<div class="">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Edit Permohonan</strong>
                    </div>
                    @if ($sukses = Session::get('sukses'))
                      <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $sukses }}</strong>
                      </div>
                    @endif
                    <form action="{{ url("pemakaman/pesanan/$tipe_pesanan/$data->id/detail/edit") }}" method="POST" enctype="multipart/form-data">
                      @csrf
                      @method('PATCH')
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="card">
                                        @php
                                            if($tipe_pesanan == 'tumpangan'){
                                                $data = $data ?? $Tumpangan;
                                                $tumpangan = $data;
                                                $iptm_lama = $data->iptm;
                                                $almarhum_lama = $iptm_lama->almarhum;
                                                $almarhum_baru = $data->almarhum;
                                                $makam = $iptm_lama->makam;
                                                $pemakaman = $makam->pemakaman;
                                                $ahli_waris = $almarhum_baru->ahliWaris;
                                            }
                                            else{
                                                $data = $data ?? $Perpanjangan;
                                                $perpanjangan = $data;
                                                $iptm_lama = $data->iptm;
                                                $almarhum_lama = $iptm_lama->almarhum;
                                                $makam = $iptm_lama->makam;
                                                $pemakaman = $makam->pemakaman;
                                                $ahli_waris = $data->ahliWaris;
                                            }
                                        @endphp
                                        <div class="card-header"><strong>Informasi Makam</strong></div>
                                        <div class="card-body card-block">
                                            <div class="form-group">
                                                <label for="nomor_iptm" class=" form-control-label">Nomor IPTM</label>
                                                <input type="text" name="nomor_iptm" placeholder="Nomor IPTM" class="form-control" value="{{ $iptm_lama->nomor_iptm }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama_almarhum" class=" form-control-label">Nama Almarhum</label>
                                                <input type="text" name="nama_almarhum" placeholder="Nama Almarhum" class="form-control" value="{{ $almarhum_lama->nama_almarhum }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="tanggal_wafat" class=" form-control-label">Tanggal Wafat</label>
                                                <input type="text" name="tanggal_wafat" placeholder="Tanggal Wafat" class="form-control" value="{{ $almarhum_lama->tanggal_wafat }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="nama_pemakaman" class=" form-control-label">Lokasi Pemakaman</label>
                                                <input type="text" name="nama_pemakaman" placeholder="Pemakaman" class="form-control" value="{{ $pemakaman->nama_pemakaman }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="blok" class=" form-control-label">Blok</label>
                                                <input type="text" name="blok" placeholder="Blok" class="form-control" value="{{ $makam->blok }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="blad" class=" form-control-label">Blad</label>
                                                <input type="text" name="blad" placeholder="Blad" class="form-control" value="{{ $makam->blad }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="petak" class=" form-control-label">Petak</label>
                                                <input type="text" name="petak" placeholder="Petak" class="form-control" value="{{ $makam->petak }}" readonly>
                                            </div>
                                            <div class="form-group">
                                                <label for="masa_berlaku" class=" form-control-label">Tanggal Kadaluwarsa</label>
                                                <input type="date" name="masa_berlaku" placeholder="Tanggal Kadaluwarsa" class="form-control" value="{{ $iptm_lama->masa_berlaku }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if($tipe_pesanan == 'tumpangan')
                                    <div class="col-sm-4">
                                        <div class="card" style="margin-bottom: 0">
                                            <div class="card-header"><strong>Informasi Almarhum Baru</strong></div>
                                            <div class="card-body card-block">
                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <label for="nama_almarhum" class=" form-control-label">Nama Almarhum</label>
                                                        <input type="text" name="nama_almarhum" placeholder="Nama Almarhum" class="form-control" value="{{ $almarhum_baru->nama_almarhum }}">
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="tanggal_wafat" class=" form-control-label">Tanggal Wafat</label>
                                                        <input type="text" name="tanggal_wafat" placeholder="Tanggal Wafat" class="form-control" value="{{ $almarhum_baru->tanggal_wafat }}">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <label for="nomor_ktp_almarhum" class=" form-control-label">No KTP Almarhum</label>
                                                        <input type="text" name="nomor_ktp_almarhum" placeholder="No KTP Almarhum" class="form-control" value="{{ $almarhum_baru->nomor_ktp_almarhum }}">
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="file_ktp_almarhum" class=" form-control-label">Foto KTP Almarhum</label>
                                                        <!-- <a href="#modal-file_ktp_almarhum" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $almarhum_baru->file_ktp_almarhum ? 'file-image-o' : 'ban' }}"></i></a> -->
                                                        <img src="{{ asset("Document/Tumpangan/$almarhum_baru->file_ktp_almarhum") }}" width="100%" alt="">
                                                        <input type="file" id="images" name="file_ktp_almarhum" class="form-control" >
                                                        <!-- <div class="modal fade" id="modal-file_ktp_almarhum" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Foto KTP Almarhum</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body text-center">
                                                                        <img src="{{ asset("Document/Tumpangan/$almarhum_baru->file_ktp_almarhum") }}" alt="">
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <label for="nomor_kk_almarhum" class=" form-control-label">No KK Almarhum</label>
                                                        <input type="text" name="nomor_kk_almarhum" placeholder="No KK Almarhum" class="form-control" value="{{ $almarhum_baru->nomor_kk_almarhum }}">
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="file_kk_almarhum" class=" form-control-label">Foto KK Almarhum</label>
                                                        <!-- <a href="#modal-file_kk_almarhum" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $almarhum_baru->file_kk_almarhum ? 'file-image-o' : 'ban' }}"></i></a> -->
                                                        <img src="{{ asset("Document/Tumpangan/$almarhum_baru->file_kk_almarhum") }}" width="100%" alt="">
                                                        <input type="file" id="images" name="file_kk_almarhum" class="form-control" >
                                                        <!-- <div class="modal fade" id="modal-file_kk_almarhum" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Foto KK Almarhum</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body text-center">
                                                                        <img src="{{ asset("Document/Tumpangan/$almarhum_baru->file_kk_almarhum") }}" alt="">
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header"><strong>Surat Keterangan & Lampiran</strong></div>
                                            <div class="card-body card-block">
                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <label for="file_iptm_asli" class=" form-control-label">Foto IPTM Asli</label>
                                                        <!-- <a href="#modal-file_iptm_asli" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $tumpangan->file_iptm_asli ? 'file-image-o' : 'ban' }}"></i></a> -->
                                                        <img src="{{ asset("Document/Tumpangan/$tumpangan->file_iptm_asli") }}" width="100%" alt="">
                                                        <input type="file" id="images" name="file_iptm_asli" class="form-control" >
                                                        <!-- <div class="modal fade" id="modal-file_iptm_asli" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Foto KTP Almarhum</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body text-center">
                                                                        <img src="{{ asset("Document/Tumpangan/$tumpangan->file_iptm_asli") }}" alt="">
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-4">
                                                        <label for="nomor_kehilangan_kepolisian" class=" form-control-label">No SKCK</label>
                                                        <input type="text" name="nomor_sk_kehilangan_kepolisian" placeholder="No" class="form-control" value="{{ $tumpangan->nomor_sk_kehilangan_kepolisian }}">
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        <label for="tanggal_kehilangan_kepolisian" class=" form-control-label">Tanggal</label>
                                                        <input type="date" name="tanggal_sk_kehilangan_kepolisian" placeholder="Tanggal" class="form-control" value="{{ $tumpangan->tanggal_sk_kehilangan_kepolisian }}">
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        <label for="file_sk_kehilangan_kepolisian" class=" form-control-label">Foto</label>
                                                        <!-- <a href="#modal-file_sk_kehilangan_kepolisian" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $tumpangan->file_sk_kehilangan_kepolisian ? 'file-image-o' : 'ban' }}"></i></a> -->
                                                        <img src="{{ asset("Document/Tumpangan/$tumpangan->file_sk_kehilangan_kepolisian") }}" width="100%" alt="">
                                                        <input type="file" id="images" name="file_sk_kehilangan_kepolisian" class="form-control" >
                                                        <!-- <div class="modal fade" id="modal-file_sk_kehilangan_kepolisian" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Foto KTP Almarhum</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body text-center">
                                                                        <img src="{{ asset("Document/Tumpangan/$tumpangan->file_sk_kehilangan_kepolisian") }}" alt="">
                                                                    </div>`
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-4">
                                                        <label for="nomor_sp_rtrw" class=" form-control-label">No Surat Ket RT/RW</label>
                                                        <input type="text" name="nomor_sp_rtrw" placeholder="No" class="form-control" value="{{ $almarhum_baru->nomor_sp_rtrw }}">
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        <label for="tanggal_sp_rtrw" class=" form-control-label">Tanggal</label>
                                                        <input type="date" name="tanggal_sp_rtrw" placeholder="-" class="form-control" value="{{ $almarhum_baru->tanggal_sp_rtrw }}">
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        <label for="file_sp_rtrw" class=" form-control-label">Foto</label>
                                                        <!-- <a href="#modal-file_sp_rtrw" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $almarhum_baru->file_sp_rtrw ? 'file-image-o' : 'ban' }}"></i></a> -->
                                                        <img src="{{ asset("Document/Tumpangan/$almarhum_baru->file_sp_rtrw") }}" width="100%" alt="">
                                                        <input type="file" id="images" name="file_sp_rtrw" class="form-control" >
                                                        <!-- <div class="modal fade" id="modal-file_sp_rtrw" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Foto KTP Almarhum</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body text-center">
                                                                        <img src="{{ asset("Document/Tumpangan/$almarhum_baru->file_sp_rtrw") }}" alt="">
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-4">
                                                        <label for="nomor_sk_kematian_rs" class=" form-control-label">No Surat Kematian</label>
                                                        <input type="text" name="nomor_sk_kematian_rs" placeholder="No" class="form-control" value="{{ $almarhum_baru->nomor_sk_kematian_rs }}">
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        <label for="tanggal_sk_kematian_rs" class=" form-control-label">Tanggal</label>
                                                        <input type="date" name="tanggal_sk_kematian_rs" placeholder="Tanggal" class="form-control" value="{{ $almarhum_baru->tanggal_sk_kematian_rs }}">
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        <label for="file_sk_kematian_rs" class=" form-control-label">Foto</label>
                                                        <!-- <a href="#modal-file_sk_kematian_rs" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $almarhum_baru->file_sk_kematian_rs ? 'file-image-o' : 'ban' }}"></i></a> -->
                                                        <img src="{{ asset("Document/Tumpangan/$almarhum_baru->file_sk_kematian_rs") }}" width="100%" alt="">
                                                        <input type="file" id="images" name="file_sk_kematian_rs" class="form-control" >
                                                        <!-- <div class="modal fade" id="modal-file_sk_kematian_rs" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Foto KTP Almarhum</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body text-center">
                                                                        <img src="{{ asset("Document/Tumpangan/$almarhum_baru->file_sk_kematian_rs") }}" alt="">
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-4">
                                    <div class="card">
                                        <div class="card-header"><strong>Informasi Ahli Waris</strong></div>
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="nomor_ktp_ahliwaris" class=" form-control-label">No KTP Ahli Waris</label>
                                                <input type="text" name="nomor_ktp_ahliwaris" placeholder="No KTP Ahli Waris" class="form-control" value="{{ $ahli_waris->nomor_ktp_ahliwaris }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="nama_ahliwaris" class=" form-control-label">Nama Ahli Waris</label>
                                                <input type="text" name="nama_ahliwaris" placeholder="Nama Ahli Waris" class="form-control" value="{{ $ahli_waris->nama_ahliwaris }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="telepon_ahliwaris" class=" form-control-label">Telepon Ahli Waris</label>
                                                <input type="text" name="telepon_ahliwaris" placeholder="Telepon Ahli Waris" class="form-control" value="{{ $ahli_waris->telepon_ahliwaris }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="hubungan_ahliwaris" class=" form-control-label">Hubungan</label>
                                                <input type="text" name="hubungan_ahliwaris" placeholder="Hubungan" class="form-control" value="{{ $ahli_waris->hubungan_ahliWaris }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="alamat_ahliwaris" class=" form-control-label">Alamat</label>
                                                <textarea type="text" name="alamat_ahliwaris" placeholder="Alamat" class="form-control" rows="5">{{ $ahli_waris->alamat_ahliwaris }}</textarea>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-3">
                                                    <label for="rt_ahliwaris" class=" form-control-label">RT</label>
                                                    <input type="text" name="rt_ahliwaris" placeholder="RT" class="form-control" value="{{ $ahli_waris->rt_ahliwaris }}">
                                                </div>
                                                <div class="form-group col-sm-3">
                                                    <label for="rw_ahliwaris" class=" form-control-label">RW</label>
                                                    <input type="text" name="rw_ahliwaris" placeholder="RW" class="form-control" value="{{ $ahli_waris->rw_ahliwaris }}">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="kelurahan_ahliwaris" class=" form-control-label">Kelurahan</label>
                                                    <input type="text" name="kelurahan_ahliwaris" placeholder="Kelurahan" class="form-control" value="{{ $ahli_waris->kelurahan_ahliwaris }}">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <label for="kecamatan_ahliwaris" class=" form-control-label">Kecamatan</label>
                                                    <input type="text" name="kecamatan_ahliwaris" placeholder="Kecamatan" class="form-control" value="{{ $ahli_waris->kecamatan_ahliwaris }}">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="kota_administrasi" class=" form-control-label">Kota Administrasi</label>
                                                    <input type="text" name="kota_administrasi" placeholder="Kota Administrasi" class="form-control" value="{{ $ahli_waris->kota_ahliwaris }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer" style="height: 60px">
                          <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
    @elseif(Auth::user()->role == 'admin_tpu')
    <div class="">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Edit Permohonan</strong>
                        </div>
                        @if ($sukses = Session::get('sukses'))
                          <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $sukses }}</strong>
                          </div>
                        @endif
                        <form action="{{ url("pemakaman/pesanan/$tipe_pesanan/$data->id/detail/edit") }}" method="POST" enctype="multipart/form-data">
                          @csrf
                          @method('PATCH')
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="card">
                                            @php
                                                if($tipe_pesanan == 'tumpangan'){
                                                    $data = $data ?? $Tumpangan;
                                                    $tumpangan = $data;
                                                    $iptm_lama = $data->iptm;
                                                    $almarhum_lama = $iptm_lama->almarhum;
                                                    $almarhum_baru = $data->almarhum;
                                                    $makam = $iptm_lama->makam;
                                                    $pemakaman = $makam->pemakaman;
                                                    $ahli_waris = $almarhum_baru->ahliWaris;
                                                }
                                                else{
                                                    $data = $data ?? $Perpanjangan;
                                                    $perpanjangan = $data;
                                                    $iptm_lama = $data->iptm;
                                                    $almarhum_lama = $iptm_lama->almarhum;
                                                    $makam = $iptm_lama->makam;
                                                    $pemakaman = $makam->pemakaman;
                                                    $ahli_waris = $data->ahliWaris;
                                                }
                                            @endphp
                                            <div class="card-header"><strong>Informasi Makam</strong></div>
                                            <div class="card-body card-block">
                                                <div class="form-group">
                                                    <label for="nomor_iptm" class=" form-control-label">Nomor IPTM</label>
                                                    <input type="text" name="nomor_iptm" placeholder="Nomor IPTM" class="form-control" value="{{ $iptm_lama->nomor_iptm }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="nama_almarhum" class=" form-control-label">Nama Almarhum</label>
                                                    <input type="text" name="nama_almarhum" placeholder="Nama Almarhum" class="form-control" value="{{ $almarhum_lama->nama_almarhum }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="tanggal_wafat" class=" form-control-label">Tanggal Wafat</label>
                                                    <input type="text" name="tanggal_wafat" placeholder="Tanggal Wafat" class="form-control" value="{{ $almarhum_lama->tanggal_wafat }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="nama_pemakaman" class=" form-control-label">Lokasi Pemakaman</label>
                                                    <input type="text" name="nama_pemakaman" placeholder="Pemakaman" class="form-control" value="{{ $pemakaman->nama_pemakaman }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="blok" class=" form-control-label">Blok</label>
                                                    <input type="text" name="blok" placeholder="Blok" class="form-control" value="{{ $makam->blok }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="blad" class=" form-control-label">Blad</label>
                                                    <input type="text" name="blad" placeholder="Blad" class="form-control" value="{{ $makam->blad }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="petak" class=" form-control-label">Petak</label>
                                                    <input type="text" name="petak" placeholder="Petak" class="form-control" value="{{ $makam->petak }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="masa_berlaku" class=" form-control-label">Tanggal Kadaluwarsa</label>
                                                    <input type="date" name="masa_berlaku" placeholder="Tanggal Kadaluwarsa" class="form-control" value="{{ $iptm_lama->masa_berlaku }}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if($tipe_pesanan == 'tumpangan')
                                        <div class="col-sm-4">
                                            <div class="card" style="margin-bottom: 0">
                                                <div class="card-header"><strong>Informasi Almarhum Baru</strong></div>
                                                <div class="card-body card-block">
                                                    <div class="row">
                                                        <div class="form-group col-sm-6">
                                                            <label for="nama_almarhum" class=" form-control-label">Nama Almarhum</label>
                                                            <input type="text" name="nama_almarhum" placeholder="Nama Almarhum" class="form-control" value="{{ $almarhum_baru->nama_almarhum }}">
                                                        </div>
                                                        <div class="form-group col-sm-6">
                                                            <label for="tanggal_wafat" class=" form-control-label">Tanggal Wafat</label>
                                                            <input type="text" name="tanggal_wafat" placeholder="Tanggal Wafat" class="form-control" value="{{ $almarhum_baru->tanggal_wafat }}">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-6">
                                                            <label for="nomor_ktp_almarhum" class=" form-control-label">No KTP Almarhum</label>
                                                            <input type="text" name="nomor_ktp_almarhum" placeholder="No KTP Almarhum" class="form-control" value="{{ $almarhum_baru->nomor_ktp_almarhum }}">
                                                        </div>
                                                        <div class="form-group col-sm-6">
                                                            <label for="file_ktp_almarhum" class=" form-control-label">Foto KTP Almarhum</label>
                                                            <!-- <a href="#modal-file_ktp_almarhum" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $almarhum_baru->file_ktp_almarhum ? 'file-image-o' : 'ban' }}"></i></a> -->
                                                            <img src="{{ asset("Document/Tumpangan/$almarhum_baru->file_ktp_almarhum") }}" width="100%" alt="">
                                                            <input type="file" id="images" name="file_ktp_almarhum" class="form-control" >
                                                            <!-- <div class="modal fade" id="modal-file_ktp_almarhum" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto KTP Almarhum</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img src="{{ asset("Document/Tumpangan/$almarhum_baru->file_ktp_almarhum") }}" alt="">
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-6">
                                                            <label for="nomor_kk_almarhum" class=" form-control-label">No KK Almarhum</label>
                                                            <input type="text" name="nomor_kk_almarhum" placeholder="No KK Almarhum" class="form-control" value="{{ $almarhum_baru->nomor_kk_almarhum }}">
                                                        </div>
                                                        <div class="form-group col-sm-6">
                                                            <label for="file_kk_almarhum" class=" form-control-label">Foto KK Almarhum</label>
                                                            <!-- <a href="#modal-file_kk_almarhum" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $almarhum_baru->file_kk_almarhum ? 'file-image-o' : 'ban' }}"></i></a> -->
                                                            <img src="{{ asset("Document/Tumpangan/$almarhum_baru->file_kk_almarhum") }}" width="100%" alt="">
                                                            <input type="file" id="images" name="file_kk_almarhum" class="form-control" >
                                                            <!-- <div class="modal fade" id="modal-file_kk_almarhum" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto KK Almarhum</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img src="{{ asset("Document/Tumpangan/$almarhum_baru->file_kk_almarhum") }}" alt="">
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header"><strong>Surat Keterangan & Lampiran</strong></div>
                                                <div class="card-body card-block">
                                                    <div class="row">
                                                        <div class="form-group col-sm-6">
                                                            <label for="file_iptm_asli" class=" form-control-label">Foto IPTM Asli</label>
                                                            <!-- <a href="#modal-file_iptm_asli" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $tumpangan->file_iptm_asli ? 'file-image-o' : 'ban' }}"></i></a> -->
                                                            <img src="{{ asset("Document/Tumpangan/$tumpangan->file_iptm_asli") }}" width="100%" alt="">
                                                            <input type="file" id="images" name="file_iptm_asli" class="form-control" >
                                                            <!-- <div class="modal fade" id="modal-file_iptm_asli" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto KTP Almarhum</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img src="{{ asset("Document/Tumpangan/$tumpangan->file_iptm_asli") }}" alt="">
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-4">
                                                            <label for="nomor_kehilangan_kepolisian" class=" form-control-label">No SKCK</label>
                                                            <input type="text" name="nomor_sk_kehilangan_kepolisian" placeholder="No" class="form-control" value="{{ $tumpangan->nomor_sk_kehilangan_kepolisian }}">
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="tanggal_kehilangan_kepolisian" class=" form-control-label">Tanggal</label>
                                                            <input type="date" name="tanggal_sk_kehilangan_kepolisian" placeholder="Tanggal" class="form-control" value="{{ $tumpangan->tanggal_sk_kehilangan_kepolisian }}">
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="file_sk_kehilangan_kepolisian" class=" form-control-label">Foto</label>
                                                            <!-- <a href="#modal-file_sk_kehilangan_kepolisian" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $tumpangan->file_sk_kehilangan_kepolisian ? 'file-image-o' : 'ban' }}"></i></a> -->
                                                            <img src="{{ asset("Document/Tumpangan/$tumpangan->file_sk_kehilangan_kepolisian") }}" width="100%" alt="">
                                                            <input type="file" id="images" name="file_sk_kehilangan_kepolisian" class="form-control" >
                                                            <!-- <div class="modal fade" id="modal-file_sk_kehilangan_kepolisian" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto KTP Almarhum</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img src="{{ asset("Document/Tumpangan/$tumpangan->file_sk_kehilangan_kepolisian") }}" alt="">
                                                                        </div>`
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-4">
                                                            <label for="nomor_sp_rtrw" class=" form-control-label">No Surat Ket RT/RW</label>
                                                            <input type="text" name="nomor_sp_rtrw" placeholder="No" class="form-control" value="{{ $almarhum_baru->nomor_sp_rtrw }}">
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="tanggal_sp_rtrw" class=" form-control-label">Tanggal</label>
                                                            <input type="date" name="tanggal_sp_rtrw" placeholder="-" class="form-control" value="{{ $almarhum_baru->tanggal_sp_rtrw }}">
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="file_sp_rtrw" class=" form-control-label">Foto</label>
                                                            <!-- <a href="#modal-file_sp_rtrw" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $almarhum_baru->file_sp_rtrw ? 'file-image-o' : 'ban' }}"></i></a> -->
                                                            <img src="{{ asset("Document/Tumpangan/$almarhum_baru->file_sp_rtrw") }}" width="100%" alt="">
                                                            <input type="file" id="images" name="file_sp_rtrw" class="form-control" >
                                                            <!-- <div class="modal fade" id="modal-file_sp_rtrw" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto KTP Almarhum</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img src="{{ asset("Document/Tumpangan/$almarhum_baru->file_sp_rtrw") }}" alt="">
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-4">
                                                            <label for="nomor_sk_kematian_rs" class=" form-control-label">No Surat Kematian</label>
                                                            <input type="text" name="nomor_sk_kematian_rs" placeholder="No" class="form-control" value="{{ $almarhum_baru->nomor_sk_kematian_rs }}">
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="tanggal_sk_kematian_rs" class=" form-control-label">Tanggal</label>
                                                            <input type="date" name="tanggal_sk_kematian_rs" placeholder="Tanggal" class="form-control" value="{{ $almarhum_baru->tanggal_sk_kematian_rs }}">
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="file_sk_kematian_rs" class=" form-control-label">Foto</label>
                                                            <!-- <a href="#modal-file_sk_kematian_rs" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $almarhum_baru->file_sk_kematian_rs ? 'file-image-o' : 'ban' }}"></i></a> -->
                                                            <img src="{{ asset("Document/Tumpangan/$almarhum_baru->file_sk_kematian_rs") }}" width="100%" alt="">
                                                            <input type="file" id="images" name="file_sk_kematian_rs" class="form-control" >
                                                            <!-- <div class="modal fade" id="modal-file_sk_kematian_rs" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto KTP Almarhum</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img src="{{ asset("Document/Tumpangan/$almarhum_baru->file_sk_kematian_rs") }}" alt="">
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-sm-4">
                                        <div class="card">
                                            <div class="card-header"><strong>Informasi Ahli Waris</strong></div>
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="nomor_ktp_ahliwaris" class=" form-control-label">No KTP Ahli Waris</label>
                                                    <input type="text" name="nomor_ktp_ahliwaris" placeholder="No KTP Ahli Waris" class="form-control" value="{{ $ahli_waris->nomor_ktp_ahliwaris }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="nama_ahliwaris" class=" form-control-label">Nama Ahli Waris</label>
                                                    <input type="text" name="nama_ahliwaris" placeholder="Nama Ahli Waris" class="form-control" value="{{ $ahli_waris->nama_ahliwaris }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="telepon_ahliwaris" class=" form-control-label">Telepon Ahli Waris</label>
                                                    <input type="text" name="telepon_ahliwaris" placeholder="Telepon Ahli Waris" class="form-control" value="{{ $ahli_waris->telepon_ahliwaris }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="hubungan_ahliwaris" class=" form-control-label">Hubungan</label>
                                                    <input type="text" name="hubungan_ahliwaris" placeholder="Hubungan" class="form-control" value="{{ $ahli_waris->hubungan_ahliWaris }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="alamat_ahliwaris" class=" form-control-label">Alamat</label>
                                                    <textarea type="text" name="alamat_ahliwaris" placeholder="Alamat" class="form-control" rows="5">{{ $ahli_waris->alamat_ahliwaris }}</textarea>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-3">
                                                        <label for="rt_ahliwaris" class=" form-control-label">RT</label>
                                                        <input type="text" name="rt_ahliwaris" placeholder="RT" class="form-control" value="{{ $ahli_waris->rt_ahliwaris }}">
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label for="rw_ahliwaris" class=" form-control-label">RW</label>
                                                        <input type="text" name="rw_ahliwaris" placeholder="RW" class="form-control" value="{{ $ahli_waris->rw_ahliwaris }}">
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="kelurahan_ahliwaris" class=" form-control-label">Kelurahan</label>
                                                        <input type="text" name="kelurahan_ahliwaris" placeholder="Kelurahan" class="form-control" value="{{ $ahli_waris->kelurahan_ahliwaris }}">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <label for="kecamatan_ahliwaris" class=" form-control-label">Kecamatan</label>
                                                        <input type="text" name="kecamatan_ahliwaris" placeholder="Kecamatan" class="form-control" value="{{ $ahli_waris->kecamatan_ahliwaris }}">
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="kota_administrasi" class=" form-control-label">Kota Administrasi</label>
                                                        <input type="text" name="kota_administrasi" placeholder="Kota Administrasi" class="form-control" value="{{ $ahli_waris->kota_ahliwaris }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer" style="height: 60px">
                              <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection
