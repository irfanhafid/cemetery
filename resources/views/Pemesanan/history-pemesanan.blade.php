@php
    $tumpangan = $data;
@endphp

@extends('layouts.app') @section('content')

    <div class="">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Riwayat Permohonan</strong>
                        </div>
                        <div class="card-body">
                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Nomor Permohonan</th>
                                    <th>Tanggal Permohonan</th>
                                    <th>Jenis IPTM</th>
                                    <th>Nama Pemakaman</th>
                                    <th>Status Permohonan</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $d)
                                    <tr>
                                        <td>{{ $d->nomor_permohonan }}</td>
                                        <td>{{ $d->tanggal_permohonan }}</td>
                                        <td>{{ ucwords($d->tipe_pesanan) }}</td>
                                        <td>{{ $d->nama_pemakaman }}</td>
                                        <td>
                                            @if($d->status == 1)
                                                <span class="badge badge-pill badge-warning">Waiting</span>
                                            @elseif($d->status == 2)
                                                <span class="badge badge-pill badge-success">Approved</span>
                                            @else
                                                <span class="badge badge-pill badge-danger">Reject</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a class="btn btn-info" href="{{ url("pemakaman/pesanan/$d->tipe_pesanan/$d->id/detail") }}"><i class="fa fa-eye"></i> Lihat Detail</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
