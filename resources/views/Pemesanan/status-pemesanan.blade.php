@extends('layouts.app') @section('content')

    <div class="">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Daftar Pengajuan {{ucwords($tipe)}}</strong>
                            <!-- {{$data}} -->
                        </div>
                        <div class="card-body">
                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    @if($tipe == 'tumpangan')
                                    <th>Nomor Permohonan</th>
                                    <th>Nama Almarhum Lama</th>
                                    <th>Nama Almarhum Baru</th>
                                    <th>Nama Ahli Waris</th>
                                    <th>Status Permohonan</th>
                                    @elseif($tipe == 'perpanjangan')
                                    <th>Nomor Permohonan</th>
                                    <th>Tanggal Permohonan</th>
                                    <th>Nama Almarhum</th>
                                    <th>Nama Ahliwaris</th>
                                    <th>Status Permohonan</th>
                                    @endif
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                    @if(count($data)>0)
                                        @foreach($data as $tabelStatusPemesanan)
                                            @php
                                                $status = $tabelStatusPemesanan->status;
                                                if($tipe == 'tumpangan'){
                                                  $almarhum_baru = $tabelStatusPemesanan->almarhum;
                                                }
                                            @endphp
                                            <tr>
                                                @if($tipe == 'tumpangan')
                                                <td>{{$tabelStatusPemesanan->nomor_permohonan}}</td>
                                                <td>{{$tabelStatusPemesanan->nama_almarhum}}</td>
                                                <td>{{$almarhum_baru->nama_almarhum}}</td>
                                                <td>{{$tabelStatusPemesanan->nama_ahliwaris}}</td>
                                                <td>
                                                    @if($status == 1)
                                                        <span class="badge badge-pill badge-warning">Waiting</span>
                                                    @elseif($status == 2)
                                                        <span class="badge badge-pill badge-success">Approved</span>
                                                    @else
                                                        <span class="badge badge-pill badge-danger">Reject</span>
                                                    @endif
                                                </td>
                                                <td style="text-align: center">
                                                    <a class="btn btn-info" href="{{ url('pemakaman/pesanan/tumpangan/'. $tabelStatusPemesanan->id .'/detail') }}"><i class="fa fa-eye"></i> Lihat Detail</a>
                                                </td>
                                                @elseif($tipe == 'perpanjangan')
                                                <td>{{$tabelStatusPemesanan->nomor_permohonan}}</td>
                                                <td>{{$tabelStatusPemesanan->tanggal_permohonan}}</td>
                                                <td>{{$tabelStatusPemesanan->nama_almarhum}}</td>
                                                <td>{{$tabelStatusPemesanan->nama_ahliwaris}}</td>
                                                <td>
                                                    @if($status == 1)
                                                        <span class="badge badge-pill badge-warning">Waiting</span>
                                                    @elseif($status == 2)
                                                        <span class="badge badge-pill badge-success">Approved</span>
                                                    @else
                                                        <span class="badge badge-pill badge-danger">Reject</span>
                                                    @endif
                                                </td>
                                                <td style="text-align: center">
                                                    <a class="btn btn-info" href="{{ url('pemakaman/pesanan/perpanjangan/'. $tabelStatusPemesanan->id .'/detail') }}"><i class="fa fa-eye"></i> Lihat Detail</a>
                                                </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->

@endsection
