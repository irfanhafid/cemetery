@push('style')
    <style>
        .card-body .card {
            border: none;
        }
        .card-body .card .card-header{
            border: none;
        }
        .modal-dialog {
            width: 100%;
            max-width: 800px;
            max-height: 800px;
        }
    </style>
@endpush

@php $user = Auth::user(); @endphp
@extends((( Auth::user()->role == 'admin_tpu'|| Auth::user()->role =='admin_dinas') ? 'layouts.app' : 'layouts.user.app' ))
@if(Auth::user()->role == 'member')
@section('header-class')
    {{"main-header-area"}}
@endsection
@endif
@section('content')

@if(Auth::user()->role == 'member')
<div class="slider_area">
    <div class=" d-flex align-items-center "style="background-color: #2952a3;height: 200px; background-size: cover;background-repeat: no-repeat"></div>
</div>

    <div class="">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Detail Permohonan</strong>
                            <div class="pull-right">
                                @if($data->status == 2)
                                    @if($user->role == 'admin_tpu')
                                        <a class="btn" href="{{ url("pemakaman/pesanan/$tipe_pesanan/$data->id/detail/print") }}" target="_blank"><i class="fa fa-print"></i> Cetak Surat Permohonan</a>
                                    @endif
                                @elseif($data->status == 1)
                                    <span class="badge badge-pill badge-{{'warning'}}">{{'Waiting'}}</span>
                                @elseif($data->status == 3)

                                @endif
                                <!-- @if($user->role == 'admin_tpu')
                                    <a class="btn btn-primary" href="{{ url("pemakaman/pesanan/$tipe_pesanan") }}" style="border-radius: 100%; margin-left: 20px"><i class="fa fa-arrow-left"></i></a>
                                @else
                                    <a class="btn btn-primary" href="{{ url('pemakaman/pesanan/details') }}" style="border-radius: 100%; margin-left: 20px"><i class="fa fa-arrow-left"></i></a>

                                @endif -->
                            </div>
                        </div>
                        @if($data->status == 3)
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="alert alert-danger alert-dismissible fade show" style="background-color: #ffe6e6; border-color: white;">
                                <span class="badge badge-pill badge-danger" style="">Ditolak</span>
                                {{$data->catatan}}
                                <a class="btn btn-info pull-right" href="{{ url("pemakaman/pesanan/$tipe_pesanan/$data->id/detail/edit") }}"><i class="fa fa-edit"></i>Edit</a>
                              </div>
                            </div>
                          </div>
                        </div>
                        @endif
                        <form id="form-approval" action="{{ url("pemakaman/pesanan/$tipe_pesanan/$data->id/detail/approval") }}">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="card">
                                            @php
                                                if($tipe_pesanan == 'tumpangan'){
                                                    $data = $data ?? $Tumpangan;
                                                    $tumpangan = $data;
                                                    $iptm_lama = $data->iptm;
                                                    $almarhum_lama = $iptm_lama->almarhum;
                                                    $almarhum_baru = $data->almarhum;
                                                    $makam = $iptm_lama->makam;
                                                    $pemakaman = $makam->pemakaman;
                                                    $ahli_waris = $almarhum_baru->ahliWaris;
                                                }
                                                else{
                                                    $data = $data ?? $Perpanjangan;
                                                    $perpanjangan = $data;
                                                    $iptm_lama = $data->iptm;
                                                    $almarhum_lama = $iptm_lama->almarhum;
                                                    $makam = $iptm_lama->makam;
                                                    $pemakaman = $makam->pemakaman;
                                                    $ahli_waris = $data->ahliWaris;
                                                }
                                            @endphp
                                            <div class="card-header"><strong>Informasi Makam</strong></div>
                                            <div class="card-body card-block" style="color: black">
                                                <div class="form-group">
                                                    <label for="nomor_iptm" class=" form-control-label">Nomor IPTM</label>
                                                    <input type="text" name="nomor_iptm" placeholder="Nomor IPTM" class="form-control" value="{{ $iptm_lama->nomor_iptm }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="nama_almarhum" class=" form-control-label">Nama Almarhum</label>
                                                    <input type="text" name="nama_almarhum" placeholder="Nama Almarhum" class="form-control" value="{{ $almarhum_lama->nama_almarhum }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="tanggal_wafat" class=" form-control-label">Tanggal Wafat</label>
                                                    <input type="text" name="tanggal_wafat" placeholder="Tanggal Wafat" class="form-control" value="{{ $almarhum_lama->tanggal_wafat }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="nama_pemakaman" class=" form-control-label">Lokasi Pemakaman</label>
                                                    <input type="text" name="nama_pemakaman" placeholder="Pemakaman" class="form-control" value="{{ $pemakaman->nama_pemakaman }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="blok" class=" form-control-label" style="color: black">Blok</label>
                                                    <input type="text" name="blok" placeholder="Blok" class="form-control" value="{{ $makam->blok }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="blad" class=" form-control-label">Blad</label>
                                                    <input type="text" name="blad" placeholder="Blad" class="form-control" value="{{ $makam->blad }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="petak" class=" form-control-label">Petak</label>
                                                    <input type="text" name="petak" placeholder="Petak" class="form-control" value="{{ $makam->petak }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="masa_berlaku" class=" form-control-label">Tanggal Kadaluwarsa</label>
                                                    <input type="date" name="masa_berlaku" placeholder="Tanggal Kadaluwarsa" class="form-control" value="{{ $iptm_lama->masa_berlaku }}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if($tipe_pesanan == 'tumpangan')
                                        <div class="col-sm-4">
                                            <div class="card" style="margin-bottom: 0">
                                                <div class="card-header"><strong>Informasi Almarhum Baru</strong></div>
                                                <div class="card-body card-block">
                                                    <div class="row">
                                                        <div class="form-group col-sm-6">
                                                            <label for="nama_almarhum" class=" form-control-label">Nama Almarhum</label>
                                                            <input type="text" name="nama_almarhum" placeholder="Nama Almarhum" class="form-control" value="{{ $almarhum_baru->nama_almarhum }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-6">
                                                            <label for="tanggal_wafat" class=" form-control-label">Tanggal Wafat</label>
                                                            <input type="text" name="tanggal_wafat" placeholder="Tanggal Wafat" class="form-control" value="{{ $almarhum_baru->tanggal_wafat }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-6">
                                                            <label for="nomor_ktp_almarhum" class=" form-control-label">No KTP Almarhum</label>
                                                            <input type="text" name="nomor_ktp_almarhum" placeholder="No KTP Almarhum" class="form-control" value="{{ $almarhum_baru->nomor_ktp_almarhum }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-6">
                                                            <label for="file_ktp_almarhum" class=" form-control-label">Foto KTP Almarhum</label>
                                                            <a href="#modal-file_ktp_almarhum" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $almarhum_baru->file_ktp_almarhum ? 'file-image-o' : 'ban' }}"></i></a>
                                                            <div class="modal fade" id="modal-file_ktp_almarhum" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content" style="z-index: 20; position:fixed; margin-top: 150px;">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto KTP Almarhum</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img class="img-responsive" src="{{ asset("Document/Tumpangan/$almarhum_baru->file_ktp_almarhum") }}" alt="" style="max-height:250px;">
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-6">
                                                            <label for="nomor_kk_almarhum" class=" form-control-label">No KK Almarhum</label>
                                                            <input type="text" name="nomor_kk_almarhum" placeholder="No KK Almarhum" class="form-control" value="{{ $almarhum_baru->nomor_kk_almarhum }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-6">
                                                            <label for="file_kk_almarhum" class=" form-control-label">Foto KK Almarhum</label>
                                                            <a href="#modal-file_kk_almarhum" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $almarhum_baru->file_kk_almarhum ? 'file-image-o' : 'ban' }}"></i></a>
                                                            <div class="modal fade" id="modal-file_kk_almarhum" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content" style="z-index: 20; position:fixed; margin-top: 150px;">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto KK Almarhum</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img class="img-responsive" src="{{ asset("Document/Tumpangan/$almarhum_baru->file_kk_almarhum") }}" alt="" style="max-height:250px;">
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="card">
                                                <div class="card-header"><strong>Surat Keterangan & Lampiran</strong></div>
                                                <div class="card-body card-block">
                                                    <div class="row">
                                                        <div class="form-group col-sm-6">
                                                            <label for="file_iptm_asli" class=" form-control-label">Foto IPTM Asli</label>
                                                            <a href="#modal-file_iptm_asli" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $tumpangan->file_iptm_asli ? 'file-image-o' : 'ban' }}"></i></a>
                                                            <div class="modal fade" id="modal-file_iptm_asli" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content" style="z-index: 20; position:fixed; margin-top: 150px;">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto KTP Almarhum</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img class="img-responsive" src="{{ asset("Document/Tumpangan/$tumpangan->file_iptm_asli") }}" alt="" style="max-height:250px;">
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-4">
                                                            <label for="nomor_kehilangan_kepolisian" class=" form-control-label">No SKCK</label>
                                                            <input type="text" name="nomor_sk_kehilangan_kepolisian" placeholder="No" class="form-control" value="{{ $tumpangan->nomor_sk_kehilangan_kepolisian }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="tanggal_kehilangan_kepolisian" class=" form-control-label">Tanggal</label>
                                                            <input type="date" name="tanggal_sk_kehilangan_kepolisian" placeholder="Tanggal" class="form-control" value="{{ $tumpangan->tanggal_sk_kehilangan_kepolisian }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="file_sk_kehilangan_kepolisian" class=" form-control-label">Foto</label>
                                                            <a href="#modal-file_sk_kehilangan_kepolisian" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $tumpangan->file_sk_kehilangan_kepolisian ? 'file-image-o' : 'ban' }}"></i></a>
                                                            <div class="modal fade" id="modal-file_sk_kehilangan_kepolisian" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content" style="z-index: 20; position:fixed; margin-top: 150px;">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto KTP Almarhum</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img class="img-responsive" src="{{ asset("Document/Tumpangan/$tumpangan->file_sk_kehilangan_kepolisian") }}" alt="" style="max-height:250px;">
                                                                        </div>`
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-4">
                                                            <label for="nomor_sp_rtrw" class=" form-control-label">No Surat Pengantar RT/RW</label>
                                                            <input type="text" name="nomor_sp_rtrw" placeholder="No" class="form-control" value="{{ $almarhum_baru->nomor_sp_rtrw }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="tanggal_sp_rtrw" class=" form-control-label">Tanggal Surat Pengantar RT/RW</label>
                                                            <input type="date" name="tanggal_sp_rtrw" placeholder="-" class="form-control" value="{{ $almarhum_baru->tanggal_sp_rtrw }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="file_sp_rtrw" class=" form-control-label">Foto Surat Pengantar RT/RW</label>
                                                            <a href="#modal-file_sp_rtrw" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $almarhum_baru->file_sp_rtrw ? 'file-image-o' : 'ban' }}"></i></a>
                                                            <div class="modal fade" id="modal-file_sp_rtrw" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content" style="z-index: 20; position:fixed; margin-top: 150px;">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto Surat Pengantar RT/RW</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img class="img-responsive" src="{{ asset("Document/Tumpangan/$almarhum_baru->file_sp_rtrw") }}" alt="" style="max-height:250px;">
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-4">
                                                            <label for="nomor_sk_kematian_rs" class=" form-control-label">No Surat Kematian RS/Puskesmas</label>
                                                            <input type="text" name="nomor_sk_kematian_rs" placeholder="No" class="form-control" value="{{ $almarhum_baru->nomor_sk_kematian_rs }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="tanggal_sk_kematian_rs" class=" form-control-label">Tanggal Surat Kematian RS/Puskesmas</label>
                                                            <input type="date" name="tanggal_sk_kematian_rs" placeholder="Tanggal" class="form-control" value="{{ $almarhum_baru->tanggal_sk_kematian_rs }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="file_sk_kematian_rs" class=" form-control-label">Foto Surat Kematian RS/Puskesmas</label>
                                                            <a href="#modal-file_sk_kematian_rs" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $almarhum_baru->file_sk_kematian_rs ? 'file-image-o' : 'ban' }}"></i></a>
                                                            <div class="modal fade" id="modal-file_sk_kematian_rs" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content" style="z-index: 20; position:fixed; margin-top: 150px;">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto Surat Kematian RS/Puskesmas</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img class="img-responsive" src="{{ asset("Document/Tumpangan/$almarhum_baru->file_sk_kematian_rs") }}" alt="" style="max-height:250px;">
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-sm-4">
                                        <div class="card">
                                            <div class="card-header"><strong>Informasi Ahli Waris</strong></div>
                                            <div class="card-body card-block">
                                                <div class="form-group">
                                                    <label for="nama_ahliwaris" class=" form-control-label">Nama Ahli Waris</label>
                                                    <input type="text" name="nama_ahliwaris" placeholder="Nama Ahli Waris" class="form-control" value="{{ $ahli_waris->nama_ahliwaris }}" readonly>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <label for="nomor_ktp_ahliwaris" class=" form-control-label">No KTP Ahli Waris</label>
                                                        <input type="text" name="nomor_ktp_ahliwaris" placeholder="No KTP Ahli Waris" class="form-control" value="{{ $ahli_waris->nomor_ktp_ahliwaris }}" readonly>
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="file_ktp_ahliwaris" class=" form-control-label">Foto KTP Ahli Waris</label>
                                                        <a href="#modal-file_ktp_ahliwaris" data-toggle="modal" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-{{ $ahli_waris->file_ktp_ahliwaris ? 'file-image-o' : 'ban' }}"></i></a>
                                                        <div class="modal fade" id="modal-file_ktp_ahliwaris" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content" style="z-index: 20; position:fixed; margin-top: 150px;">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Foto KTP Ahli Waris</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body text-center">
                                                                        <img class="img-responsive" src="{{ asset("Document/Tumpangan/$ahli_waris->file_ktp_ahliwaris") }}" alt="" style="max-height:250px;">
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="telepon_ahliwaris" class=" form-control-label">Telepon Ahli Waris</label>
                                                    <input type="text" name="telepon_ahliwaris" placeholder="Telepon Ahli Waris" class="form-control" value="{{ $ahli_waris->telepon_ahliwaris }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="hubungan_ahliwaris" class=" form-control-label">Hubungan</label>
                                                    <input type="text" name="hubungan_ahliwaris" placeholder="Hubungan" class="form-control" value="{{ $ahli_waris->hubungan_ahliwaris }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="alamat_ahliwaris" class=" form-control-label">Alamat</label>
                                                    <textarea type="text" name="alamat_ahliwaris" placeholder="Alamat" class="form-control" rows="5" readonly>{{ $ahli_waris->alamat_ahliwaris }}</textarea>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-3">
                                                        <label for="rt_ahliwaris" class=" form-control-label">RT</label>
                                                        <input type="text" name="rt_ahliwaris" placeholder="RT" class="form-control" value="{{ $ahli_waris->rt_ahliwaris }}" readonly>
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label for="rw_ahliwaris" class=" form-control-label">RW</label>
                                                        <input type="text" name="rw_ahliwaris" placeholder="RW" class="form-control" value="{{ $ahli_waris->rw_ahliwaris }}" readonly>
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="kelurahan_ahliwaris" class=" form-control-label">Kelurahan</label>
                                                        <input type="text" name="kelurahan_ahliwaris" placeholder="Kelurahan" class="form-control" value="{{ $ahli_waris->kelurahan_ahliwaris }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <label for="kecamatan_ahliwaris" class=" form-control-label">Kecamatan</label>
                                                        <input type="text" name="kecamatan_ahliwaris" placeholder="Kecamatan" class="form-control" value="{{ $ahli_waris->kecamatan_ahliwaris }}" readonly>
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="kota_administrasi" class=" form-control-label">Kota Administrasi</label>
                                                        <input type="text" name="kota_administrasi" placeholder="Kota Administrasi" class="form-control" value="{{ $ahli_waris->kota_ahliwaris }}" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="status">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @elseif(Auth::user()->role == 'admin_tpu')
    <div class="">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Detail Permohonan</strong>
                            <div class="pull-right">
                                @if($data->status == 2)
                                    @if($user->role == 'admin_tpu')
                                        <a class="btn" href="{{ url("pemakaman/pesanan/$tipe_pesanan/$data->id/detail/print") }}" target="_blank"><i class="fa fa-print"></i> Cetak Surat Permohonan</a>
                                    @endif
                                @else
                                    <span class="badge badge-pill badge-{{ $data->status == 1 ? 'warning' : 'danger' }}">{{ $data->status == 1 ? 'Waiting' : 'Reject' }}</span>
                                @endif
                                <!-- @if($user->role == 'admin_tpu')
                                    <a class="btn btn-primary" href="{{ url("pemakaman/pesanan/$tipe_pesanan") }}" style="border-radius: 100%; margin-left: 20px"><i class="fa fa-arrow-left"></i></a>
                                @else
                                    <a class="btn btn-primary" href="{{ url('pemakaman/pesanan/details') }}" style="border-radius: 100%; margin-left: 20px"><i class="fa fa-arrow-left"></i></a>

                                @endif -->
                            </div>
                        </div>
                        @if($data->status == 3)
                        <div class="card-body">
                          <div class="row">
                            <div class="col-md-12">
                              <div class="alert alert-danger alert-dismissible fade show" style="background-color: #ffe6e6; border-color: white;">
                                <span class="badge badge-pill badge-danger" style="">Ditolak</span>
                                <!-- {{$data->catatan}}
                                <a class="btn btn-info pull-right" href="{{ url("pemakaman/pesanan/$tipe_pesanan/$data->id/detail/edit") }}"><i class="fa fa-edit"></i>Edit</a> -->
                              </div>
                            </div>
                          </div>
                        </div>
                        @endif
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="card">
                                            @php
                                                if($tipe_pesanan == 'tumpangan'){
                                                    $data = $data ?? $Tumpangan;
                                                    $tumpangan = $data;
                                                    $iptm_lama = $data->iptm;
                                                    $almarhum_lama = $iptm_lama->almarhum;
                                                    $almarhum_baru = $data->almarhum;
                                                    $makam = $iptm_lama->makam;
                                                    $pemakaman = $makam->pemakaman;
                                                    $ahli_waris = $almarhum_baru->ahliWaris;
                                                }
                                                else{
                                                    $data = $data ?? $Perpanjangan;
                                                    $perpanjangan = $data;
                                                    $iptm_lama = $data->iptm;
                                                    $almarhum_lama = $iptm_lama->almarhum;
                                                    $makam = $iptm_lama->makam;
                                                    $pemakaman = $makam->pemakaman;
                                                    $ahli_waris = $data->ahliWaris;
                                                }
                                            @endphp
                                            <div class="card-header"><strong>Informasi Makam</strong></div>
                                            <div class="card-body card-block">
                                                <div class="form-group">
                                                    <label for="nomor_iptm" class="form-control-label" style="color: black">
                                                        Nomor IPTM
                                                    </label>
                                                    <input type="text" name="nomor_iptm" placeholder="Nomor IPTM" class="form-control" value="{{ $iptm_lama->nomor_iptm }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="nama_almarhum" class=" form-control-label" style="color: black">
                                                        Nama Almarhum
                                                    </label>
                                                    <input type="text" name="nama_almarhum" placeholder="Nama Almarhum" class="form-control" value="{{ $almarhum_lama->nama_almarhum }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="tanggal_wafat" class=" form-control-label" style="color: black">Tanggal Wafat</label>
                                                    <input type="text" name="tanggal_wafat" placeholder="Tanggal Wafat" class="form-control" value="{{ $almarhum_lama->tanggal_wafat }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="nama_pemakaman" class=" form-control-label" style="color: black">Lokasi Pemakaman</label>
                                                    <input type="text" name="nama_pemakaman" placeholder="Pemakaman" class="form-control" value="{{ $pemakaman->nama_pemakaman }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="blok" class=" form-control-label" style="color: black">Blok</label>
                                                    <input type="text" name="blok" placeholder="Blok" class="form-control" value="{{ $makam->blok }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="blad" class=" form-control-label" style="color: black">Blad</label>
                                                    <input type="text" name="blad" placeholder="Blad" class="form-control" value="{{ $makam->blad }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="petak" class=" form-control-label" style="color: black">Petak</label>
                                                    <input type="text" name="petak" placeholder="Petak" class="form-control" value="{{ $makam->petak }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="masa_berlaku" class=" form-control-label" style="color: black">Tanggal Kadaluwarsa</label>
                                                    <input type="date" name="masa_berlaku" placeholder="Tanggal Kadaluwarsa" class="form-control" value="{{ $iptm_lama->masa_berlaku }}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if($tipe_pesanan == 'tumpangan')
                                        <div class="col-sm-4">
                                            <div class="card" style="margin-bottom: 0">
                                                <div class="card-header"><strong>Informasi Almarhum Baru</strong></div>
                                                <div class="card-body card-block">
                                                    <div class="row">
                                                        <div class="form-group col-sm-6">
                                                            <label for="nama_almarhum" class=" form-control-label">Nama Almarhum</label>
                                                            <input type="text" name="nama_almarhum" placeholder="Nama Almarhum" class="form-control" value="{{ $almarhum_baru->nama_almarhum }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-6">
                                                            <label for="tanggal_wafat" class=" form-control-label">Tanggal Wafat</label>
                                                            <input type="text" name="tanggal_wafat" placeholder="Tanggal Wafat" class="form-control" value="{{ $almarhum_baru->tanggal_wafat }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-6">
                                                            <label for="nomor_ktp_almarhum" class=" form-control-label">No KTP Almarhum</label>
                                                            <input type="text" name="nomor_ktp_almarhum" placeholder="No KTP Almarhum" class="form-control" value="{{ $almarhum_baru->nomor_ktp_almarhum }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-6">
                                                            <label for="file_ktp_almarhum" class=" form-control-label">Foto KTP Almarhum</label>
                                                            <div class="file-preview">
                                                                <a href="#modal-file_ktp_almarhum" data-toggle="modal" style="padding: 10px"><i class="fa fa-{{ $almarhum_baru->file_ktp_almarhum ? 'file-image-o' : 'ban' }}"></i></a>
                                                                @if($almarhum_baru->file_ktp_almarhum && file_exists(public_path("Document/Tumpangan/$almarhum_baru->file_ktp_almarhum")))
                                                                    <a href="{{ url("pemakaman/pesanan/tumpangan/$id/detail/download/file_ktp_almarhum") }}" style="padding: 10px"><i class="fa fa-download"></i></a>
                                                                @endif
                                                            </div>

                                                            <div class="modal fade" id="modal-file_ktp_almarhum" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content" style="z-index: 20; position:fixed; margin-top: 150px;">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto KTP Almarhum</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img class="img-responsive" src="{{ asset("Document/Tumpangan/$almarhum_baru->file_ktp_almarhum") }}" alt="" style="max-height:250px;">
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-6">
                                                            <label for="nomor_kk_almarhum" class=" form-control-label">No KK Almarhum</label>
                                                            <input type="text" name="nomor_kk_almarhum" placeholder="No KK Almarhum" class="form-control" value="{{ $almarhum_baru->nomor_kk_almarhum }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-6">
                                                            <label for="file_kk_almarhum" class=" form-control-label">Foto KK Almarhum</label>
                                                            <div class="file-preview">
                                                                <a href="#modal-file_kk_almarhum" data-toggle="modal" target="_blank" style="padding: 10px"><i class="fa fa-{{ $almarhum_baru->file_kk_almarhum ? 'file-image-o' : 'ban' }}"></i></a>
                                                                @if($almarhum_baru->file_kk_almarhum && file_exists(public_path("Document/Tumpangan/$almarhum_baru->file_kk_almarhum")))
                                                                    <a href="{{ url("pemakaman/pesanan/tumpangan/$id/detail/download/file_kk_almarhum") }}" style="padding: 10px"><i class="fa fa-download"></i></a>
                                                                @endif
                                                            </div>
                                                            <div class="modal fade" id="modal-file_kk_almarhum" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content" style="z-index: 20; position:fixed; margin-top: 150px;">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto KK Almarhum</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img class="img-responsive" src="{{ asset("Document/Tumpangan/$almarhum_baru->file_kk_almarhum") }}" alt="" style="max-height:250px;">
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header"><strong>Surat Keterangan & Lampiran</strong></div>
                                                <div class="card-body card-block">
                                                    <div class="row">
                                                        <div class="form-group col-sm-6">
                                                            <label for="file_iptm_asli" class=" form-control-label">Foto IPTM Asli</label>
                                                            <div class="file-preview">
                                                                <a href="#modal-file_iptm_asli" data-toggle="modal" target="_blank" style="padding: 10px"><i class="fa fa-{{ $tumpangan->file_iptm_asli ? 'file-image-o' : 'ban' }}"></i></a>
                                                                @if($tumpangan->file_iptm_asli && file_exists(public_path("Document/Tumpangan/$tumpangan->file_iptm_asli")))
                                                                    <a href="{{ url("pemakaman/pesanan/tumpangan/$id/detail/download/file_iptm_asli") }}" style="padding: 10px"><i class="fa fa-download"></i></a>
                                                                @endif
                                                            </div>
                                                            <div class="modal fade" id="modal-file_iptm_asli" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content" style="z-index: 20; position:fixed; margin-top: 150px;">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto KTP Almarhum</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img class="img-responsive" src="{{ asset("Document/Tumpangan/$tumpangan->file_iptm_asli") }}" alt="" style="max-height:250px;">
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-4">
                                                            <label for="nomor_kehilangan_kepolisian" class=" form-control-label">No Surat Kehilangan Kepolisian</label>
                                                            <input type="text" name="nomor_sk_kehilangan_kepolisian" class="form-control" value="{{ $tumpangan->nomor_sk_kehilangan_kepolisian }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="tanggal_kehilangan_kepolisian" class=" form-control-label">Tanggal Surat Kehilangan Kepolisian</label>
                                                            <input type="date" name="tanggal_sk_kehilangan_kepolisian" placeholder="Tanggal" class="form-control" value="{{ $tumpangan->tanggal_sk_kehilangan_kepolisian }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="file_sk_kehilangan_kepolisian" class=" form-control-label">Foto Surat Kehilangan Kepolisian</label>
                                                            <div class="file-preview">
                                                                <a href="#modal-file_sk_kehilangan_kepolisian" data-toggle="modal" target="_blank" style="padding: 10px"><i class="fa fa-{{ $tumpangan->file_sk_kehilangan_kepolisian ? 'file-image-o' : 'ban' }}"></i></a>
                                                                @if($tumpangan->file_sk_kehilangan_kepolisian && file_exists(public_path("Document/Tumpangan/$tumpangan->file_sk_kehilangan_kepolisian")))
                                                                    <a href="{{ url("pemakaman/pesanan/tumpangan/$id/detail/download/file_sk_kehilangan_kepolisian") }}" style="padding: 10px"><i class="fa fa-download"></i></a>
                                                                @endif
                                                            </div>
                                                            <div class="modal fade" id="modal-file_sk_kehilangan_kepolisian" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content" style="z-index: 20; position:fixed; margin-top: 150px;">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto Surat Kehilangan Kepolisian</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img class="img-responsive" src="{{ asset("Document/Tumpangan/$tumpangan->file_sk_kehilangan_kepolisian") }}" alt="" style="max-height:250px;">
                                                                        </div>`
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-4">
                                                            <label for="nomor_sp_rtrw" class=" form-control-label">No Surat Pengantar RT/RW</label>
                                                            <input type="text" name="nomor_sp_rtrw" class="form-control" value="{{ $almarhum_baru->nomor_sp_rtrw }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="tanggal_sp_rtrw" class=" form-control-label">Tanggal Surat Pengantar RT/RW</label>
                                                            <input type="date" name="tanggal_sp_rtrw" placeholder="-" class="form-control" value="{{ $almarhum_baru->tanggal_sp_rtrw }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="file_sp_rtrw" class=" form-control-label">Foto Surat Pengantar RT/RW</label>
                                                            <div class="file-preview">
                                                                <a href="#modal-file_sp_rtrw" data-toggle="modal" target="_blank" style="padding: 10px"><i class="fa fa-{{ $almarhum_baru->file_sp_rtrw ? 'file-image-o' : 'ban' }}"></i></a>
                                                                @if($almarhum_baru->file_sp_rtrw && file_exists(public_path("Document/Tumpangan/$almarhum_baru->file_sp_rtrw")))
                                                                    <a href="{{ url("pemakaman/pesanan/tumpangan/$id/detail/download/file_sp_rtrw") }}" style="padding: 10px"><i class="fa fa-download"></i></a>
                                                                @endif
                                                            </div>
                                                            <div class="modal fade" id="modal-file_sp_rtrw" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content" style="z-index: 20; position:fixed; margin-top: 150px;">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto Surat Pengantar RT/RW</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img class="img-responsive" src="{{ asset("Document/Tumpangan/$almarhum_baru->file_sp_rtrw") }}" alt="" style="max-height:250px;">
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-4">
                                                            <label for="nomor_sk_kematian_rs" class=" form-control-label">No Surat Kematian RS/Puskesmas</label>
                                                            <input type="text" name="nomor_sk_kematian_rs" placeholder="No" class="form-control" value="{{ $almarhum_baru->nomor_sk_kematian_rs }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="tanggal_sk_kematian_rs" class=" form-control-label">Tanggal Surat Kematian RS/Puskesmas</label>
                                                            <input type="date" name="tanggal_sk_kematian_rs" placeholder="Tanggal" class="form-control" value="{{ $almarhum_baru->tanggal_sk_kematian_rs }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="file_sk_kematian_rs" class=" form-control-label">Foto SK Kematian RS/Puskesmas</label>
                                                            <div class="file-preview">
                                                                <a href="#modal-file_sk_kematian_rs" data-toggle="modal" target="_blank" style="padding: 10px"><i class="fa fa-{{ $almarhum_baru->file_sk_kematian_rs ? 'file-image-o' : 'ban' }}"></i></a>
                                                                @if($almarhum_baru->file_sk_kematian_rs && file_exists(public_path("Document/Tumpangan/$almarhum_baru->file_sk_kematian_rs")))
                                                                    <a href="{{ url("pemakaman/pesanan/tumpangan/$id/detail/download/file_sk_kematian_rs") }}" style="padding: 10px"><i class="fa fa-download"></i></a>
                                                                @endif
                                                            </div>
                                                            <div class="modal fade" id="modal-file_sk_kematian_rs" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content" style="z-index: 20; position:fixed; margin-top: 150px;">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto SK Kematian RS/Puskesmas</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img class="img-responsive" src="{{ asset("Document/Tumpangan/$almarhum_baru->file_sk_kematian_rs") }}" alt="" style="max-height:250px;">
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-4">
                                                            <label for="nomor_sk_kematian_kelurahan" class=" form-control-label">No Surat Kematian Kelurahan</label>
                                                            <input type="text" name="nomor_sk_kematian_kelurahan" placeholder="No" class="form-control" value="{{ $almarhum_baru->nomor_sk_kematian_kelurahan }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="tanggal_sk_kematian_kelurahan" class=" form-control-label">Tanggal Surat Kematian Kelurahan</label>
                                                            <input type="date" name="tanggal_sk_kematian_kelurahan" placeholder="Tanggal" class="form-control" value="{{ $almarhum_baru->tanggal_sk_kematian_kelurahan }}" readonly>
                                                        </div>
                                                        <div class="form-group col-sm-4">
                                                            <label for="file_sk_kematian_kelurahan" class=" form-control-label">Foto SK Kematian Kelurahan</label>
                                                            <div class="file-preview">
                                                                <a href="#modal-file_sk_kematian_kelurahan" data-toggle="modal" target="_blank" style="padding: 10px"><i class="fa fa-{{ $almarhum_baru->file_sk_kematian_kelurahan ? 'file-image-o' : 'ban' }}"></i></a>
                                                                @if($almarhum_baru->file_sk_kematian_kelurahan && file_exists(public_path("Document/Tumpangan/$almarhum_baru->file_sk_kematian_kelurahan")))
                                                                    <a href="{{ url("pemakaman/pesanan/tumpangan/$id/detail/download/file_sk_kematian_kelurahan") }}" style="padding: 10px"><i class="fa fa-download"></i></a>
                                                                @endif
                                                            </div>
                                                            <div class="modal fade" id="modal-file_sk_kematian_kelurahan" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content" style="z-index: 20; position:fixed; margin-top: 150px;">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Foto SK Kematian Kelurahan</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body text-center">
                                                                            <img class="img-responsive" src="{{ asset("Document/Tumpangan/$almarhum_baru->file_sk_kematian_kelurahan") }}" alt="" style="max-height:250px;">
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-sm-4">
                                        <div class="card">
                                            <div class="card-header"><strong>Informasi Ahli Waris</strong></div>
                                            <div class="card-body card-block">
                                                <div class="form-group">
                                                    <label for="nama_ahliwaris" class=" form-control-label" style="color: black">
                                                        Nama Ahli Waris
                                                    </label>
                                                    <input type="text" name="nama_ahliwaris" placeholder="Nama Ahli Waris" class="form-control" value="{{ $ahli_waris->nama_ahliwaris }}" readonly>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <label for="nomor_ktp_ahliwaris" class=" form-control-label" style="color: black">
                                                            No KTP Ahli Waris
                                                        </label>
                                                        <input type="text" name="nomor_ktp_ahliwaris" placeholder="No KTP Ahli Waris" class="form-control" value="{{ $ahli_waris->nomor_ktp_ahliwaris }}" readonly>
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="file_ktp_ahliwaris" class=" form-control-label" style="color: black">
                                                            Foto KTP Ahli Waris
                                                        </label><br>
                                                        <div class="file-preview">
                                                            <a href="#modal-file_ktp_ahliwaris" data-toggle="modal" style="padding: 10px"><i class="fa fa-{{ $ahli_waris->file_ktp_ahliwaris ? 'file-image-o' : 'ban' }}"></i></a>
                                                            @if($ahli_waris->file_ktp_ahliwaris && file_exists(public_path("Document/Tumpangan/$ahli_waris->file_ktp_ahliwaris")))
                                                                <a href="{{ url("pemakaman/pesanan/tumpangan/$id/detail/download/file_ktp_ahliwaris") }}" style="padding: 10px"><i class="fa fa-download"></i></a>
                                                            @endif
                                                        </div>
                                                        <div class="modal fade" id="modal-file_ktp_ahliwaris" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content" style="z-index: 20; position:fixed; margin-top: 150px;">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="color: black">Foto KTP Ahli Waris</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body text-center">
                                                                        <img class="img-responsive" src="{{ asset("Document/Tumpangan/$ahli_waris->file_ktp_ahliwaris") }}" alt="" style="max-height:250px;">
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="telepon_ahliwaris" class=" form-control-label" style="color: black">
                                                        Telepon Ahli Waris
                                                    </label>
                                                    <input type="text" name="telepon_ahliwaris" placeholder="Telepon Ahli Waris" class="form-control" value="{{ $ahli_waris->telepon_ahliwaris }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="hubungan_ahliwaris" class=" form-control-label" style="color: black">
                                                        Hubungan
                                                    </label>
                                                    <input type="text" name="hubungan_ahliwaris" placeholder="Hubungan" class="form-control" value="{{ $ahli_waris->hubungan_ahliwaris }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="alamat_ahliwaris" class=" form-control-label" style="color: black">
                                                        Alamat
                                                    </label>
                                                    <textarea type="text" name="alamat_ahliwaris" placeholder="Alamat" class="form-control" rows="5" readonly>{{ $ahli_waris->alamat_ahliwaris }}</textarea>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-3">
                                                        <label for="rt_ahliwaris" class=" form-control-label" style="color: black">RT</label>
                                                        <input type="text" name="rt_ahliwaris" placeholder="RT" class="form-control" value="{{ $ahli_waris->rt_ahliwaris }}" readonly>
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label for="rw_ahliwaris" class=" form-control-label" style="color: black">RW</label>
                                                        <input type="text" name="rw_ahliwaris" placeholder="RW" class="form-control" value="{{ $ahli_waris->rw_ahliwaris }}" readonly>
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="kelurahan_ahliwaris" class=" form-control-label" style="color: black">Kelurahan</label>
                                                        <input type="text" name="kelurahan_ahliwaris" placeholder="Kelurahan" class="form-control" value="{{ $ahli_waris->kelurahan_ahliwaris }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-sm-6">
                                                        <label for="kecamatan_ahliwaris" class=" form-control-label" style="color: black">Kecamatan</label>
                                                        <input type="text" name="kecamatan_ahliwaris" placeholder="Kecamatan" class="form-control" value="{{ $ahli_waris->kecamatan_ahliwaris }}" readonly>
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <label for="kota_administrasi" class=" form-control-label" style="color: black">Kota Administrasi</label>
                                                        <input type="text" name="kota_administrasi" placeholder="Kota Administrasi" class="form-control" value="{{ $ahli_waris->kota_ahliwaris }}" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($data->status == 1)
                                <div class="card-footer" style="height: 60px">
                                    @if($user->role == 'admin_tpu')
                                        <button type="submit" class="btn-approve btn btn-primary btn-sm pull-right" data-target="#modal-approve" data-toggle="modal">
                                            <i class="fa fa-dot-circle-o"></i> Approve
                                        </button>
                                        <button class="btn-reject btn btn-danger btn-sm pull-right mr-2" data-target="#modal-reject" data-toggle="modal">
                                            <i class="fa fa-ban"></i> Reject
                                        </button>
                                    @endif

                                    @if($tipe_pesanan == 'perpanjangan')
                                            <form id="form-approval" action="{{ url("pemakaman/pesanan/$tipe_pesanan/$data->id/detail/approval") }}">
                                            <div class="modal fade" id="modal-approve" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog" role="document" style="max-width: 800px; width: 800px">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Isi Data Surat</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body text-center">
                                                            <div class="row form-group">
                                                                <div class="input-group">
                                                                    <div class="col col-md-3">
                                                                        <label for="hf-email" class=" form-control-label">
                                                                            Nomor Surat
                                                                            <label style="color: red">*</label>
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-12 col-md-9">
                                                                        <input type="text" name="nomor_surat" class="form-control" required=""
                                                                               oninvalid="this.setCustomValidity('Nomor Surat tidak boleh kosong')"
                                                                               oninput="setCustomValidity('')">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col col-md-3"><label for="hf-email" class=" form-control-label">
                                                                        Tanggal Surat
                                                                        <label style="color: red">*</label>
                                                                    </label>
                                                                </div>
                                                                <div class="col-12 col-md-9">
                                                                    <input type="date" name="tanggal_surat" class="form-control" required=""
                                                                           oninvalid="this.setCustomValidity('Tanggal Surat tidak boleh kosong')"
                                                                           oninput="setCustomValidity('')">
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col col-md-3"><label for="hf-email" class=" form-control-label">
                                                                        Daftar Jenazah
                                                                        <br>
                                                                        ( Sesuai dengan surat IPTM )
                                                                    </label>
                                                                </div>
                                                                <div class="col-12 col-md-9">
                                                                    <input type="text" name="daftar_jenazah" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col col-md-3"><label for="hf-email" class=" form-control-label">
                                                                        Berlaku Dari
                                                                        <label style="color: red">*</label>
                                                                    </label>
                                                                </div>
                                                                <div class="col-12 col-md-9">
                                                                    <input type="date" name="tanggal_berlaku_dari" class="form-control" required=""
                                                                           oninvalid="this.setCustomValidity('Tanggal berlaku dari tidak boleh kosong')"
                                                                           oninput="setCustomValidity('')">
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col col-md-3"><label for="hf-email" class=" form-control-label">
                                                                        Berlaku Sampai
                                                                        <label style="color: red">*</label>
                                                                    </label>
                                                                </div>
                                                                <div class="col-12 col-md-9">
                                                                    <input type="date" name="tanggal_berlaku_sampai" class="form-control" required=""
                                                                           oninvalid="this.setCustomValidity('Tanggal berlaku sampai tidak boleh kosong')"
                                                                           oninput="setCustomValidity('')">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn-submit-approve btn btn-primary" type="submit" >Submit</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </form>

                                            <form id="form-reject" action="{{ url("pemakaman/pesanan/$tipe_pesanan/$data->id/detail/reject") }}">
                                            <div class="modal fade" id="modal-reject" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog" role="document" style="max-width: 500px;">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Penjelasan Penolakan Permohonan</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body text-center">
                                                            <div class="row form-group">
                                                                <div class="col-md-12">
                                                                    <label class="">Catatan</label>
                                                                      <textarea name="catatan" class="form-control" id="rejection" cols="30" rows="10"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn-submit-reject btn btn-primary">
                                                                Simpan
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </form>
                                        @else
                                            <form id="form-approval" action="{{ url("pemakaman/pesanan/$tipe_pesanan/$data->id/detail/approval") }}">
                                            <div class="modal fade" id="modal-approve" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog" role="document" style="max-width: 700px;">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Isi data surat</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body text-center">
                                                            <div class="row form-group">
                                                                <div class="input-group">
                                                                    <div class="col col-md-3">
                                                                        <label for="hf-email" class=" form-control-label">
                                                                            Nomor Surat
                                                                            <label style="color: red">*</label>
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-12 col-md-9">
                                                                        <input type="text" name="nomor_surat" class="form-control" required=""
                                                                               oninvalid="this.setCustomValidity('Nomor Surat tidak boleh kosong')"
                                                                               oninput="setCustomValidity('')">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col col-md-3"><label for="hf-email" class=" form-control-label">
                                                                        Tanggal Surat
                                                                        <label style="color: red">*</label>
                                                                    </label>
                                                                </div>
                                                                <div class="col-12 col-md-9">
                                                                    <input type="date" name="tanggal_surat" class="form-control" required=""
                                                                           oninvalid="this.setCustomValidity('Tanggal Surat tidak boleh kosong')"
                                                                           oninput="setCustomValidity('')">
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col col-md-3"><label for="hf-email" class=" form-control-label">
                                                                        Daftar Jenazah
                                                                        <br>
                                                                        ( Sesuai dengan surat IPTM )
                                                                    </label>
                                                                </div>
                                                                <div class="col-12 col-md-9">
                                                                    <input type="text" name="daftar_jenazah" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col col-md-3"><label for="hf-email" class=" form-control-label">
                                                                        Berlaku Dari
                                                                        <label style="color: red">*</label>
                                                                    </label>
                                                                </div>
                                                                <div class="col-12 col-md-9">
                                                                    <input type="date" name="tanggal_berlaku_dari" class="form-control" required=""
                                                                           oninvalid="this.setCustomValidity('Tanggal berlaku dari tidak boleh kosong')"
                                                                           oninput="setCustomValidity('')">
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col col-md-3"><label for="hf-email" class=" form-control-label">
                                                                        Berlaku Sampai
                                                                        <label style="color: red">*</label>
                                                                    </label>
                                                                </div>
                                                                <div class="col-12 col-md-9">
                                                                    <input type="date" name="tanggal_berlaku_sampai" class="form-control" required=""
                                                                           oninvalid="this.setCustomValidity('Tanggal berlaku sampai tidak boleh kosong')"
                                                                           oninput="setCustomValidity('')">
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn-submit-approve btn btn-primary" type="submit" >Submit</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                          </form>

                                          <form id="form-reject" action="{{ url("pemakaman/pesanan/$tipe_pesanan/$data->id/detail/reject") }}">
                                            <div class="modal fade" id="modal-reject" data-backdrop="false" tabindex="-1" role="dialog" aria-hidden="true">
                                                <div class="modal-dialog" role="document" style="max-width: 500px;">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Penjelasan Penolakan Permohonan</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body text-center">
                                                            <div class="row form-group">
                                                                <div class="col-md-12">
                                                                    <label class="">Catatan</label>
                                                                    <textarea name="catatan" class="form-control" id="rejection" cols="30" rows="10"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn-submit-reject btn btn-primary">
                                                                Simpan
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                          </form>
                                        @endif
                                </div>
                            @endif
                            <input type="hidden" name="status">
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('.btn-approve, .btn-reject').click(function (e) {
                e.preventDefault();
            });
            $('.btn-submit-approve').click(function (e) {
                var nullRequiredField = false;

                $("#form-approval").find(':input').each(function(){
                    if($(this).attr("required")){
                        if($(this).val() === null || $(this).val() === undefined || $(this).val() === "" ){
                            nullRequiredField = true;
                            return false;
                        }
                    }

                });
                $('input[name=status]').val(2);
            });

            $('.btn-submit-reject').click(function (e) {
                var $reason = $('.modal textarea[name=catatan]').val();
                $('input[name=status]').val(3);

                if(!$reason){
                    e.preventDefault();
                    alert('Reason is required.')
                }
                else{
                    $('#form-reject').submit();
                }
            });
            $(':input').not('.modal input, .modal textarea').prop('readonly', true)
            $('.form-group .fa-ban').parent().prop('style', 'padding: 10px; pointer-events: none')
        });
    </script>
@endsection
