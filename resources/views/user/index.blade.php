@extends('layouts.app') @section('content')
    <div class="">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Pengelolaan Data User</strong>
                        </div>
                        <div class="card-body">
                            <div class="col-md-12 text-right">
                                <a href="{{ url('user/create') }}" class="btn-add btn btn-success" style="margin-bottom: 20px"><i class="fa fa-plus"></i> Tambah</a>
                            </div>

                            <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <td>Nama</td>
                                    <td>Email</td>
                                    <td>Role</td>
                                    <td>Nama Pemakaman</td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $d)
                                    <tr>
                                        <td>{{ $d->fullname }}</td>
                                        <td>{{ $d->email }}</td>
                                        <td>{{ $d->role }}</td>
                                        <td>{{!empty($d->Pemakaman->nama_pemakaman) ? $d->Pemakaman->nama_pemakaman:''}}</td>
                                        <td>
                                            <a href="{{ url("user/$d->id/edit") }}" class="btn-edit btn btn-primary"><i class="fa fa-edit"> Edit</i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
