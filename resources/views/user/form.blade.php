<div class="col-md-6">
    <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="" class=" form-control-label" style="color: black">
                    Nama
                    <label style="color: red">*</label>
                </label>
            </div>
            <div class="col-12 col-md-9">
                <input type="text" name="fullname" class="form-control" value="{{ $User->fullname ?? old('fullname') }}">
                @if($errors->has('fullname'))
                    <small class="help-block form-text" style="color: red">{{ $errors->first('fullname') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="" class=" form-control-label" style="color: black">
                    Email
                    <label style="color: red">*</label>
                </label>
            </div>
            <div class="col-12 col-md-9">
                <input type="text" name="email" class="form-control" value="{{ $User->email ?? old('email') }}">
                @if($errors->has('email'))
                    <small class="help-block form-text" style="color: red">{{ $errors->first('email') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="" class=" form-control-label" style="color: black">
                    Password
                    <label style="color: red">*</label>
                </label>
            </div>
            <div class="col-12 col-md-9">
                <input name="password" type="password" class="form-control">
                @if($errors->has('password'))
                    <small class="help-block form-text" style="color: red">{{ $errors->first('password') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="" class=" form-control-label" style="color: black">
                    Alamat
                    <label style="color: red">*</label>
                </label>
            </div>
            <div class="col-12 col-md-9">
                <input type="text" name="address" class="form-control" value="{{ $User->address ?? old('address') }}">
                @if($errors->has('address'))
                    <small class="help-block form-text" style="color: red">{{ $errors->first('address') }}</small>
                @endif
            </div>
        </div>
        {{--<div class="row form-group">
            <div class="col col-md-3">
                <label for="" class=" form-control-label">Jenis Kelamin</label>
            </div>
            <div class="col-12 col-md-9">
                <input type="text" name="gender" class="form-control" value="{{ $User->gender ?? old('gender') }}">
                @if($errors->has('fullname'))
                    <small class="help-block form-text" style="color: red">{{ $errors->first('fullname') }}</small>
                @endif
            </div>
        </div>--}}
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="" class=" form-control-label" style="color: black">
                    Gender
                    <label style="color: red">*</label>
                </label>
            </div>
            <div class="col-12 col-md-9">
                <select name="gender" id="" class="form-control">
                    <option value="">-Select-</option>
                    <option value="male" {{ isset($User) && $User->gender == 'male' ? 'selected' : '' }}>Laki - laki</option>
                    <option value="female" {{ isset($User) && $User->gender == 'female' ? 'selected' : '' }}>Perempuan</option>
                </select>
                @if($errors->has('gender'))
                    <small class="help-block form-text" style="color: red">{{ $errors->first('gender') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="" class=" form-control-label" style="color: black">
                    Role
                    <label style="color: red">*</label>
                </label>
            </div>
            <div class="col-12 col-md-9">
                <select name="role" id="" class="form-control">
                    <option value="">-Select-</option>
                    <option value="admin_tpu" {{ isset($User) && $User->role == 'admin_tpu' ? 'selected' : '' }}>Admin TPU</option>
                    <option value="member" {{ isset($User) && $User->role == 'member' ? 'selected' : '' }}>Member</option>
                </select>
                @if($errors->has('role'))
                    <small class="help-block form-text" style="color: red">{{ $errors->first('role') }}</small>
                @endif
            </div>
        </div>
        <div class="row form-group">
            <div class="col col-md-3">
                <label for="" class=" form-control-label" style="color: black">TPU (Khusus Admin TPU)</label>
            </div>
            <div class="col-12 col-md-9">
              <select name="pemakaman_id" class="form-control">
                  <option value="">-Select-</option>
                  @foreach(\App\Pemakaman::pluck('nama_pemakaman', 'id') as $key => $namaTPU)
                      <option value="{{ $key }}" {{ isset($Pemakaman) && $Pemakaman->id == $key ? 'selected' : '' }}>{{ $namaTPU }}</option>
                  @endforeach
              </select>
            </div>
        </div>
    </form>
</div>
