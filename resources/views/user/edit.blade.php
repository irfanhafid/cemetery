@extends('layouts.app')
@section('content')
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ url("user/$id/edit") }}" method="POST">
                    @csrf
                    @method('PATCH')
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Ubah Data User</strong>
                            <a class="btn btn-primary pull-right" href="{{ url('user/list') }}" style="border-radius: 100%; margin-left: 20px"><i class="fa fa-arrow-left"></i></a>
                        </div>

                        <div class="card-body">
                            @include('user.form')
                        </div>
                        <div class="card-footer text-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
