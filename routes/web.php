<?php

use RealRashid\SweetAlert\Facades\Alert;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', 'UserController@index');
Route::get('/dashboard',    function(){

    return view('home');
});

Route::get('/logout', function(){
    return redirect('/')->with(Auth::logout());
});

//Tidak Dipakai
Route::get('/test', 'UserController@TestFunction');

// Tidak dipakai
Route::post('/user/register', "UserController@register");

Route::prefix('IPTM')->group(function () {

    Route::get('/pesanan', 'IPTMController@ShowAllIPTM');

    //Tidak Dipakai
    Route::get('/buat-permohonan', 'IPTMController@ShowNewIPTMForm');

    //All IPTM
    Route::get('/{inputType}/sukses/{id}','IPTMController@ShowSuksesInput');

    //Tidak Dipakai
    Route::get('/{inputType}/cetak/{id}','IPTMController@PrintIPTM');

    //IPTM Perpanjangan
    Route::get('/perpanjangan','PerpanjanganController@ShowFormPerpanjanganBaru');
    Route::post('/perpanjangan/submit','PerpanjanganController@SubmitPerpanjangan');
    Route::get('/perpanjangan/{id}','PerpanjanganController@ShowFormPerpanjangan');
    //Route::get('/perpanjangan/cetak/{id}', 'PerpanjanganController@PrintIPTMPerpanjangan');

    //IPTM Tumpangan
    Route::get('/tumpangan','TumpanganController@ShowFormTumpanganBaru');
    Route::get('/tumpangan/{id}','TumpanganController@ShowFormTumpangan');
    Route::post('/tumpangan/submit','TumpanganController@SubmitTumpangan');

    Route::get('/riwayat', 'IPTMController@riwayat');
});


Route::prefix('pemakaman')->group(function () {
    Route::get('/', 'PemakamanController@index');
    Route::get('/info', 'PemakamanController@info');
    Route::match(['get', 'put'], '/create', 'PemakamanController@create');
    Route::match(['get', 'patch'], '/{id}/edit', 'PemakamanController@edit');
    Route::match(['get', 'patch'], '/editpemakaman/{id}', 'PemakamanController@edit');
    Route::delete('/{id}/delete', 'PemakamanController@destroy');

    Route::get('/pesanan/riwayat', 'PemesananController@riwayat');
   // Route::get('/','PemakamanController@ShowPemakaman');
    Route::get('/details/{id}','PemakamanController@ShowPemakaman');
    Route::get('/detailsbyadmin/{id}','PemakamanController@ShowPemakamanByAdminTPU');
    Route::get('/pesanan/details','PemesananController@pesananDetail');
    Route::get('/pesanan/{tipe_pesanan}','PemesananController@showStatusPemesanan');
    Route::get('/pesanan/{tipe_pesanan}/{id}/detail','PemesananController@showDetailPesanan');
    Route::get('/pesanan/{tipe_pesanan}/{id}/detail/download/{attr}','PemesananController@downloadFile');
    Route::get('/pesanan/{tipe_pesanan}/{id}/detail/approval','PemesananController@approvalDetailPesanan');
    Route::get('/pesanan/{tipe_pesanan}/{id}/detail/reject','PemesananController@rejectDetailPesanan');
    Route::match(['get', 'patch'],'/pesanan/{tipe_pesanan}/{id}/detail/edit','PemesananController@editDetailPesanan');
    Route::get('/pesanan/{tipe_pesanan}/{id}/detail/print','PemesananController@printDetailPesanan');
    Route::get('/pesanan/{tipe_pesanan}/{id}/detail/show/{file}','PemesananController@showFile');

    Route::get('/jadwal',function (){
        return view('Pemakaman.jadwal-pemakaman');
    });
    Route::get('/expired', 'IPTMController@ShowMakamKadaluarsa');
    Route::get('/kelola', 'PemakamanController@kelola');

    Route::get('/cari','PemakamanController@ShowAllPemakaman');
    Route::get('/edit/{id}','PemakamanController@ShowDetailPemakamanByUser');

    Route::post('/edit/{id}', 'PemakamanController@SubmitEditPemakaman');

    Route::get('/tata-cara',function(){

        return view('Pemakaman.tata-cara');
    });
});

Route::prefix('jadwal-pemakaman')->group(function () {
    Route::get('/', 'JadwalPemakamanController@index');
    Route::match(['get', 'put'], '/create', 'JadwalPemakamanController@create');
    Route::match(['get', 'patch'], '/{id}/edit', 'JadwalPemakamanController@edit');
    Route::get('/{id}/delete', 'JadwalPemakamanController@delete');
});

Route::prefix('user')->group(function () {
    Route::get('/list', 'UserController@list');
    Route::match(['get', 'put'], '/create', 'UserController@create');
    Route::match(['get', 'patch'], '/{id}/edit', 'UserController@edit');
    Route::delete('/{id}/delete', 'UserController@destrprofileoy');
});


Route::post('/insertTumpangan','PemakamanController@insertMakamTumpangan');
Route::get('/izinTumpangan',function (){
    return view('Tumpangan.insert_tumpangan');
});

Route::post('/pendaftaranPemakaman','PemakamanController@regisPemakaman');
Route::get('/pendaftaranPemakaman',function (){
    return view('Pemakaman.registerpic');
});
Route::get('/daftarMakam',function(){
    return view('Makam.insert_datamakam');
});
Route::get('/lihatMakam/{id}','PemakamanController@ShowMakam');
Route::get('/viewSukses/{id}','IPTMController@ShowSuksesInput');

Route::get('/profile',function (){
    return view('auth.profile');
});
Route::match(['get', 'post'], 'profile/edit', 'UserController@profile');

Route::prefix('import')->group(function()
{
  //Route::get('/', 'ImportController@iptm');
  Route::match(['get', 'put'], '/iptm', 'ImportController@ImportIptm');
  Route::match(['get', 'put'], '/makam','ImportController@ImportMakam');
  Route::match(['get', 'put'], '/almarhum','ImportController@ImportAlmarhum');
  Route::match(['get', 'put'], '/ahliwaris','ImportController@ImportAhliwaris');
  Route::match(['get', 'patch'], '/makam/edit/{id}','ImportController@editMakam');
  Route::match(['get', 'patch'], '/ahliwaris/edit/{id}','ImportController@editAhliwaris');
  Route::match(['get', 'patch'], '/iptm/edit/{id}','ImportController@editIPTM');
  Route::match(['get', 'patch'], '/almarhum/edit/{id}','ImportController@editAlmarhum');
  Route::delete('/makam/delete/{id}','ImportController@deleteMakam');
  Route::delete('/ahliwaris/delete/{id}','ImportController@deleteAhliwaris');
  Route::delete('/iptm/delete/{id}','ImportController@deleteIPTM');
  Route::delete('/almarhum/delete/{id}','ImportController@deleteAlmarhum');
});

Route::get("/json/iptm", "IPTMController@RequestGetIPTM");
Route::get("/json/iptm-by-no", "IPTMController@RequestGetIPTMByNo");
Route::get("/json/getCountData", "IPTMController@RequestCountDataAdminTPU");

Route::get('/download/{file}', function ($file='') {
    return response()->download(public_path('document/download/'.$file));
});
