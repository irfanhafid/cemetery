<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Almarhum extends Model
{
    protected $table='almarhum';
    protected $primaryKey='id';
    protected $fillable = [
        'ahli_waris_id',
        'iptm_id',
        'nama_almarhum',
        'tanggal_wafat',
        'nomor_ktp_almarhum',
        'file_ktp_almarhum',
        'nomor_kk_almarhum',
        'file_kk_almarhum',
        'nomor_sp_rtrw',
        'tanggal_sp_rtrw',
        'file_sp_rtrw',
        'nomor_sk_kematian_rs',
        'tanggal_sk_kematian_rs',
        'file_sk_kematian_rs',
        'nomor_sk_kematian_kelurahan',
        'tanggal_sk_kematian_kelurahan',
        'file_sk_kematian_kelurahan',
    ];

    public function ahliWaris()
    {
        return $this->belongsTo(AhliWaris::class, 'ahli_waris_id');
    }

    public function iptm()
    {
        return $this->belongsTo(iptm::class, 'iptm_id');
    }
}
