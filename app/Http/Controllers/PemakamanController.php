<?php

namespace App\Http\Controllers;

use App\Tumpangan;
use App\Pemakaman;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;


class PemakamanController extends Controller
{
    public function index()
    {
        $data = Pemakaman::whereHas('users', function ($q) {
            $q->where('users.id', Auth::user()->id);
        })->get();

        $data = Pemakaman::all();

        return view('Pemakaman.index', get_defined_vars());
    }

    public function create(Request $request)
    {
        if($request->method() == 'PUT'){
            $this->validate($request,[
                'kepala_pemakaman' => 'required',
                'nip_kepala_pemakaman' => 'required',
                'nama_pemakaman' => 'required',
                'alamat_pemakaman' => 'required',
                'kelurahan_pemakaman' => 'required',
                'kecamatan_pemakaman' => 'required',
                'kota_pemakaman' => 'required',
                'provinsi_pemakaman' => 'required',
                'telepon_pemakaman' => 'required',
                'kodepos_pemakaman' => 'required',
                'email_pemakaman' => 'required',
                'photo_pemakaman' => 'required',
            ], [
                'kepala_pemakaman.required' => 'field kepala tpu tidak boleh kosong',
                'kepala_pemakaman.alpha' => 'field kepala tpu tidak bisa diisi angka',
                'nip_kepala_pemakaman.required' => 'field NIP kepala tpu tidak boleh kosong',
                'nama_pemakaman.required' => 'field nama tpu tidak boleh kosong',
                'alamat_pemakaman.required' => 'field alamat tpu tidak boleh kosong',
                'kelurahan_pemakaman.required' => 'field kelurahan tpu tidak boleh kosong',
                'kecamatan_pemakaman.required' => 'field kecamatan tpu tidak boleh kosong',
                'kota_pemakaman.required' => 'field kota tpu tidak boleh kosong',
                'provinsi_pemakaman.required' => 'field provinsi tpu tidak boleh kosong',
                'telepon_pemakaman.required' => 'field telepon tpu tidak boleh kosong',
                'kodepos_pemakaman.required' => 'field kode pos tpu tidak boleh kosong',
                'email_pemakaman.required' => 'field email tpu tidak boleh kosong',
                'photo_pemakaman.required' => 'field foto tpu tidak boleh kosong',
            ]);

            if($file = $request->photo_pemakaman){
                $fileName = $file->getClientOriginalName();
                $file->move(public_path('/images/pemakaman'), $fileName);
            }
            $pemakaman = $request->all();
            $pemakaman['photo_pemakaman'] = $fileName ?? null;
            Pemakaman::create($pemakaman);

            $data = Pemakaman::all();
            Alert::success('Success!', 'Data TPU berhasil ditambah');
            return redirect('pemakaman')->with('success', 'Pemakaman sudah dibuat');
        }

        return view('Pemakaman.create');
    }

    public function edit(Request $request, $id)
    {
        $Pemakaman = Pemakaman::find($id);

        if($request->method() == 'PATCH'){
            $this->validate($request,[
                'kepala_pemakaman' => 'required',
                'nip_kepala_pemakaman' => 'required',
                'nama_pemakaman' => 'required',
                'alamat_pemakaman' => 'required',
                'kelurahan_pemakaman' => 'required',
                'kecamatan_pemakaman' => 'required',
                'kota_pemakaman' => 'required',
                'provinsi_pemakaman' => 'required',
                'telepon_pemakaman' => 'required',
                'kodepos_pemakaman' => 'required',
                'email_pemakaman' => 'required',
                'photo_pemakaman' => 'required',
            ], [
                'kepala_pemakaman.required' => 'field kepala tpu tidak boleh kosong',
                'nip_kepala_pemakaman.required' => 'field NIP kepala tpu tidak boleh kosong',
                'nama_pemakaman.required' => 'field nama tpu tidak boleh kosong',
                'alamat_pemakaman.required' => 'field alamat tpu tidak boleh kosong',
                'kelurahan_pemakaman.required' => 'field kelurahan tpu tidak boleh kosong',
                'kecamatan_pemakaman.required' => 'field kecamatan tpu tidak boleh kosong',
                'kota_pemakaman.required' => 'field kota tpu tidak boleh kosong',
                'provinsi_pemakaman.required' => 'field provinsi tpu tidak boleh kosong',
                'telepon_pemakaman.required' => 'field telepon tpu tidak boleh kosong',
                'kodepos_pemakaman.required' => 'field kode pos tpu tidak boleh kosong',
                'email_pemakaman.required' => 'field email tpu tidak boleh kosong',
                'photo_pemakaman.required' => 'field foto tpu tidak boleh kosong',
            ]);

            if($file = $request->photo_pemakaman){
                $fileName = $file->getClientOriginalName();
                $file->move(public_path('/images/pemakaman'), $fileName);

                $oldPath = public_path("images/pemakaman/$Pemakaman->photo_pemakaman");
                if(\File::exists($oldPath) && $Pemakaman->photo_pemakaman)
                    unlink((string)$oldPath);
            }
            $Pemakaman->update($request->all());
            $Pemakaman->photo_pemakaman = $fileName ?? null;
            $Pemakaman->save();

            $data = Pemakaman::all();
            Alert::success('Success!', 'Data TPU diperbaharui');
            return redirect('pemakaman')->with('success', 'Pemakaman sudah diperbaharui');
        }

        return view('Pemakaman.edit', get_defined_vars());
    }

    public function destroy($id)
    {
        $Pemakaman = Pemakaman::find($id);
        $Pemakaman->delete();
        Alert::success('Success!', 'Data TPU terhapus');

        return redirect('pemakaman');
    }
    //
    public function regisPemakaman(Request $request){
        $rules=[
            'fullname' => 'required',
            'email' => 'required|string|email|max:255|unique:user',
            'password' => 'required|string|min:6',
            'confirmation_password' => 'required|same:password',
            'address' => 'required',
            'gender' => 'required',
            'images' => 'required',
            'namaPemakaman'=>'required',
            'KepalaPemakaman'=>'required',
            'NIPKepalaPemakaman'=>'required',
            'alamatPemakaman'=>'required',
            'kotaPemakaman'=>'required',
            'provinsiPemakaman'=>'required',
            'kodeposPemakaman'=>'required',
            'emailPemakaman'=>'required|string|email|max:255|unique:Pemakaman',
            'jumlahPemakaman'=>'required',
            'luasPemakaman'=>'required',
            'deskripsiPemakaman'=>'required|string',
            'photoPemakaman'=>'required',

        ];
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return redirect('/pendaftaranPemakaman')->withErrors($validator)->withInput();
        }

        $file = $request->file('photoPemakaman');
        $file->move(public_path('/images/Pemakaman'), $file->getClientOriginalName());

        $pemakaman = new Pemakaman();
        $pemakaman->namaPemakaman = $request['namaPemakaman'];
        $pemakaman->alamatPemakaman = $request['alamatPemakaman'];
        $pemakaman->kotaPemakaman = $request['kotaPemakaman'];
        $pemakaman->provinsiPemakaman = $request['provinsiPemakaman'];
        $pemakaman->kodeposPemakaman = $request['kodeposPemakaman'];
        $pemakaman->emailPemakaman = $request['emailPemakaman'];
        $pemakaman->jumlahPemakaman = $request['jumlahPemakaman'];
        $pemakaman->luasPemakaman = $request['luasPemakaman'];
        $pemakaman->deskripsiPemakaman = $request['deskripsiPemakaman'];
        $pemakaman->photoPemakaman = $file->getClientOriginalName();
        $pemakaman->save();

        $file = $request->file('images');
        $file->move(public_path('/images/profile'), $file->getClientOriginalName());
        $usr = new User();
        $usr->fullname = $request['fullname'];
        $usr->email = $request['email'];
        $usr->password = bcrypt($request['password']);
        $usr->KepalaPemakaman = bcrypt($request['KepalaPemakaman']);
        $usr->NIPKepalaPemakaman = bcrypt($request['NIPKepalaPemakaman']);
        $usr->address = $request['address'];
        $usr->gender = $request['gender'];
        $usr->pemakaman_id=$pemakaman->pemakamanid;
        $usr->images = $file->getClientOriginalName();
        $usr->role = 'admin_tpu';

        $usr->save();
        return redirect('/')->with('register_success','Welcome,');


    }

    public function kelola()
    {
        $Pemakaman = Pemakaman::all();

        return view('Makam.manage-makam', get_defined_vars());
    }

    public function ShowPemakaman($id){
        $pemakaman = DB::table('pemakaman')
            ->where('pemakaman.id','=',$id)
            ->get();

        session(['pemakaman_id' => $id]);

        return view('pemakaman.lihat_detailByUser')->with([
            "pemakamanumum" => $pemakaman,
        ]);

    }
    public function ShowPemakamanByAdminTPU($id){
        $pemakaman = DB::table('pemakaman')
            ->where('pemakaman.id','=',$id)
            ->get();

        return view('pemakaman.lihat_detail')->with([
            "pemakamanumum" => $pemakaman,
        ]);

    }


    public function ShowMakam($id){
        $makams = DB::table('makam')
            ->join('Pemakaman','makam.pemakaman_id','=','Pemakaman.id')
            ->where('Pemakaman.id','=',$id)
            ->get();
        $makamtumpangan =DB::table('Pemakaman')
            -> join('makam','Pemakaman.id','=','makam.pemakaman_id')
            ->get();
        return view ('Makam.show_Makam')->with([
            "listmakam"=>$makams,
            "makam_tumpangan"=>$makamtumpangan
        ]);
    }

    public function ShowDetailPemakamanByUser($pemakamanid){
        $pemakamandetail = DB::table('Pemakaman')
            ->where('Pemakaman.id','=',$pemakamanid)
            ->get();
        $makamtumpangan = DB::table('Pemakaman','makam')
            ->join('makam','Pemakaman.id','=','makam.pemakaman_id')
            ->get();
        $users = DB::table('Pemakaman','user')
            ->join('user','Pemakaman.id','=','user.pemakaman_id')
            ->get();
        return view('Pemakaman.lihat_detailByUser')->with([
            "detailpemakaman"=>$pemakamandetail,
            "makamstumpangan"=>$makamtumpangan,
            "pic"=>$users
        ]);
    }

    //untuk membuat
    public function insertMakamTumpangan(Request $request){
        $rules=[
            'nama_Almarhum'=>'required',
            'lokasi_Pemakaman'=>'required',
            'blok'=>'required',
            'blad'=>'required',
            'petak'=>'required',
            'MasaBerlaku'=>'required',
            'nama_Ahliwaris'=>'required',
            'alamat_Ahliwaris'=>'required',
            'RT_Ahliwaris'=>'required',
            'RW_Ahliwaris'=>'required',
            'email_Ahliwaris'=>'required',
            'phone_Ahliwaris'=>'required',

        ];

        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return redirect('/daftar_makam')->withErrors($validator)->withInput();
        }

        $user = Auth::user();
      //  dd($user);
        $file = $request->file('photo_makam');
        $file->move(public_path('/images/makamtumpangan'), $file->getClientOriginalName());
        $file = $request->file('photo_makam2');
        $file->move(public_path('/images/makamtumpangan'), $file->getClientOriginalName());
        $file = $request->file('photo_makam3');
        $file->move(public_path('/images/makamtumpangan'), $file->getClientOriginalName());

        $makam_tumpangan = new Tumpangan();
        $makam_tumpangan->nama_Almarhum = $request['nama_Almarhum'];
        $makam_tumpangan->tanggal_Wafat = $request['tanggal_Wafat'];
        $makam_tumpangan->lokasi_Pemakaman = $request['lokasi_Pemakaman'];
        $makam_tumpangan->pemakaman_id = $user->pemakaman_id;
        $makam_tumpangan->blok = $request['blok'];
        $makam_tumpangan->blad = $request['blad'];
        $makam_tumpangan->petak = $request['petak'];
        $makam_tumpangan->MasaBerlaku = $request['MasaBerlaku'];
        $makam_tumpangan->nama_Ahliwaris = $request['nama_Ahliwaris'];
        $makam_tumpangan->alamat_Ahliwaris = $request['alamat_Ahliwaris'];
        $makam_tumpangan->RT_Ahliwaris = $request['RT_Ahliwaris'];
        $makam_tumpangan->RW_Ahliwaris = $request['RW_Ahliwaris'];
        $makam_tumpangan->email_Ahliwaris = $request['email_Ahliwaris'];
        $makam_tumpangan->phone_Ahliwaris = $request['phone_Ahliwaris'];
        $makam_tumpangan->masa_Berlakuizin = $request['masa_Berlakuizin'];
        $makam_tumpangan->avail_tumpangan = 'Tersedia';
        $makam_tumpangan->photo_makam = $file->getClientOriginalName();
        $makam_tumpangan->photo_makam2 = $file->getClientOriginalName();
        $makam_tumpangan->photo_makam3= $file->getClientOriginalName();
        $makam_tumpangan->save();
        return redirect('/')->with('register_success','Welcome,');
    }

    public function SubmitEditPemakaman(Request $request, $id){

        $file = $request->file('photo_pemakaman');
        $file->move(public_path('/images/pemakaman'), $file->getClientOriginalName());

        $pemakaman = Pemakaman::find($id);
        $pemakaman->nama_pemakaman = $request['nama_pemakaman'];
        $pemakaman->email_pemakaman = $request['email_pemakaman'];
        $pemakaman->jumlah_pemakaman = $request['jumlah_pemakaman'];
        $pemakaman->luas_pemakaman = $request['luas_pemakaman'];
        $pemakaman->alamat_pemakaman = $request['alamat_pemakaman'];
        $pemakaman->deskripsi_pemakaman = $request['deskripsi_pemakaman'];
        $pemakaman->photo_pemakaman = $file->getClientOriginalName();

        $pemakaman->save();


        return redirect("/pemakaman");
    }

    public function ShowAllPemakaman(){
        $pemakaman = Pemakaman::all();
        return view('Pemakaman.cari-Pemakaman')->with([
            'listPemakaman'=>$pemakaman
        ]);
    }

}
