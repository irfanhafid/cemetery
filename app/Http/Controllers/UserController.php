<?php

namespace App\Http\Controllers;

use App\Pemakaman;
use App\Perpanjangan;
use App\product;
use App\Tumpangan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        if($request->ajax()){
            $pemakamanId = Auth::user()->pemakaman_id;

            $tumpangan = Tumpangan::select([
                'tumpangan.id AS id',
                'tumpangan.status',
                \DB::raw("DATE_FORMAT(tanggal_permohonan, '%M') AS month"),
            ])
                ->join('iptm', 'iptm.id', '=', 'tumpangan.iptm_id')
                ->join('makam', 'makam.id', '=', 'iptm.makam_id')
                ->join('pemakaman', 'pemakaman.id', '=', 'makam.pemakaman_id')
                ->join('users','users.id','=','tumpangan.user_id')
                ->where('users.role','=','member')
                ->where('pemakaman.id','=',$pemakamanId)
                ->get();

            $perpanjangan = Perpanjangan::select([
                'perpanjangan.id AS id',
                'perpanjangan.status',
                \DB::raw("DATE_FORMAT(tanggal_permohonan, '%M') AS month"),
            ])
                ->join('iptm', 'iptm.id', '=', 'perpanjangan.iptm_id')
                ->join('makam', 'makam.id', '=', 'iptm.makam_id')
                ->join('pemakaman', 'pemakaman.id', '=', 'makam.pemakaman_id')
                ->join('users','users.id','=','perpanjangan.user_id')
                ->where('users.role','=','member')
                ->where('pemakaman.id','=',$pemakamanId)
                ->get();

            $data = collect();
            foreach ($tumpangan as $item) {
                $data->push($item);
            }
            foreach ($perpanjangan as $item) {
                $data->push($item);
            }
            $group = $data->groupBy('month');

            $data1['labels'] = [];
            $data2['labels'] = [];
            $waiting = [];
            $approve = [];
            $reject = [];
            $arrayTumpangan = [];
            $arrayPerpanjangan = [];
            foreach($group as $month => $item){
                array_push($data1['labels'], $month);
                array_push($waiting, $item->where('status', 1)->count());
                array_push($approve, $item->where('status', 2)->count());
                array_push($reject, $item->where('status', 3)->count());

                array_push($data2['labels'], $month);
                array_push($arrayTumpangan, $tumpangan->where('month', $month)->count());
                array_push($arrayPerpanjangan, $perpanjangan->where('month', $month)->count());
            }
            $data1['datasets'] = [
                [
                    'label' => 'Waiting',
                    'backgroundColor' => '#ffc107',
                    'data' => $waiting,
                ],
                [
                    'label' => 'Approve',
                    'backgroundColor' => '#28a745',
                    'data' => $approve,
                ],
                [
                    'label' => 'Reject',
                    'backgroundColor' => '#dc3545',
                    'data' => $reject,
                ]
            ];

            $data2['datasets'] = [
                [
                    'label' => 'Tumpangan',
                    'backgroundColor' => '#007bff',
                    'borderColor' => '#007bff',
                    'fill' => false,
                    'data' => $arrayTumpangan,
                ],
                [
                    'label' => 'Perpanjangan',
                    'backgroundColor' => '#20c997',
                    'borderColor' => '#20c997',
                    'fill' => false,
                    'data' => $arrayPerpanjangan,
                ],
            ];

            return response()->json([
                'status' => 'SUCCESS',
                'data' => [
                    'barChart' => $data1,
                    'lineChart' => $data2,
                ],
            ]);
        }

        $pemakaman = Pemakaman::all();
        if(Auth::user()) {
            $role = Auth::user()->role;
            if ($role == "member") {

                return view('welcome')->with(["pemakaman"=>$pemakaman]);
            } else {
                return view('home');
            }
        }
        return view('welcome')->with(["pemakaman"=>$pemakaman]);
    }

    //Tidak dipakai
    public function register(Request $request){

        $this->validate($request, [
            'fullname' => 'required|min:4',
            'gender' => 'required',
            'email' => 'required|min:4|email',
            'password' => 'required',
            'confirm' => 'required|same:password',
            'address' => 'required|min:4',
            'confirm-checkbox' => 'required'
        ]);

        $user = new Users();
        $user->fullname = $request["fullname"];
        $user->gender = $request["jenis kelamin"];
        $user->email = $request["email"];
        $user->password = bcrypt($request["password"]);
        $user->address = $request['address'];
        $user->role = 'member';
        $user->save();
        return redirect('user/register')->with('alert-success','anda berhasil daftar');
    }

    // Tidak Dipakai
    public function TestFunction(){
        return Hash::check("", Auth::user()->password, []);
    }

    public function list()
    {
        $data = User::all();

        return view('user.index', get_defined_vars());
    }

    public function create(Request $request)
    {
        if($request->method() == 'PUT'){
            //$request['pemakaman_id'] = $request["pemakaman_id"];
            $this->validate($request,[
                'fullname' => 'required',
                'email' => 'required',
                'password' => 'required',
                'address' => 'required',
                'role' => 'required',
                'gender' => 'required',
            ], [
                'fullname.required' => 'field nama tidak boleh kosong',
                'email.required' => 'field email tidak boleh kosong',
                'password.required' => 'field password tidak boleh kosong',
                'address.required' => 'field alamat tidak boleh kosong',
                'role.required' => 'field role tidak boleh kosong',
                'gender.required' => 'field gender tidak boleh kosong',
            ]);
            $request['password'] = bcrypt($request["password"]);
            User::create($request->all());

            $data = User::all();
            Alert::success('Success!', 'Data user berhasil ditambah');
            return redirect('user/list');
        }

        return view('user.create');
    }

    public function edit(Request $request, $id)
    {
        $User = User::find($id);

        if($request->method() == 'PATCH'){
            $this->validate($request,[
                'fullname' => 'required',
                'email' => 'required',
                'password' => 'required',
                'address' => 'required',
                'role' => 'required',
                'gender' => 'required',
            ], [
                'fullname.required' => 'field nama tidak boleh kosong',
                'email.required' => 'field email tidak boleh kosong',
                'password.required' => 'field password tidak boleh kosong',
                'address.required' => 'field alamat tidak boleh kosong',
                'role.required' => 'field role tidak boleh kosong',
                'gender.required' => 'field gender tidak boleh kosong',
            ]);
            $request['password'] = bcrypt($request["password"]);
            //$request['pemakaman_id'] = $request["pemakaman_id"];
            $User->update($request->all());

            $data = User::all();
            Alert::success('Success!', 'Data user berhasil diperbaharui');
            return redirect('user/list');
        }

        return view('user.edit', get_defined_vars());
    }

    public function destroy($id)
    {
        $User = User::find($id);
        $User->delete();
        Alert::success('Success!', 'Data user berhasil dihapus');

        return redirect('user/list');
    }

    public function profile(Request $request)
    {

        if($request->method() == 'POST'){
            $this->validate($request,[
                'fullname' => 'required',
            ], [
                'fullname.required' => 'field nama tidak boleh kosong',
            ]);
            $user = User::find(Auth::user()->id);
            if($file = $request->images){
                $fileName = $file->getClientOriginalName();
                $file->move(public_path('/images/profile'), $fileName);
                $user->images = $fileName;
            }

            $user->update($request->all());
            $user->save();
            Alert::success('Success!', 'Data profil berhasil diperbaharui');

            return redirect('profile')->with('success', 'Profile berhasil diperbaharui');
        }

        return view('auth.edit-profile');
    }
}
