<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\IptmImport;
use App\Imports\MakamImport;
use App\Imports\AlmarhumImport;
use App\Imports\AhliWarisImport;
use App\IPTM;
use App\Makam;
use App\Almarhum;
use App\AhliWaris;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use Session;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class ImportController extends Controller
{

    public function ImportIptm(Request $request)
    {
      // code...
      if(Auth::user()->role == "admin_tpu"){
          $iptm = IPTM::all();

          if($request->method() == 'PUT'){
            $this->validate($request, [
        			'file' => 'required|mimes:csv,xls,xlsx'
        		]);

            $file = $request->file('file');

        		$nama_file = rand().$file->getClientOriginalName();

        		$file->move(public_path('document/excel/'),$nama_file);

        		Excel::import(new IptmImport, public_path('document/excel/'.$nama_file));

        		Session::flash('sukses','Data IPTM Berhasil Dimasukkan');

        		return redirect()->back();
        }
        return view('Import.iptm')->with(["iptm"=>$iptm]);
      }
      return view('home');
    }

    public function ImportMakam(Request $request)
    {
      // code...
      if(Auth::user()->role == "admin_tpu"){
        $makam = Makam::all();

      if($request->method() == 'PUT'){
        $this->validate($request, [
    			'file' => 'required|mimes:csv,xls,xlsx'
    		]);

        $file = $request->file('file');

    		$nama_file = rand().$file->getClientOriginalName();

    		$file->move(public_path('document/excel/'),$nama_file);

    		Excel::import(new MakamImport, public_path('document/excel/'.$nama_file));

    		Session::flash('sukses','Data Makam Berhasil Dimasukkan');

        return redirect()->back();
      }
      return view('Import.makam')->with(["makam"=>$makam]);
    }
    return view('home');
  }

  public function ImportAlmarhum(Request $request)
  {
    // code...
    if(Auth::user()->role == "admin_tpu"){
      $almarhum = Almarhum::select([
                  '*',
                  'ahli_waris.nama_ahliwaris',
                  'iptm.nomor_iptm'])
                  ->join('ahli_waris','ahli_waris.id','=','almarhum.ahli_waris_id')
                  ->join('iptm','iptm.id','=','almarhum.iptm_id')
                  ->get();

    if($request->method() == 'PUT'){
      $this->validate($request, [
        'file' => 'required|mimes:csv,xls,xlsx'
      ]);

      $file = $request->file('file');

      $nama_file = rand().$file->getClientOriginalName();

      $file->move(public_path('document/excel/'),$nama_file);

      Excel::import(new AlmarhumImport, public_path('document/excel/'.$nama_file));

      Session::flash('sukses','Data Almarhum Berhasil Dimasukkan');

      return redirect()->back();
      }
      return view('Import.almarhum')->with(["almarhum"=>$almarhum]);
    }
    return view('home');
  }

  public function ImportAhliwaris(Request $request)
  {
    // code...
    if(Auth::user()->role == "admin_tpu"){
      $ahliwaris = AhliWaris::all();

    if($request->method() == 'PUT'){
      $this->validate($request, [
        'file' => 'required|mimes:csv,xls,xlsx'
      ]);

      $file = $request->file('file');

      $nama_file = rand().$file->getClientOriginalName();

      $file->move(public_path('document/excel/'),$nama_file);

      Excel::import(new AhliWarisImport, public_path('document/excel/'.$nama_file));

      Session::flash('sukses','Data Ahliwaris Berhasil Dimasukkan');

      return redirect()->back();
      }
      return view('Import.ahliWaris')->with(["ahliwaris"=>$ahliwaris]);
    }
    return view('home');
  }

  public function addMakam(Request $request, $id)
  {
    // code...
    if($request->method() == 'PUT'){
        Makam::create($request->all());

        /*Session::flash('sukses','Data Makam Berhasil Diperbaharui');*/
        Alert::success('Success','Data Makam Berhasil Dimasukkan');

        return redirect()->back();
      }
  }

  public function addAhliWaris(Request $request, $id)
  {
    // code...
    if($request->method() == 'PUT'){
        AhliWaris::create($request->all());

        /*Session::flash('sukses','Data Makam Berhasil Diperbaharui');*/
        Alert::success('Success','Data AhliWaris Berhasil Dimasukkan');

        return redirect()->back();
      }
  }

  public function addIPTM(Request $request, $id)
  {
    // code...
    if($request->method() == 'PUT'){
        IPTM::create($request->all());

        /*Session::flash('sukses','Data Makam Berhasil Diperbaharui');*/
        Alert::success('Success','Data IPTM Berhasil Dimasukkan');

        return redirect()->back();
      }
  }

  public function addAlmarhum(Request $request, $id)
  {
    // code...
    if($request->method() == 'PUT'){
        Almarhum::create($request->all());

        /*Session::flash('sukses','Data Makam Berhasil Diperbaharui');*/
        Alert::success('Success','Data Almarhum Berhasil Dimasukkan');

        return redirect()->back();
      }
  }

  public function editMakam(Request $request, $id)
  {
    // code...
    if($request->method() == 'PATCH'){
        $makam = Makam::find($id);
        $makam->update($request->all());

        /*Session::flash('sukses','Data Makam Berhasil Diperbaharui');*/
        Alert::success('Success','Data Makam Berhasil Diperbaharui');

        return redirect()->back();
      }
  }

  public function editAhliwaris(Request $request, $id)
  {
    // code...
    if($request->method() == 'PATCH'){
        $ahliwaris = AhliWaris::find($id);
        $ahliwaris->update($request->all());

        /*Session::flash('sukses','Data AhliWaris Berhasil Diperbaharui');*/
        Alert::success('Success','Data Ahliwaris Berhasil Diperbaharui');

        return redirect()->back();
      }
  }

  public function editIPTM(Request $request, $id)
  {
    // code...
    if($request->method() == 'PATCH'){
        $iptm = IPTM::find($id);
        $iptm->update($request->all());

       /* Session::flash('sukses','Data IPTM Berhasil Diperbaharui');*/
        Alert::success('Success','Data IPTM Berhasil Diperbaharui');

        return redirect()->back();
      }
  }

  public function editAlmarhum(Request $request, $id)
  {
    // code...
    if($request->method() == 'PATCH'){
        $almarhum = Almarhum::find($id);
        $almarhum->update($request->all());

        /*Session::flash('sukses','Data Almarhum Berhasil Diperbaharui');*/
        Alert::success('Success','Data Almarhum Berhasil Diperbaharui');

        return redirect()->back();
      }
  }

  public function deleteMakam($id)
  {
    // code...
        $makam = Makam::find($id);
        $makam->delete();

        /*Session::flash('sukses','Data Makam Berhasil Dihapus');*/
      Alert::success('Success','Data Makam Berhasil Dihapus');

        return redirect()->back();
  }

  public function deleteAhliwaris($id)
  {
    // code...
        $ahliwaris = AhliWaris::find($id);
        $ahliwaris->delete();

        /*Session::flash('sukses','Data AhliWaris Berhasil Dihapus');*/
      Alert::success('Success','Data Ahliwaris Berhasil Dihapus');

        return redirect()->back();
  }

  public function deleteIPTM($id)
  {
    // code...
        $iptm = IPTM::find($id);
        $iptm->delete();

        /*Session::flash('sukses','Data IPTM Berhasil Dihapus');*/
      Alert::success('Success','Data IPTM Berhasil Dihapus');

        return redirect()->back();
  }

  public function deleteAlmarhum($id)
  {
    // code...
        $almarhum = Almarhum::find($id);
        $almarhum->delete();

        /*Session::flash('sukses','Data Almarhum Berhasil Dihapus');*/
      Alert::success('Success','Data Almarhum Berhasil Dihapus');

        return redirect()->back();
  }

}
