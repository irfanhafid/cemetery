<?php

namespace App\Http\Controllers;

use App\JadwalPemakaman;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class JadwalPemakamanController extends Controller
{
    public function index()
    {
        $data = JadwalPemakaman::all();

        return view('jadwal-pemakaman.index', get_defined_vars());
    }

    public function create(Request $request)
    {
        if($request->method() == 'PUT'){
            $this->validate($request,[
                'iptm_id' => 'required',
                'tanggal_pemakaman' => 'required',
                'jam_pemakaman' => 'required',
            ], [
                'iptm_id.required' => 'field nomor iptm tidak boleh kosong',
                'tanggal_pemakaman.required' => 'field tanggal pemakaman tidak boleh kosong',
                'jam_pemakaman.required' => 'field jam pemakaman tidak boleh kosong',
            ]);
            JadwalPemakaman::create($request->all());

            $data = JadwalPemakaman::all();
            Alert::success('Success!','Jadwal Pemakaman berhasil ditambah');

            return redirect('jadwal-pemakaman');
        }

        return view('jadwal-pemakaman.create');
    }

    public function edit(Request $request, $id)
    {
        $JadwalPemakaman = JadwalPemakaman::find($id);

        if($request->method() == 'PATCH'){
            $this->validate($request,[
                'iptm_id' => 'required',
                'tanggal_pemakaman' => 'required',
                'jam_pemakaman' => 'required',
            ], [
                'iptm_id.required' => 'field nomor iptm tidak boleh kosong',
                'tanggal_pemakaman.required' => 'field tanggal pemakaman tidak boleh kosong',
                'jam_pemakaman.required' => 'field jam pemakaman tidak boleh kosong',
            ]);
            $JadwalPemakaman->update($request->all());

            $data = JadwalPemakaman::all();
            Alert::success('Success!','Jadwal Pemakaman berhasil diperbaharui');

            return redirect('jadwal-pemakaman');
        }

        return view('jadwal-pemakaman.edit', get_defined_vars());
    }

    public function delete($id){
        JadwalPemakaman::find($id)->delete();
        Alert::success('Success!','Jadwal Pemakaman berhasil dihapus');

        return redirect('jadwal-pemakaman');
    }
}
