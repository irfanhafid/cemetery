<?php

namespace App\Http\Controllers;

use App\Almarhum;
use App\Tumpangan;
use App\Perpanjangan;
use App\NomorPemesanan;
use App\Pemakaman;
use App\AhliWaris;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Session;
use PDF;
Use RealRashid\SweetAlert\Facades\Alert;

class PemesananController extends Controller
{
    public function showStatusPemesanan($tipe_pesanan){

        $output = null;
        $user = \Auth::user();
        $pemakamanId = Auth::user()->pemakaman_id;

        if($tipe_pesanan == "perpanjangan"){
            $output = Perpanjangan::select([
                '*',
                'perpanjangan.id AS id',
            ])
                ->join('iptm', 'iptm.id', '=', 'perpanjangan.iptm_id')
                ->join('makam', 'makam.id', '=', 'iptm.makam_id')
                ->join('almarhum', 'almarhum.id', '=', 'perpanjangan.almarhum_id')
                ->join('ahli_waris', 'ahli_waris.id', '=', 'perpanjangan.ahli_waris_id')
                ->join('users','users.id','=','perpanjangan.user_id')
                ->where('makam.pemakaman_id','=',$pemakamanId)
                ->where('users.role','=','member')
                //->where('perpanjangan.status', '=', 'waiting')
                // ->where(function ($q) use ($user) {
                //     if($user->role == 'member'){
                //         $q->where('user_id', $user->id);
                //     }
                // })
                ->get();

        }else if($tipe_pesanan == "tumpangan"){
            $output = Tumpangan::select([
                    '*',
                    'tumpangan.id AS id',
                ])
                ->join('iptm', 'iptm.id', '=', 'tumpangan.iptm_id')
                ->join('makam', 'makam.id', '=', 'iptm.makam_id')
                ->join('almarhum', 'almarhum.iptm_id', '=', 'iptm.id')
                ->join('ahli_waris', 'ahli_waris.id', '=', 'tumpangan.ahli_waris_id')
                ->join('users','users.id','=','tumpangan.user_id')
                ->where('makam.pemakaman_id','=',$pemakamanId)
                ->where('users.role','=','member')
//                ->where('tumpangan.status', '=', 'waiting')
                // ->where(function ($q) use ($user) {
                //     if($user->role == 'member'){
                //         $q->where('user_id', $user->id);
                //     }
                // })
                ->get();
        }

        return view('Pemesanan.status-pemesanan')->with([
            "data" => $output,
            "tipe" => $tipe_pesanan
        ]);
    }
    /*public function showStatusPemesananByUser($id){

    }*/

    public function showDetailPesanan($tipe_pesanan, $id)
    {
        if($tipe_pesanan == "perpanjangan"){
            $data = Perpanjangan::find($id);
        }
        else if($tipe_pesanan == "tumpangan"){
            $data = Tumpangan::find($id);
        }

        return view('Pemesanan.detail-pemesanan', get_defined_vars());
    }

    public function approvalDetailPesanan(Request $request, $tipe_pesanan, $id)
    {
        if($tipe_pesanan == "perpanjangan"){
            $Perpanjangan = Perpanjangan::find($id);
            $Perpanjangan->nomor_surat = $request->nomor_surat;
            $Perpanjangan->tanggal_surat = $request->tanggal_surat;
            $Perpanjangan->daftar_jenazah = $request["daftar_jenazah"];
            $Perpanjangan->tanggal_berlaku_dari = $request["tanggal_berlaku_dari"];
            $Perpanjangan->tanggal_berlaku_sampai = $request["tanggal_berlaku_sampai"];
            /*$Perpanjangan->user_id = \Auth::user()->id;*/
            $Perpanjangan->status = $request->status ?: 2;

            $fileName = str_replace('/', '-', $Perpanjangan->nomor_surat) . '_' . time() . '.pdf';
            $pdf = PDF::loadView('Pemesanan.print-pemesanan', get_defined_vars());

            $Perpanjangan->file_iptm = $fileName;
            $Perpanjangan->save();
            /*return $pdf->stream("$fileName");*/
        }
        else if($tipe_pesanan == "tumpangan"){
            $Tumpangan = Tumpangan::find($id);
            $Tumpangan->nomor_surat = $request->nomor_surat;
            $Tumpangan->tanggal_surat = $request->tanggal_surat;
            $Tumpangan->daftar_jenazah = $request["daftar_jenazah"];
            $Tumpangan->tanggal_berlaku_dari = $request["tanggal_berlaku_dari"];
            $Tumpangan->tanggal_berlaku_sampai = $request["tanggal_berlaku_sampai"];
            /*$Tumpangan->user_id = \Auth::user()->id;*/
            $Tumpangan->status = $request->status ?: 2;

            $fileName = str_replace('/', '-', $Tumpangan->nomor_surat) . '_' . time() . '.pdf';
            $pdf = PDF::loadView('Pemesanan.print-pemesanan', get_defined_vars());
            /*if(!File::exists(storage_path("document/tumpangan"))) {
                File::makeDirectory(storage_path("document/tumpangan"), 0775, true);
            }
            $pdf->save(storage_path("document/tumpangan/$fileName"));*/

            $Tumpangan->file_iptm = $fileName;
            $Tumpangan->save();
            /*return $pdf->stream("$fileName");*/
        }
        Alert::success('Success!','Data berhasil disimpan');

        return redirect()->back()->with('success', 'Your record has been updated');
    }

    public function rejectDetailPesanan(Request $request, $tipe_pesanan, $id)
    {
      if($tipe_pesanan == "perpanjangan"){
          $Perpanjangan = Perpanjangan::find($id);
          $Perpanjangan->catatan = $request["catatan"];
          $Perpanjangan->status = $request->status ?: 3;

          $Perpanjangan->save();
          return redirect()->back();
      }
      else if($tipe_pesanan == "tumpangan"){
          $Tumpangan = Tumpangan::find($id);
          $Tumpangan->catatan = $request["catatan"];
          $Tumpangan->status = $request->status ?: 3;

          $Tumpangan->save();
          return redirect()->back();
    }
    return url()->previous()->with('rejected', 'Your record has been updated');
  }

  public function editDetailPesanan(Request $request, $tipe_pesanan, $id)
  {
    if($tipe_pesanan == "perpanjangan"){
        $data = Perpanjangan::find($id);

        if($request->method() == 'PATCH'){
          //$Perpanjangan->update($request->all());
          $ahliwaris = AhliWaris::where('id',$data->ahli_waris_id)
                  ->update([
                    'nomor_ktp_ahliwaris'=>$request['nomor_ktp_ahliwaris'],
                    'nama_ahliwaris'=>$request['nama_ahliwaris'],
                    'telepon_ahliwaris'=>$request['telepon_ahliwaris'],
                    'hubungan_ahliWaris'=>$request['hubungan_ahliwaris'],
                    'alamat_ahliwaris'=>$request['alamat_ahliwaris'],
                    'rt_ahliwaris'=>$request['rt_ahliwaris'],
                    'rw_ahliwaris'=>$request['rw_ahliwaris'],
                    'kelurahan_ahliwaris'=>$request['kelurahan_ahliwaris'],
                    'kecamatan_ahliwaris'=>$request['kecamatan_ahliwaris'],
                    'kota_ahliwaris'=>$request['kota_administrasi'],
                  ]);

          $data->status = '1';
          $data->save();

          Session::flash('Sukses','Data anda berhasil diubah menjadi waiting');
          return redirect()->back();
        }

        return view('pemesanan.edit-pemesanan', get_defined_vars());
    }
    else if($tipe_pesanan == "tumpangan"){
        $data = Tumpangan::find($id);

        if($request->method() == 'PATCH'){
          //$Tumpangan->update($request->all());
          $almarhum = Almarhum::find($data->almarhum_id);
          $almarhum->nama_almarhum = $request['nama_almarhum'];
          $almarhum->tanggal_wafat = $request['tanggal_wafat'];
          $almarhum->nomor_ktp_almarhum = $request['nomor_ktp_almarhum'];
          $almarhum->nomor_kk_almarhum = $request['nomor_kk_almarhum'];
          $almarhum->nomor_sp_rtrw = $request['nomor_sp_rtrw'];
          $almarhum->tanggal_sp_rtrw = $request['tanggal_sp_rtrw'];
          $almarhum->nomor_sk_kematian_rs = $request['nomor_sk_kematian_rs'];
          $almarhum->tanggal_sk_kematian_rs = $request['tanggal_sk_kematian_rs'];

          $ahliwaris = AhliWaris::where('id',$data->ahli_waris_id)
                  ->update([
                    'nomor_ktp_ahliwaris'=>$request['nomor_ktp_ahliwaris'],
                    'nama_ahliwaris'=>$request['nama_ahliwaris'],
                    'telepon_ahliwaris'=>$request['telepon_ahliwaris'],
                    'hubungan_ahliWaris'=>$request['hubungan_ahliwaris'],
                    'alamat_ahliwaris'=>$request['alamat_ahliwaris'],
                    'rt_ahliwaris'=>$request['rt_ahliwaris'],
                    'rw_ahliwaris'=>$request['rw_ahliwaris'],
                    'kelurahan_ahliwaris'=>$request['kelurahan_ahliwaris'],
                    'kecamatan_ahliwaris'=>$request['kecamatan_ahliwaris'],
                    'kota_ahliwaris'=>$request['kota_administrasi'],
                  ]);

          $data->nomor_sk_kehilangan_kepolisian = $request['nomor_sk_kehilangan_kepolisian'];
          $data->tanggal_sk_kehilangan_kepolisian = $request['tanggal_sk_kehilangan_kepolisian'];

                  if($request->hasFile('file_ktp_almarhum')){
                      $file = $request->input('file_ktp_almarhum');
                      $fileName = $request->file('file_ktp_almarhum')->getClientOriginalName();
                      $request->file('file_ktp_almarhum')->move(public_path('Document/Tumpangan/'), $fileName);
                      $almarhum->file_ktp_almarhum = $fileName;
                      $almarhum->save();
                  }

                  if($request->hasFile('file_kk_almarhum')){
                      $file = $request->input('file_kk_almarhum');
                      $fileName = $request->file('file_kk_almarhum')->getClientOriginalName();
                      $request->file('file_kk_almarhum')->move(public_path('Document/Tumpangan/'), $fileName);
                      $almarhum->file_kk_almarhum = $fileName;
                      $almarhum->save();
                  }

                  if($request->hasFile('file_iptm_asli')){
                      $file = $request->input('file_iptm_asli');
                      $fileName = $request->file('file_iptm_asli')->getClientOriginalName();
                      $request->file('file_iptm_asli')->move(public_path('Document/Tumpangan/'), $fileName);
                      $data->file_iptm_asli = $fileName;
                      $data->save();
                  }

                  if($request->hasFile('file_sk_kehilangan_kepolisian')){
                      $file = $request->input('file_sk_kehilangan_kepolisian');
                      $fileName = $request->file('file_sk_kehilangan_kepolisian')->getClientOriginalName();
                      $request->file('file_sk_kehilangan_kepolisian')->move(public_path('Document/Tumpangan/'), $fileName);
                      $data->file_sk_kehilangan_kepolisian = $fileName;
                      $data->save();
                  }

                  if($request->hasFile('file_sp_rtrw')){
                      $file = $request->input('file_sp_rtrw');
                      $fileName = $request->file('file_sp_rtrw')->getClientOriginalName();
                      $request->file('file_sp_rtrw')->move(public_path('Document/Tumpangan/'), $fileName);
                      $almarhum->file_sp_rtrw = $fileName;
                      $almarhum->save();
                  }

                  if($request->hasFile('file_sk_kematian_rs')){
                      $file = $request->input('file_sk_kematian_rs');
                      $fileName = $request->file('file_sk_kematian_rs')->getClientOriginalName();
                      $request->file('file_sk_kematian_rs')->move(public_path('Document/Tumpangan/'), $fileName);
                      $almarhum->file_sk_kematian_rs = $fileName;
                      $almarhum->save();
                  }


          $data->status = '1';
          $almarhum->save();
          $data->save();

          Session::flash('Sukses','Data anda telah berhasil diubah menjadi waiting');
          return redirect()->back();
        }

        return view('pemesanan.edit-pemesanan', get_defined_vars());
    }
}

    public function printDetailPesanan($tipe_pesanan, $id)
    {
        if($tipe_pesanan == "perpanjangan"){
            $data = Perpanjangan::find($id);
            $pdf = PDF::loadView('Pemesanan.print-pemesanan', get_defined_vars());
            return $pdf->stream('perpanjangan.pdf');
        }
        else if($tipe_pesanan == "tumpangan"){
            $data = Tumpangan::find($id);
            $pdf = PDF::loadView('Pemesanan.print-pemesanan', get_defined_vars());
            return $pdf->stream('tumpangan.pdf');
        }
        /*else if($tipe_pesanan == "pemindahan"){

        }*/

        return view('Pemesanan.print-pemesanan', get_defined_vars());
    }

    public function showFile(Request $request, $tipe_pesanan, $id, $file)
    {
        if($tipe_pesanan == "perpanjangan"){

        }
        else if($tipe_pesanan == "tumpangan"){
            $tumpangan = Tumpangan::find($id);
            $almarhum_baru = $tumpangan->almarhum;
            $data = array_merge($tumpangan->toArray(), $almarhum_baru->toArray());
            $path = public_path('Document/Tumpangan');
        }
        else if($tipe_pesanan == "pemindahan"){

        }

        return redirect()->to($path . DIRECTORY_SEPARATOR . $data[$file]);
    }

    public function pesananDetail(Request $request)
    {
        $data = collect();
        $user = Auth::user();

        $tumpangan = Tumpangan::select([
            'tumpangan.id AS id',
            'tumpangan.nomor_permohonan',
            'tumpangan.tanggal_permohonan',
            \DB::raw('"tumpangan" AS tipe_pesanan'),
            'pemakaman.nama_pemakaman',
            'tumpangan.status',
            'tumpangan.created_at',
        ])
            ->join('iptm', 'iptm.id', '=', 'tumpangan.iptm_id')
            ->join('makam', 'makam.id', '=', 'iptm.makam_id')
            ->join('pemakaman', 'pemakaman.id', '=', 'makam.pemakaman_id')
            ->where('tumpangan.user_id', '=', $user->id)
            // ->where(function ($q) use ($user) {
            //     if($user->role == 'member'){
            //         $q->where('user_id', $user->id);
            //     }
            ->get();

        $perpanjangan = Perpanjangan::select([
            'perpanjangan.id AS id',
            'perpanjangan.nomor_permohonan',
            'perpanjangan.tanggal_permohonan',
            \DB::raw('"perpanjangan" AS tipe_pesanan'),
            'pemakaman.nama_pemakaman',
            'perpanjangan.status',
            'perpanjangan.created_at',
        ])
            ->join('iptm', 'iptm.id', '=', 'perpanjangan.iptm_id')
            ->join('makam', 'makam.id', '=', 'iptm.makam_id')
            ->join('pemakaman', 'pemakaman.id', '=', 'makam.pemakaman_id')
            ->where('perpanjangan.user_id', '=', $user->id)
            // ->where(function ($q) use ($user) {
            //     if($user->role == 'member'){
            //         $q->where('user_id', $user->id);
            //     }
            ->get();

            foreach ($tumpangan as $tumpangans) {
               $data->push($tumpangans);
            }

            foreach ($perpanjangan as $perpanjangans) {
               $data->push($perpanjangans);
            }


        return view('Pemesanan.member.detail', get_defined_vars());
    }

    public function riwayat()
    {
        $data = collect();
        $pemakamanId = Auth::user()->pemakaman_id;

        $tumpangan = Tumpangan::select([
            'tumpangan.id AS id',
            'tumpangan.nomor_permohonan',
            'tumpangan.tanggal_permohonan',
            \DB::raw('"tumpangan" AS tipe_pesanan'),
            'pemakaman.nama_pemakaman',
            'tumpangan.status',
            'tumpangan.created_at',
        ])
            ->join('iptm', 'iptm.id', '=', 'tumpangan.iptm_id')
            ->join('makam', 'makam.id', '=', 'iptm.makam_id')
            ->join('pemakaman', 'pemakaman.id', '=', 'makam.pemakaman_id')
            ->join('almarhum', 'almarhum.iptm_id', '=', 'iptm.id')
            ->join('users','users.id','=','tumpangan.user_id')
            ->where('users.role','=','member')
            ->where('pemakaman.id','=',$pemakamanId)
            ->get();
//                ->where('tumpangan.status', '=', 'waiting')
            /*->where(function ($q) use ($user) {
                if($user->role == 'member'){
                    $q->where('user_id', $user->id);
                }
            })->get();*/

        $perpanjangan = Perpanjangan::select([
                'perpanjangan.id AS id',
                'perpanjangan.nomor_permohonan',
                'perpanjangan.tanggal_permohonan',
                \DB::raw('"perpanjangan" AS tipe_pesanan'),
                'pemakaman.nama_pemakaman',
                'perpanjangan.status',
                'perpanjangan.created_at',
            ])
                ->join('iptm', 'iptm.id', '=', 'perpanjangan.iptm_id')
                ->join('makam', 'makam.id', '=', 'iptm.makam_id')
                ->join('pemakaman', 'pemakaman.id', '=', 'makam.pemakaman_id')
                ->join('almarhum', 'almarhum.id', '=', 'perpanjangan.almarhum_id')
                ->join('users','users.id','=','perpanjangan.user_id')
                ->where('users.role','=','member')
                ->where('pemakaman.id','=',$pemakamanId)
                ->get();
        /*$data = $tumpangan->merge($perpanjangan)->sortByDesc('created_at');*/
        $data = collect()->sortByDesc('created_at');

        foreach ($tumpangan as $tumpangans) {
            $data->push($tumpangans);
        }

        foreach ($perpanjangan as $perpanjangans) {
            $data->push($perpanjangans);
        }

        return view('Pemesanan.history-pemesanan', get_defined_vars());
    }

    public function downloadFile($tipe_pesanan, $id, $attr)
    {
        if($tipe_pesanan == 'tumpangan'){
            $Tumpangan = Tumpangan::find($id);
            $almarhum_baru = $Tumpangan->almarhum;
            $ahli_waris = $Tumpangan->ahliWaris;

            $fileName = $Tumpangan[$attr] ?: ( $almarhum_baru[$attr] ?: ($ahli_waris[$attr] ?: null) );
            $path = public_path("Document/Tumpangan/$fileName");
        }
        return \Response::download($path);
    }
}
