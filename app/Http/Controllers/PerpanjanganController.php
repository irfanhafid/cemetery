<?php

namespace App\Http\Controllers;

use App\AhliWaris;
use App\IPTM;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use App\Perpanjangan;
use App\NomorPemesanan;
use App\Almarhum;
use App\Makam;
use PDF;
use RealRashid\SweetAlert\Facades\Alert;

class PerpanjanganController extends Controller
{

    //PERPANJANGAN
    public function SubmitPerpanjangan(Request $request)
    {
        $iptm_id = $request['iptm_id'];
        $almarhum_id = $request['almarhum_id'];
        $user = auth()->user();

        if($user->role == "admin_tpu")
        {
            $iptm_perpanjangan = IPTM::find($iptm_id);
            $iptm_perpanjangan->tanggal_iptm = $iptm_perpanjangan->tanggal_iptm;
            $iptm_perpanjangan->save();

            $ahliwaris = new AhliWaris();
            $ahliwaris->nama_ahliwaris = $request["nama_ahliwaris"];
            $ahliwaris->nomor_ktp_ahliwaris = $request["nomor_ktp_ahliwaris"];
            $ahliwaris->alamat_ahliwaris = $request["alamat_ahliwaris"];
            $ahliwaris->rt_ahliwaris = $request["rt_ahliwaris"];
            $ahliwaris->rw_ahliwaris = $request["rw_ahliwaris"];
            $ahliwaris->kelurahan_ahliwaris = $request["kelurahan_ahliwaris"];
            $ahliwaris->kecamatan_ahliwaris = $request["kecamatan_ahliwaris"];
            $ahliwaris->kota_ahliwaris = $request["kota_ahliwaris"];
            $ahliwaris->telepon_ahliwaris = $request["telepon_ahliwaris"];
            $ahliwaris->hubungan_ahliwaris = $request["hubungan_ahliwaris"];
            $ahliwaris->save();

            $perpanjangan = new Perpanjangan();
            $perpanjangan->iptm_id = $iptm_id;
            $perpanjangan->almarhum_id = $almarhum_id;
            $perpanjangan->user_id = Auth::user()->id;
            $perpanjangan->ahli_waris_id = $ahliwaris->id;
            $perpanjangan->nomor_sk_kehilangan_kepolisian = $request["nomor_sk_kehilangan_kepolisian"];
            $perpanjangan->tanggal_sk_kehilangan_kepolisian = $request["tanggal_sk_kehilangan_kepolisian"];
            $perpanjangan->file_sk_kehilangan_kepolisian = $request["file_sk_kehilangan_kepolisian"];
            $perpanjangan->perpanjangan_ke = $request["perpanjangan_ke"];
            $perpanjangan->tanggal_permohonan = now();
            $perpanjangan->nomor_surat = $request["nomor_surat"];
            $perpanjangan->tanggal_surat = $request["tanggal_surat"];
            $perpanjangan->daftar_jenazah = $request["daftar_jenazah"];
            $perpanjangan->tanggal_berlaku_dari = $request["tanggal_berlaku_dari"];
            $perpanjangan->tanggal_berlaku_sampai = $request["tanggal_berlaku_sampai"];
            $perpanjangan->status = "2";
            $perpanjangan->cetak_oleh = $user->fullname;
            $perpanjangan->save();

            $getdata = Perpanjangan::find($perpanjangan->id);
            $almarhum = Almarhum::find($iptm_id);
            $ahliwaris = AhliWaris::find($perpanjangan->ahli_waris_id);
            $iptm = IPTM::find($iptm_id);
            $makam = Makam::find($iptm_perpanjangan->makam_id);

            $fileName = str_replace('/', '-', $perpanjangan->nomor_surat) . '_' . time() . '.pdf';
            $perpanjangan->file_iptm = $fileName;
            $perpanjangan->save();
            $pdf = PDF::loadView('Perpanjangan.Print-perpanjangan', ['getdata'=>$getdata, 'almarhum'=>$almarhum, 'ahliwaris'=>$ahliwaris, 'iptm'=>$iptm, 'makam'=>$makam]);
            return $pdf->stream("$fileName");

            return redirect('IPTM/perpanjangan');
        }
        else
        {
            $ahliwaris = new AhliWaris();

            //Foto KTP Ahliwaris
            if ($request->hasFile('file_ktp_ahliwaris')) {
                $file = $request->file('file_ktp_ahliwaris');
                $file->move(public_path('/Document/Tumpangan'), $file->getClientOriginalName());

                $ahliwaris->file_ktp_ahliwaris = $file->getClientOriginalName();
            }

            $ahliwaris->nomor_ktp_ahliwaris = $request->nomor_ktp_ahliwaris;
            $ahliwaris->nama_ahliwaris = $request->nama_ahliwaris;
            $ahliwaris->alamat_ahliwaris = $request->alamat_ahliwaris;
            $ahliwaris->rt_ahliwaris = $request->rt_ahliwaris;
            $ahliwaris->rw_ahliwaris = $request->rw_ahliwaris;
            $ahliwaris->kelurahan_ahliwaris = $request->kelurahan_ahliwaris;
            $ahliwaris->kecamatan_ahliwaris = $request->kecamatan_ahliwaris;
            $ahliwaris->kota_ahliwaris = $request->kota_administrasi;
            $ahliwaris->telepon_ahliwaris = $request->telepon_ahliwaris;
            $ahliwaris->hubungan_ahliwaris = $request->hubungan_ahliwaris;
            $ahliwaris->save();

            $perpanjangan = new Perpanjangan();
            $perpanjangan->iptm_id = $iptm_id;
            $perpanjangan->almarhum_id = $almarhum_id;
            $perpanjangan->ahli_waris_id = $ahliwaris->id;
            $perpanjangan->user_id = Auth::user()->id;
            if ($request->hasFile('file_iptm_asli')) {
                $file = $request->file('file_iptm_asli');
                $file->move(public_path('/Document/Perpanjangan'), $file->getClientOriginalName());
                $perpanjangan->file_iptm_asli = $file->getClientOriginalName();
            }
            $perpanjangan->nomor_sk_kehilangan_kepolisian = $request["nomor_sk_kehilangan_kepolisian"];
            $perpanjangan->tanggal_sk_kehilangan_kepolisian = $request["tanggal_sk_kehilangan_kepolisian"];
            $perpanjangan->file_sk_kehilangan_kepolisian = $request["file_sk_kehilangan_kepolisian"];
            $perpanjangan->tanggal_permohonan = now();
            $perpanjangan->perpanjangan_ke = $request["perpanjangan_ke"];
            $perpanjangan->status = "1";
            $perpanjangan->save();
            $perpanjanganId = $perpanjangan->id;
            $nomorPermohonan = "PI-".str_pad( $perpanjanganId, 3, 0, STR_PAD_LEFT);
            $perpanjangan->nomor_permohonan = $nomorPermohonan;
            $perpanjangan->save();
            toast('Data Permohonan Telah Tersimpan','success');

            return redirect('/IPTM/perpanjangan/sukses/' . $perpanjangan->id)->with('register_success', 'Welcome');
        }
    }

    public function ShowFormPerpanjangan($pemakamanid)
    {
        $pemakamans = DB::table('Pemakaman')
            ->where('Pemakaman.id', '=', $pemakamanid)
            ->get();
        return view('Perpanjangan.insert_perpanjangan')->with([
            "pemakamanname" => $pemakamans,

        ]);
    }

    // Fungsi ini dipakai member untuk pengajuan perpanjangan (form pengajuan perpanjangan)
    public function ShowFormPerpanjanganBaru(){

        $user_role= Auth::user()->role;
        if($user_role == "admin_tpu"){
            $cetakPemakamanName = DB::table('pemakaman','users')
                ->join('users','pemakaman.id','=','users.pemakaman_id')
                ->where('users.id','=',Auth::user()->id)
                ->get();
            return view('Perpanjangan.CetakPerpanjangan')->with([
                "cetakPemakamanName"=>$cetakPemakamanName,
            ]);
        }

        return view('Member.IPTM.ajukan-perpanjangan');
    }

    public function ShowSuksesInputPerpanjangan($id)
    {
        $perpanjangan = DB::table('perpanjangan')
            ->where('perpanjangan.id', '=', $id)
            ->get();
        return view('Perpanjangan.SuksesInsert_perpanjangan')->with([
            "selesaiperpanjangan" => $perpanjangan
        ]);

    }

    public function PrintIPTMPerpanjangan($id)
    {
      // code...
      $perpanjangan = Perpanjangan::find($id);

      $fileName = $perpanjangan->nomor_surat . '_' . time() . '.pdf';
      $pdf = PDF::loadView('Perpanjangan.Print-perpanjangan', get_defined_vars());
      $pdf->save(storage_path("Document\Perpanjangan\$fileName"));

      return $pdf->stream($fileName);
    }

}
