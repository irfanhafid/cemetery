<?php

namespace App\Http\Controllers;

use App\IPTM;
use App\Pemakaman;
use App\User;
use App\Tumpangan;
use App\Pemindahan;
use App\NomorPemesanan;
use App\Perpanjangan;
use App\JadwalPemakaman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Response;
use App;

class IPTMController extends Controller
{
    // Belum dipakai
    public function ShowAllIPTM(){

        if(Auth::user()->role == "member"){
            return view('Member.IPTM.status-pesanan');
        }

        return redirect('/');
    }

    public function ShowSuksesInput($inputType, $id)
    {
        $output = null;

        if($inputType == "perpanjangan"){
            $output = DB::table('perpanjangan')
                ->where('perpanjangan.id', '=', $id)
                ->get();

        }else if($inputType == "tumpangan"){
            $output = DB::table('tumpangan')
                ->where('tumpangan.id', '=', $id)
                ->get();
        }/*else if($inputType == "pemindahan"){
            $output = DB::table('pemindahan')
                ->where('ijinpemindahan.pemindahanid', '=', $id)
                ->get();
        }*/

        return view('Perpanjangan.SuksesInsert_perpanjangan')->with([
            "data" => $output,
            "tipe" => $inputType
        ]);

    }

    //Tidak Dipakai
    public function PrintIPTM($inputType, $id){

        $output = null;

        if ($inputType == "perpanjangan")
        {
            $output = DB::table("perpanjangan")->where("perpanjangan.id", "=", $id)
                ->join('iptm', 'iptm.id', '=', 'perpanjangan.iptm_id')
                ->join('makam', 'makam.id', '=', 'iptm.makam_id')
                ->join('almarhum', 'almarhum.iptm_id', '=', 'iptm.id')
                ->join('ahli_waris', 'almarhum.ahli_waris_id', '=', 'ahli_waris.id')->get();
        }
        else if($inputType == "pemindahan")
        {
            $output = DB::table("pemindahan")->where("pemindahan.id", "=", $id)
                ->join('iptm', 'iptm.id', '=', 'pemindahan.iptm_id')
                ->join('makam', 'makam.id', '=', 'iptm.makam_id')
                ->join('almarhum', 'almarhum.iptm_id', '=', 'iptm.id')
                ->join('ahli_waris', 'almarhum.ahli_waris_id', '=', 'ahli_waris.id')->get();
        }
        else if($inputType == "tumpangan")
        {
            $output = DB::table("tumpangan")->where("tumpangan.id", "=", $id)
                ->join('iptm', 'iptm.id', '=', 'tumpangan.iptm_id')
                ->join('makam', 'makam.id', '=', 'iptm.makam_id')
                ->join('almarhum', 'almarhum.iptm_id', '=', 'iptm.id')
                ->join('ahli_waris', 'almarhum.ahli_waris_id', '=', 'ahli_waris.id')->get();
        }

        return view('printIPTM')->with([
            'data' => $output,
            "tipe" => $inputType
        ]);
    }

    // Tidak Dipakai
    public function ShowRiwayatCetakIPTM(){
        return view('IPTM.riwayat-cetak');
    }

    public function ShowMakamKadaluarsa(){
        $pemakamanId = Auth::user()->pemakaman_id;
        $makam = DB::table('iptm')
                ->join('almarhum','almarhum.iptm_id','=','iptm.id')
                ->join('ahli_waris','ahli_waris.id','=','almarhum.ahli_waris_id')
                ->join('makam','makam.id','=','iptm.makam_id')
                ->join('pemakaman','pemakaman.id','=','makam.pemakaman_id')
                ->where('iptm.masa_berlaku', '<', now())
                ->where('makam.pemakaman_id','=',$pemakamanId)
                ->get();

        return view("Makam.makam-kadaluarsa")->with([
            "makam" => $makam,
        ]);
    }

    //Tidak dipakai
    public function ShowNewIPTMForm(){
        if(Auth::user()->role == "member"){

            return view('Member.buat-iptm');

        }else{
            return redirect('/');
        }
    }

    //JSON
    public function RequestGetIPTM(Request $req){
        $noIptm = "";
        $namaAlmarum = "";
        if($req->query('noiptm')){
            $noIptm = $req->query('noiptm');
        }elseif ($req->query('namaAlmarhum')){
            $namaAlmarum = $req->query('namaAlmarhum');
        }

        $pemakamanId = Auth::user()->pemakaman_id;

        $iptm = DB::table("iptm")
            ->select('*','almarhum.id AS almarhum_id')
            ->join('makam', 'makam.id', '=', 'iptm.makam_id')
            ->join('almarhum', 'almarhum.iptm_id', '=', 'iptm.id')
            ->join('ahli_waris', 'almarhum.ahli_waris_id', '=', 'ahli_waris.id')
            ->where('makam.pemakaman_id','=', $pemakamanId)
            ->where('iptm.nomor_iptm', "=", $noIptm)
            ->orWhere('almarhum.nama_almarhum', "=", $namaAlmarum)
            ->get();

        return json_encode($iptm);
    }

    public function RequestGetIPTMByNo(Request $req){
        $noIptm = $req->query('noiptm');

        $jenisPemesanan = $req->query('jenis_pemesanan');

        $pemakamanId = session('pemakaman_id');

        $iptm = "[]";

        if ($jenisPemesanan == "tumpangan") {
          $iptm = DB::table("iptm")
              ->select('*','almarhum.id AS almarhum_id')
              ->join('makam', 'makam.id', '=', 'iptm.makam_id')
              ->join('pemakaman', 'pemakaman.id', '=', 'makam.pemakaman_id')
              ->join('almarhum', 'almarhum.iptm_id', '=', 'iptm.id')
              ->join('ahli_waris', 'almarhum.ahli_waris_id', '=', 'ahli_waris.id')
              ->where('iptm.nomor_iptm', "=", $noIptm)
              ->where('pemakaman.id','=',$pemakamanId)
              // ->where(DB::raw('DATE_ADD(iptm.tanggal_iptm, INTERVAL 1 YEAR)'),'=',now())
              // ->where('iptm.masa_berlaku','>',now())
              ->orderBy('almarhum.tanggal_wafat', 'desc')
              ->get();
        }elseif ($jenisPemesanan == "perpanjangan") {
          $iptm = DB::table("iptm")
              ->select('*','almarhum.id AS almarhum_id')
              ->join('makam', 'makam.id', '=', 'iptm.makam_id')
              ->join('pemakaman', 'pemakaman.id', '=', 'makam.pemakaman_id')
              ->join('almarhum', 'almarhum.iptm_id', '=', 'iptm.id')
              ->join('ahli_waris', 'almarhum.ahli_waris_id', '=', 'ahli_waris.id')
              ->where('iptm.nomor_iptm', "=", $noIptm)
              ->where('pemakaman.id','=',$pemakamanId)
              // ->where(DB::raw('DATE_SUB(iptm.masa_berlaku, INTERVAL 2 MONTH)'),'=',now())
              // ->where('iptm.masa_berlaku','>',now())
              ->orderBy('almarhum.tanggal_wafat', 'desc')
              ->get();
        }

        return json_encode($iptm);
    }

    public function riwayat()
    {
      $user = Auth::user();
      $pemakamanId = Auth::user()->pemakaman_id;

      $tumpangan = Tumpangan::select([
          'tumpangan.id AS id',
          'tumpangan.nomor_surat',
          'tumpangan.tanggal_surat',
          \DB::raw('"tumpangan" AS tipe_pesanan'),
          'pemakaman.nama_pemakaman',
          'tumpangan.status',
          'tumpangan.created_at',
      ])
          ->join('iptm', 'iptm.id', '=', 'tumpangan.iptm_id')
          ->join('makam', 'makam.id', '=', 'iptm.makam_id')
          ->join('pemakaman', 'pemakaman.id', '=', 'makam.pemakaman_id')
          ->join('users','users.id','=','tumpangan.user_id')
          ->where('tumpangan.user_id','=',$user->id)
          ->where('pemakaman.id','=',$pemakamanId)
          ->get();

      $perpanjangan = Perpanjangan::select([
              'perpanjangan.id AS id',
              'perpanjangan.nomor_surat',
              'perpanjangan.tanggal_surat',
              \DB::raw('"perpanjangan" AS tipe_pesanan'),
              'pemakaman.nama_pemakaman',
              'perpanjangan.status',
              'perpanjangan.created_at',
          ])
              ->join('iptm', 'iptm.id', '=', 'perpanjangan.iptm_id')
              ->join('makam', 'makam.id', '=', 'iptm.makam_id')
              ->join('pemakaman', 'pemakaman.id', '=', 'makam.pemakaman_id')
              ->join('users','users.id','=','perpanjangan.user_id')
              ->where('perpanjangan.user_id','=',$user->id)
              ->where('pemakaman.id','=',$pemakamanId)
              ->get();

        $data = collect()->sortByDesc('created_at');

        foreach ($tumpangan as $tumpangans) {
            $data->push($tumpangans);
        }

        foreach ($perpanjangan as $perpanjangans) {
            $data->push($perpanjangans);
        }

        return view('IPTM.riwayat-cetak', get_defined_vars());
    }

    public function RequestCountDataAdminTPU(Request $request){

        $TPUId = $request->query('idTPU');
        $role = $request->query('roleAdmin');

        if ($role == 'admin_dinas') {
          $jumlahTPU = DB::table('pemakaman')->select(['*', DB::raw('IFNULL(id,0)')])
                       ->get()
                       ->count();

                       $pemesananTumpangan = DB::table('tumpangan')->select(['*', DB::raw('IFNULL(tumpangan.id,0)')])
                                          ->join('iptm','iptm.id','=','tumpangan.iptm_id')
                                          ->join('makam','makam.id','=','iptm.makam_id')
                                          ->join('pemakaman','pemakaman.id','=','makam.pemakaman_id')
                                          ->get();

                       $pemesananPerpanjangan = DB::table('perpanjangan')->select(['*', DB::raw('IFNULL(perpanjangan.id,0)')])
                                          ->join('iptm','iptm.id','=','perpanjangan.iptm_id')
                                          ->join('makam','makam.id','=','iptm.makam_id')
                                          ->join('pemakaman','pemakaman.id','=','makam.pemakaman_id')
                                          ->get();

                       $pemesanan = $pemesananTumpangan->merge($pemesananPerpanjangan)->sortByDesc('created_at')->count();

                       $menungguTumpangan = DB::table('tumpangan')->select(['*', DB::raw('IFNULL(tumpangan.id,0)')])
                                          ->join('iptm','iptm.id','=','tumpangan.iptm_id')
                                          ->join('makam','makam.id','=','iptm.makam_id')
                                          ->join('pemakaman','pemakaman.id','=','makam.pemakaman_id')
                                          ->where('pemakaman.id','=',$TPUId)
                                          ->where("tumpangan.status","=","1")
                                          ->get();

                       $menungguPerpanjangan = DB::table('perpanjangan')->select(['*', DB::raw('IFNULL(perpanjangan.id,0)')])
                                          ->join('iptm','iptm.id','=','perpanjangan.iptm_id')
                                          ->join('makam','makam.id','=','iptm.makam_id')
                                          ->join('pemakaman','pemakaman.id','=','makam.pemakaman_id')
                                          ->where('pemakaman.id','=',$TPUId)
                                          ->where("perpanjangan.status","=","1")
                                          ->get();

                       $permohonan = $menungguTumpangan->merge($menungguPerpanjangan)->sortByDesc('created_at')->count();

                       $tumpangan = Tumpangan::select([
                           'tumpangan.id AS id',
                           'tumpangan.status',
                           \DB::raw("DATE_FORMAT(tanggal_permohonan, '%M') AS month"),
                       ])
                           ->join('iptm', 'iptm.id', '=', 'tumpangan.iptm_id')
                           ->join('makam', 'makam.id', '=', 'iptm.makam_id')
                           ->join('pemakaman', 'pemakaman.id', '=', 'makam.pemakaman_id')
                           ->join('users','users.id','=','tumpangan.user_id')
                           ->get();

                       $perpanjangan = Perpanjangan::select([
                           'perpanjangan.id AS id',
                           'perpanjangan.status',
                           \DB::raw("DATE_FORMAT(tanggal_permohonan, '%M') AS month"),
                       ])
                           ->join('iptm', 'iptm.id', '=', 'perpanjangan.iptm_id')
                           ->join('makam', 'makam.id', '=', 'iptm.makam_id')
                           ->join('pemakaman', 'pemakaman.id', '=', 'makam.pemakaman_id')
                           ->join('users','users.id','=','perpanjangan.user_id')
                           ->get();

                       $data = collect();
                       foreach ($tumpangan as $item) {
                           $data->push($item);
                       }
                       foreach ($perpanjangan as $item) {
                           $data->push($item);
                       }
                       $group = $data->groupBy('month');

                       $data1['labels'] = [];
                       $data2['labels'] = [];
                       $waiting = [];
                       $approve = [];
                       $reject = [];
                       $arrayTumpangan = [];
                       $arrayPerpanjangan = [];
                       foreach($group as $month => $item){
                           array_push($data1['labels'], $month);
                           array_push($waiting, $item->where('status', 1)->count());
                           array_push($approve, $item->where('status', 2)->count());
                           array_push($reject, $item->where('status', 3)->count());

                           array_push($data2['labels'], $month);
                           array_push($arrayTumpangan, $tumpangan->where('month', $month)->count());
                           array_push($arrayPerpanjangan, $perpanjangan->where('month', $month)->count());
                       }
                       $data1['datasets'] = [
                           [
                               'label' => 'Waiting',
                               'backgroundColor' => '#ffc107',
                               'data' => $waiting,
                           ],
                           [
                               'label' => 'Approve',
                               'backgroundColor' => '#28a745',
                               'data' => $approve,
                           ],
                           [
                               'label' => 'Reject',
                               'backgroundColor' => '#dc3545',
                               'data' => $reject,
                           ]
                       ];

                       $data2['datasets'] = [
                           [
                               'label' => 'Tumpangan',
                               'backgroundColor' => '#007bff',
                               'borderColor' => '#007bff',
                               'fill' => false,
                               'data' => $arrayTumpangan,
                           ],
                           [
                               'label' => 'Perpanjangan',
                               'backgroundColor' => '#20c997',
                               'borderColor' => '#20c997',
                               'fill' => false,
                               'data' => $arrayPerpanjangan,
                           ],
                       ];

                       return Response::json(array('jumlahTPU'=>$jumlahTPU,'pemesanan'=>$pemesanan,'permohonan'=>$permohonan, 'barChart'=>$data1, 'lineChart'=>$data2));

        }else{

        $jumlahPemakaman =  DB::table('jadwal_pemakaman')->select(['*', DB::raw('IFNULL(jadwal_pemakaman.id,0)')])
                           ->join('iptm','iptm.id','=','jadwal_pemakaman.iptm_id')
                           ->join('makam','makam.id','=','iptm.makam_id')
                           ->join('pemakaman','pemakaman.id','=','makam.pemakaman_id')
                           ->where('pemakaman.id','=',$TPUId)
                           ->get()
                           ->count();

        $makamKadaluarsa = DB::table('iptm')->select(['*', DB::raw('IFNULL(iptm.id,0)')])
                           ->join('makam','makam.id','=','iptm.makam_id')
                           ->join('pemakaman','pemakaman.id','=','makam.pemakaman_id')
                           ->where('pemakaman.id','=',$TPUId)
                           ->where("iptm.masa_berlaku", "<", now())
                           ->get()
                           ->count();

        $pemesananTumpangan = DB::table('tumpangan')->select(['*', DB::raw('IFNULL(tumpangan.id,0)')])
                           ->join('iptm','iptm.id','=','tumpangan.iptm_id')
                           ->join('makam','makam.id','=','iptm.makam_id')
                           ->join('pemakaman','pemakaman.id','=','makam.pemakaman_id')
                           ->where('pemakaman.id','=',$TPUId)
                           ->where("tumpangan.status","=","1")
                           ->get();

        $pemesananPerpanjangan = DB::table('perpanjangan')->select(['*', DB::raw('IFNULL(perpanjangan.id,0)')])
                           ->join('iptm','iptm.id','=','perpanjangan.iptm_id')
                           ->join('makam','makam.id','=','iptm.makam_id')
                           ->join('pemakaman','pemakaman.id','=','makam.pemakaman_id')
                           ->where('pemakaman.id','=',$TPUId)
                           ->where("perpanjangan.status","=","1")
                           ->get();

        $pemeriksaan = $pemesananTumpangan->merge($pemesananPerpanjangan)->sortByDesc('created_at')->count();

        $penolakanTumpangan = DB::table('tumpangan')->select(['*', DB::raw('IFNULL(tumpangan.id,0)')])
                           ->join('iptm','iptm.id','=','tumpangan.iptm_id')
                           ->join('makam','makam.id','=','iptm.makam_id')
                           ->join('pemakaman','pemakaman.id','=','makam.pemakaman_id')
                           ->where('pemakaman.id','=',$TPUId)
                           ->where("tumpangan.status","=","3")
                           ->get();

        $penolakanPerpanjangan = DB::table('perpanjangan')->select(['*', DB::raw('IFNULL(perpanjangan.id,0)')])
                           ->join('iptm','iptm.id','=','perpanjangan.iptm_id')
                           ->join('makam','makam.id','=','iptm.makam_id')
                           ->join('pemakaman','pemakaman.id','=','makam.pemakaman_id')
                           ->where('pemakaman.id','=',$TPUId)
                           ->where("perpanjangan.status","=","3")
                           ->get();

        $penolakan = $penolakanTumpangan->merge($penolakanPerpanjangan)->sortByDesc('created_at')->count();

        return Response::json(array('jumlahPemakaman'=>$jumlahPemakaman,'makamKadaluarsa'=>$makamKadaluarsa,'pemeriksaan'=>$pemeriksaan,'penolakan'=>$penolakan));
      }

      return Response::json(array('jumlahPemakaman'=>'null','makamKadaluarsa'=>'null','pemeriksaan'=>'null','penolakan'=>'null'));
    }
}
