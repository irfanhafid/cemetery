<?php

namespace App\Http\Controllers;

use App\AhliWaris;
use App\Almarhum;
use App\IPTM;
use App\Makam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Tumpangan;
use App\NomorPemesanan;
use PDF;
use RealRashid\SweetAlert\Facades\Alert;

class TumpanganController extends Controller
{
    //TUMPANGAN
    public function SubmitTumpangan(Request $request)
    {
        $iptm_id = $request['iptm_id'];
        $user = auth()->user();

        if ($user->role == "admin_tpu")
        {
            $iptm_tumpangan = IPTM::find($iptm_id);
            $iptm_tumpangan->tanggal_iptm = $iptm_tumpangan->tanggal_iptm;
            $iptm_tumpangan->masa_berlaku = $iptm_tumpangan->masa_berlaku;
            $iptm_tumpangan->save();

            $ahliwaris = new AhliWaris();
            $ahliwaris->nama_ahliwaris = $request["nama_ahliwaris"];
            $ahliwaris->nomor_ktp_ahliwaris = $request["nomor_ktp_ahliwaris"];
            $ahliwaris->alamat_ahliwaris = $request["alamat_ahliwaris"];
            $ahliwaris->rt_ahliwaris = $request["rt_ahliwaris"];
            $ahliwaris->rw_ahliwaris = $request["rw_ahliwaris"];
            $ahliwaris->kelurahan_ahliwaris = $request["kelurahan_ahliwaris"];
            $ahliwaris->kecamatan_ahliwaris = $request["kecamatan_ahliwaris"];
            $ahliwaris->kota_ahliwaris = $request["kota_ahliwaris"];
            $ahliwaris->telepon_ahliwaris = $request["telepon_ahliwaris"];
            $ahliwaris->hubungan_ahliwaris = $request["hubungan_ahliwaris"];
            $ahliwaris->save();

            $almarhum = new Almarhum();
            //$almarhum->iptm_id = $iptm_id;
            $almarhum->ahli_waris_id = $ahliwaris->id;
            $almarhum->nama_almarhum = $request["nama_almarhum"];
            $almarhum->tanggal_wafat = $request["tanggal_wafat"];
            $almarhum->nomor_ktp_almarhum = $request["nomor_ktp_almarhum"];
            $almarhum->nomor_kk_almarhum = $request["nomor_kk_almarhum"];
            $almarhum->nomor_sk_kematian_kelurahan = $request["nomor_sk_kematian_kelurahan"];
            $almarhum->nomor_sk_kematian_rs = $request["nomor_sk_kematian_rs"];
            $almarhum->nomor_sp_rtrw = $request["nomor_sp_rtrw"];
            $almarhum->tanggal_sp_rtrw = $request["tanggal_sp_rtrw"];
            $almarhum->save();

            $tumpangan = new Tumpangan();
            $tumpangan->iptm_id = $iptm_id;
            $tumpangan->user_id = Auth::user()->id;
            $tumpangan->ahli_waris_id = $ahliwaris->id;
            $tumpangan->almarhum_id = $almarhum->id;
            $tumpangan->nomor_sk_kehilangan_kepolisian = $request["nomor_sk_kehilangan_kepolisian"];
            $tumpangan->tanggal_sk_kehilangan_kepolisian = $request["tanggal_sk_kehilangan_kepolisian"];
            $tumpangan->file_sk_kehilangan_kepolisian = $request["file_sk_kehilangan_kepolisian"];
            $tumpangan->nomor_surat = $request["nomor_surat"];
            $tumpangan->tanggal_surat = $request["tanggal_surat"];
            $tumpangan->daftar_jenazah = $request["daftar_jenazah"];
            $tumpangan->tanggal_berlaku_dari = $request["tanggal_berlaku_dari"];
            $tumpangan->tanggal_berlaku_sampai = $request["tanggal_berlaku_sampai"];
            $tumpangan->status = "2";
            $tumpangan->cetak_oleh = $user->fullname;
            $tumpangan->save();

            $getdata = Tumpangan::find($tumpangan->id);
            $iptm = IPTM::find($tumpangan->iptm_id);
            $makam = Makam::find($iptm_id);

            $pdf = PDF::loadView('Tumpangan.Print-tumpangan', ['getdata'=>$getdata, 'almarhum'=>$almarhum, 'ahliwaris'=>$ahliwaris, 'iptm'=>$iptm, 'makam'=>$makam]);
            $fileName = str_replace('/', '-', $tumpangan->nomor_surat) . '_' . time() . '.pdf';
            $tumpangan->file_iptm = $fileName;
            $tumpangan->save();
            return $pdf->stream("$fileName");
            return redirect('IPTM/tumpangan');
        }
        else {
            $ahliwaris = new AhliWaris();

            //Foto KTP Ahliwaris
            if ($request->hasFile('file_ktp_ahliwaris')) {
                $file = $request->file('file_ktp_ahliwaris');
                $file->move(public_path('/Document/Tumpangan'), $file->getClientOriginalName());

                $ahliwaris->file_ktp_ahliwaris = $file->getClientOriginalName();
            }

            $ahliwaris->nomor_ktp_ahliwaris = $request->nomor_ktp_ahliwaris;
            $ahliwaris->nama_ahliwaris = $request->nama_ahliwaris;
            $ahliwaris->alamat_ahliwaris = $request->alamat_ahliwaris;
            $ahliwaris->rt_ahliwaris = $request->rt_ahliwaris;
            $ahliwaris->rw_ahliwaris = $request->rw_ahliwaris;
            $ahliwaris->kelurahan_ahliwaris = $request->kelurahan_ahliwaris;
            $ahliwaris->kecamatan_ahliwaris = $request->kecamatan_ahliwaris;
            $ahliwaris->kota_ahliwaris = $request->kota_administrasi;
            $ahliwaris->telepon_ahliwaris = $request->telepon_ahliwaris;
            $ahliwaris->hubungan_ahliwaris = $request->hubungan_ahliwaris;
            $ahliwaris->save();

            $almarhum = new Almarhum();
            $almarhum->ahli_waris_id = $ahliwaris->id;
            $almarhum->nama_almarhum = $request->nama_almarhum;
            $almarhum->tanggal_wafat = $request->tanggal_wafat;

            $almarhum->nomor_ktp_almarhum = $request->nomor_ktp_almarhum;

            //Foto KTP Almarhum
            if ($request->hasFile('file_ktp_almarhum')) {
                $file = $request->file('file_ktp_almarhum');
                $file->move(public_path('/Document/Tumpangan'), $file->getClientOriginalName());

                $almarhum->file_ktp_almarhum = $file->getClientOriginalName();
            }

            $almarhum->nomor_kk_almarhum = $request->nomor_kk_almarhum;

            //Foto KK Almarhum
            if ($request->hasFile('file_kk_almarhum')) {
                $file = $request->file('file_kk_almarhum');
                $file->move(public_path('/Document/Tumpangan'), $file->getClientOriginalName());

                $almarhum->file_kk_almarhum = $file->getClientOriginalName();
            }

            //Surat pengantar RT/RW
            $almarhum->nomor_sp_rtrw = $request->nomor_sp_rtrw;
            $almarhum->tanggal_sp_rtrw = $request->tanggal_sp_rtrw;
            if ($request->hasFile('file_sp_rtrw')) {
                $file = $request->file('file_sp_rtrw');
                $file->move(public_path('/Document/Tumpangan'), $file->getClientOriginalName());

                $almarhum->file_sp_rtrw = $file->getClientOriginalName();
            }

            //Surat keterangan RS
            $almarhum->nomor_sk_kematian_rs = $request->nomor_sk_kematian_rs;
            $almarhum->tanggal_sk_kematian_rs = $request->tanggal_sk_kematian_rs;
            if ($request->hasFile('file_sk_kematian_rs')) {
                $file = $request->file('file_sk_kematian_rs');
                $file->move(public_path('/Document/Tumpangan'), $file->getClientOriginalName());

                $almarhum->file_sk_kematian_rs = $file->getClientOriginalName();
            }

            //Surat keterangan kematian kelurahan
            $almarhum->nomor_sk_kematian_kelurahan = $request->nomor_sk_kematian_kelurahan;
            $almarhum->tanggal_sk_kematian_kelurahan = $request->tanggal_sk_kematian_kelurahan;
            if ($request->hasFile('file_sk_kematian_kelurahan')) {
                $file = $request->file('file_sk_kematian_kelurahan');
                $file->move(public_path('/Document/Tumpangan'), $file->getClientOriginalName());

                $almarhum->file_sk_kematian_kelurahan = $file->getClientOriginalName();
            }
            $almarhum->save();

            $iptm_tumpangan = new Tumpangan();
            $iptm_tumpangan->iptm_id = $iptm_id;
            $iptm_tumpangan->status = 1;
            $iptm_tumpangan->almarhum_id = $almarhum->id;
            $iptm_tumpangan->ahli_waris_id = $almarhum->ahli_waris_id;
            $iptm_tumpangan->user_id = Auth::user()->id;
            $iptm_tumpangan->tanggal_permohonan = now();

            $iptm_tumpangan->nomor_sk_kehilangan_kepolisian = $request->nomor_kehilangan_kepolisian;
            $iptm_tumpangan->tanggal_sk_kehilangan_kepolisian = $request->tanggal_kehilangan_kepolisian;
            if ($request->hasFile('file_sk_kehilangan_kepolisian')) {
                $file = $request->file('file_sk_kehilangan_kepolisian');
                $file->move(public_path('/Document/Tumpangan'), $file->getClientOriginalName());
                $iptm_tumpangan->file_sk_kehilangan_kepolisian = $file->getClientOriginalName();
            }
            if ($request->hasFile('file_iptm_asli')) {
                $file = $request->file('file_iptm_asli');
                $file->move(public_path('/Document/Tumpangan'), $file->getClientOriginalName());
                $iptm_tumpangan->file_iptm_asli = $file->getClientOriginalName();
            }

            $iptm_tumpangan->save();
            $tumpanganId = $iptm_tumpangan->id;
            $nomorPermohonan = "TP-".str_pad( $tumpanganId, 3, 0, STR_PAD_LEFT);
            $iptm_tumpangan->nomor_permohonan = $nomorPermohonan;
            $iptm_tumpangan->save();
            /*Alert::success('Sukses!', 'Data Permohonan telah tersimpan');*/
            /*alert()->success('Sukses!','Data Permohonan berhasil disimpan.');*/
            toast('Data Permohonan Telah Tersimpan','success');

            return redirect('/IPTM/tumpangan/sukses/' . $iptm_tumpangan->id)->with('register_success', 'Welcome');
        }
    }

    public function ShowFormTumpangan($pemakamanid)
    {
        $pemakamans = DB::table('pemakaman')
            ->where('pemakaman.id', '=', $pemakamanid)
            ->get();
        return view('Tumpangan.insert_tumpangan')->with([
            "pemakamanname" => $pemakamans,
        ]);
    }

    public function ShowFormTumpanganBaru(){

        $user_role= Auth::user()->role;
        if($user_role == "admin_tpu") {
            $cetakPemakamanName = DB::table('pemakaman', 'users')
                ->join('users', 'pemakaman.id', '=', 'users.pemakaman_id')
                ->where('users.id', '=', Auth::user()->id)
                ->get();

            return view('Tumpangan.CetakTumpangan')->with([
                "cetakPemakamanName" => $cetakPemakamanName,
            ]);
        }
        return view ('Member.IPTM.ajukan-tumpangan');

    }

    public function ShowSuksesInput($id)
    {
        $tumpangan = DB::table('tumpangan')
            ->where('tumpangan.id', '=', $id)
            ->get();
        return view('Tumpangan.SuksesInsert')->with([
            "selesaitumpangan" => $tumpangan
        ]);
    }

}
