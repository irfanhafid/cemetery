<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AhliWaris extends Model
{
    protected $table='ahli_waris';
    protected $primaryKey='id';
    protected $fillable =[
      'nomor_ktp_ahliwaris',
      'nama_ahliwaris',
      'alamat_ahliwaris',
      'rt_ahliwaris',
      'rw_ahliwaris',
      'kelurahan_ahliwaris',
      'kecamatan_ahliwaris',
      'kota_ahliwaris',
      'telepon_ahliwaris',
      'hubungan_ahliWaris',
    ];
}
