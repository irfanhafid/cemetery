<?php

namespace App\Imports;

use App\Almarhum;
use App\AhliWaris;
use App\IPTM;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class AlmarhumImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
          $row['ahli_waris_id'] = $row['nama_ahliwaris'] == "null" ? null : AhliWaris::select('id')->where("nama_ahliwaris","like","%".$row['nama_ahliwaris']."%")->first()->id;

          $row['iptm_id'] = $row['nomor_iptm'] == "null" ? null : IPTM::select('id')->where("nomor_iptm","like","%".$row['nomor_iptm']."%")->first()->id;

        return new Almarhum([
          'ahli_waris_id'=>$row['ahli_waris_id'],
          'iptm_id'=>$row['iptm_id'],
          'nama_almarhum'=>$row['nama_almarhum'],
          'tanggal_wafat'=>\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['tanggal_wafat']),
          'nomor_ktp_almarhum'=>$row['nomor_ktp_almarhum'],
          'nomor_kk_almarhum'=>$row['nomor_kk_almarhum'],
          'nomor_sp_rtrw'=>$row['nomor_sp_rtrw'],
          'tanggal_sp_rtrw'=>\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['tanggal_sp_rtrw']),
          'nomor_sk_kematian_rs'=>$row['nomor_sk_kematian_rs'],
          'tanggal_sk_kematian_rs'=>\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['tanggal_sk_kematian_rs']),
          'nomor_sk_kematian_kelurahan'=>$row['nomor_sk_kematian_kelurahan'],
          'tanggal_sk_kematian_kelurahan'=>\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['tanggal_sk_kematian_kelurahan']),
        ]);
    }
}
