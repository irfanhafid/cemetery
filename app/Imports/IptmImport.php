<?php

namespace App\Imports;

use App\IPTM;
use App\Makam;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class IptmImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

      $row['makam_id'] = Makam::select('id')->where("blok","like","%".$row['blok']."%")->where("blad","like","%".$row['blad']."%")->where("petak","like","%".$row['petak']."%")->first()->id;

        return new IPTM([
          'makam_id'=>$row['makam_id'],
          'nomor_iptm'=>$row['nomor_iptm'],
          'tanggal_iptm'=>\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['tanggal_iptm']),
          'masa_berlaku'=>\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['masa_berlaku']),
        ]);
    }
}
