<?php

namespace App\Imports;

use App\Makam;
use App\Pemakaman;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MakamImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
      $row['pemakaman_id'] = Pemakaman::select('id')->where("nama_pemakaman","like","%".$row['nama_pemakaman']."%")->first()->id;

        return new Makam([
          'pemakaman_id'=>$row['pemakaman_id'],
          'blok'=>$row['blok'],
          'blad'=>$row['blad'],
          'petak'=>$row['petak'],
        ]);
    }
}
