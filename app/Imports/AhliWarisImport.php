<?php

namespace App\Imports;

use App\AhliWaris;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class AhliWarisImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        return new AhliWaris([
          'nomor_ktp_ahliwaris'=>$row['nomor_ktp_ahliwaris'],
          'nama_ahliwaris'=>$row['nama_ahliwaris'],
          'alamat_ahliwaris'=>$row['alamat_ahliwaris'],
          'rt_ahliwaris'=>$row['rt_ahliwaris'],
          'rw_ahliwaris'=>$row['rw_ahliwaris'],
          'kelurahan_ahliwaris'=>$row['kelurahan_ahliwaris'],
          'kecamatan_ahliwaris'=>$row['kecamatan_ahliwaris'],
          'kota_ahliwaris'=>$row['kota_ahliwaris'],
          'telepon_ahliwaris'=>$row['telepon_ahliwaris'],
          'hubungan_ahliWaris'=>$row['hubungan_ahliWaris'],
        ]);
    }
}
