<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $table='users';
    protected $primaryKey='id';

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $fillable = [
        'fullname',
        'email',
        'password',
        'address',
        'gender',
        'role',
        'pemakaman_id'
    ];

    public function pemakaman()
    {
        return $this->belongsTo(Pemakaman::class, 'pemakaman_id');
    }
}
