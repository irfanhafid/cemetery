<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Makam extends Model
{
    //
    protected $table='makam';
    protected $primaryKey='id';
    protected $fillable = [
        'pemakaman_id',
        'blok',
        'blad',
        'petak',
    ];

    public function pemakaman()
    {
        return $this->belongsTo(Pemakaman::class, 'pemakaman_id');
    }
}
