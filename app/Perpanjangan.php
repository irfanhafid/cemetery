<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perpanjangan extends Model
{
    protected $table='perpanjangan';
    protected $primaryKey='id';
    protected $fillable = [
        'iptm_id',
        'almarhum_id',
        'ahli_waris_id',
        'user_id',
    ];

    public function iptm()
    {
        return $this->belongsTo(IPTM::class, 'iptm_id');
    }

    public function almarhum()
    {
        return $this->belongsTo(Almarhum::class, 'almarhum_id');
    }

    public function ahliwaris()
    {
        return $this->belongsTo(AhliWaris::class, 'ahli_waris_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
