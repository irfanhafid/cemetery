<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemakaman extends Model
{
    protected $table='pemakaman';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nama_pemakaman',
        'alamat_pemakaman',
        'kelurahan_pemakaman',
        'kecamatan_pemakaman',
        'kota_pemakaman',
        'provinsi_pemakaman',
        'telepon_pemakaman',
        'kodepos_pemakaman',
        'email_pemakaman',
        'jumlah_makam',
        'luas_pemakaman',
        'deskripsi_pemakaman',
        'photo_pemakaman',
        'kepala_pemakaman',
        'nip_kepala_pemakaman',
    ];

    public function users()
    {
        return $this->hasMany(User::class, 'pemakaman_id');
    }
}
