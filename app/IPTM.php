<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IPTM extends Model
{
    //
    protected $table='iptm';
    protected $primaryKey='id';

    protected $fillable=[
        'makam_id',
        'nomor_iptm',
        'tanggal_iptm',
        'masa_berlaku'
    ];

    public function makam()
    {
        return $this->belongsTo(Makam::class, 'makam_id');
    }

    public function almarhum()
    {
        return $this->hasOne(Almarhum::class, 'iptm_id');
    }
}
