<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JadwalPemakaman extends Model
{
    protected $table = 'jadwal_pemakaman';
    protected $fillable = [
        'iptm_id',
        'tanggal_pemakaman',
        'jam_pemakaman',
    ];

    public function iptm()
    {
        return $this->belongsTo(IPTM::class, 'iptm_id');
    }
}
