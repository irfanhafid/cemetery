<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToTableTumpangan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tumpangan', function (Blueprint $table) {
            $table->string('nomor_iptm')->nullable()->after('file_sk_kehilangan_kepolisian');
            $table->date('tanggal_iptm')->nullable()->after('file_sk_kehilangan_kepolisian');
            $table->unsignedInteger('user_id')->nullable()->after('file_sk_kehilangan_kepolisian');
            $table->string('file_iptm')->nullable()->after('file_sk_kehilangan_kepolisian');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
