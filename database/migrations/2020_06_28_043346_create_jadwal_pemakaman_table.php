<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJadwalPemakamanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal_pemakaman', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('iptm_id')->unsigned()->nullable();
            $table->date('tanggal_pemakaman');
            $table->time('jam_pemakaman');

            $table->foreign('iptm_id')->references('id')->on('iptm');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal_pemakaman');
    }
}
