-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 19 Jul 2020 pada 02.23
-- Versi Server: 10.1.29-MariaDB
-- PHP Version: 7.1.12

/*SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";*/


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- -------------------------------------------------------
DROP TABLE IF EXISTS `perpanjangan`;
DROP TABLE IF EXISTS `tumpangan`;
DROP TABLE IF EXISTS `almarhum`;
DROP TABLE IF EXISTS `ahli_waris`;
DROP TABLE IF EXISTS `jadwal_pemakaman`;
DROP TABLE IF EXISTS `nomorpemesanan`;
DROP TABLE IF EXISTS `iptm`;
DROP TABLE IF EXISTS `makam`;
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `peraturan`;
DROP TABLE IF EXISTS `pemakaman`;
DROP TABLE IF EXISTS `migrations`;
DROP TABLE IF EXISTS `password_resets`;
-- --------------------------------------------------------


CREATE TABLE IF NOT EXISTS `ahli_waris` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nomor_ktp_ahliwaris` bigint(20) DEFAULT NULL,
  `file_ktp_ahliwaris` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_ahliwaris` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat_ahliwaris` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rt_ahliwaris` int(11) DEFAULT NULL,
  `rw_ahliwaris` int(11) DEFAULT NULL,
  `kelurahan_ahliwaris` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kecamatan_ahliwaris` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kota_ahliwaris` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telepon_ahliwaris` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hubungan_ahliwaris` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data untuk tabel `ahli_waris`
--
INSERT INTO `ahli_waris` (`id`, `nomor_ktp_ahliwaris`,`file_ktp_ahliwaris`, `nama_ahliwaris`, `alamat_ahliwaris`, `rt_ahliwaris`, `rw_ahliwaris`, `kelurahan_ahliwaris`, `kecamatan_ahliwaris`, `kota_ahliwaris`, `telepon_ahliwaris`, `hubungan_ahliwaris`, `created_at`, `updated_at`) VALUES
    (1, 3174567895542, '', 'Ahliwaris 1', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SUAMI', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(2, 3174567895542, '', 'Ahliwaris 2', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(3, 3174567895542, '', 'Ahliwaris 3', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(4, 3174567895542, '', 'Ahliwaris 4', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(5, 3174567895542, '', 'Ahliwaris 5', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(6, 3174567895542, '', 'Ahliwaris 6', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(7, 3174567895542, '', 'Ahliwaris 7', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(8, 3174567895542, '', 'Ahliwaris 8', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(9, 3174567895542, '', 'Ahliwaris 9', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(10, 3174567895542, '', 'Ahliwaris 10', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(11, 3174567895542, '', 'Ahliwaris 11', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(12, 3174567895542, '', 'Ahliwaris 12', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(13, 3174567895542, '', 'Ahliwaris 13', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(14, 3174567895542, '', 'Ahliwaris 14', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(15, 3174567895542, '', 'Ahliwaris 15', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(16, 3174567895542, '', 'Ahliwaris 16', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(17, 3174567895542, '', 'Ahliwaris 17', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(18, 3174567895542, '', 'Ahliwaris 18', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(19, 3174567895542, '', 'Ahliwaris 19', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(20, 3174567895542, '', 'Ahliwaris 20', 'JL. KRAMAT PULO GG. XIV', 9, 4, 'GELORA', 'TANAH ABANG', 'JAKARTA PUSAT', '082288880000', 'SAUDARA', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(21, 3277000011112222, 'Icon KTP.jpg', 'Ahli Waris Baru 1', 'Jl Test', 1, 11, 'KELURAHAN', 'KECAMATAN', 'KOTA', '082288880000', 'SAUDARA', '2020-08-12 22:58:16', '2020-08-12 22:58:16'),
(22, 3277000011110000, 'Icon KTP.jpg', 'Ahli Waris Tumpangan 1', 'Jl Test', 8, 5, 'KELURAHAN', 'KECAMATAN', 'KOTA', '082288880000', 'SAUDARA', '2020-08-12 23:02:53', '2020-08-12 23:02:53'),
(23, 3172030704810030, NULL, 'SYAH RUD', 'Jl. Mujair', 8, 2, 'KELURAHAN', 'KECAMATAN', 'KOTA', '082245452020', 'AYAH', '2020-08-12 23:29:27', '2020-08-12 23:29:27'),
(24, 3172030704810030, NULL, 'test', 'Jl. Mujair', 1, 5, 'KELURAHAN', 'KECAMATAN', 'KOTA', '082245452020', 'SAUDARA', '2020-08-12 23:35:18', '2020-08-12 23:35:18'),
(25, 3277000011110000, NULL, 'AHLI WARIS', 'Jl. Mujair', 1, 2, 'KELURAHAN', 'KECAMATAN', 'KOTA', '082245452020', 'AYAH', '2020-08-12 23:40:28', '2020-08-12 23:40:28');

-- ---------------------------------------------------------------------

-- ----------------------------------------------------------------------
-- Struktur dari tabel `pemakaman`

CREATE TABLE IF NOT EXISTS `pemakaman` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kelurahan_pemakaman` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kecamatan_pemakaman` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kota_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provinsi_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telepon_pemakaman` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kodepos_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jumlah_makam` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `luas_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deskripsi_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kepala_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nip_kepala_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pemakaman`
--

INSERT INTO `pemakaman` (`id`, `nama_pemakaman`, `alamat_pemakaman`, `kelurahan_pemakaman`, `kecamatan_pemakaman`, `kota_pemakaman`, `provinsi_pemakaman`, `telepon_pemakaman`, `kodepos_pemakaman`, `email_pemakaman`, `jumlah_makam`, `luas_pemakaman`, `deskripsi_pemakaman`, `photo_pemakaman`, `kepala_pemakaman`, `nip_kepala_pemakaman`, `created_at`, `updated_at`) VALUES
(1, 'Karet Bivak', 'Jl. KH. Mas Mansyur Raya', 'Karet Tengsin', 'Tanah Abang', 'Jakarta', 'DKI Jakarta', '(021)5734785', '10250', 'tpubivak@gmail.com', 'Kira-kira 48,000', 'Kira-kira 16,2 hektare', 'TPU BIVAK tidak melayani pengajuan izin penggunaan tanah makam baru', 'Foto_Profil_Karet_Bivak.jpg', 'Saiman S.Ag.S.AP', '197609032008011027', '2020-07-17 10:00:00', '2020-08-03 21:03:08');

-- ----------------------------------------------------------------------

-- ----------------------------------------------------------------------
-- Struktur dari tabel `makam`

CREATE TABLE IF NOT EXISTS `makam` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pemakaman_id` int(10) UNSIGNED NOT NULL,
  `blok` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blad` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `petak` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `makam_pemakaman_id_foreign` (`pemakaman_id`),
  CONSTRAINT `makam_pemakaman_id_foreign` FOREIGN KEY (`pemakaman_id`) REFERENCES `pemakaman` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `makam`
--

INSERT INTO `makam` (`id`, `pemakaman_id`, `blok`, `blad`, `petak`, `created_at`, `updated_at`) VALUES
(1, 1, 'AII', '001', '0001', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(2, 1, 'AII', '001', '0002', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(3, 1, 'AII', '001', '0003', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(4, 1, 'AII', '001', '0004', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(5, 1, 'AII', '001', '0005', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(6, 1, 'AII', '001', '0006', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(7, 1, 'AII', '001', '0007', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(8, 1, 'AII', '001', '0008', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(9, 1, 'AII', '001', '0009', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(10, 1, 'AII', '001', '0010', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(11, 1, 'AII', '001', '0011', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(12, 1, 'AII', '001', '0012', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(13, 1, 'AII', '001', '0013', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(14, 1, 'AII', '001', '0014', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(15, 1, 'AII', '001', '0015', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(16, 1, 'AII', '001', '0016', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(17, 1, 'AII', '001', '0017', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(18, 1, 'AII', '001', '0018', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(19, 1, 'AII', '001', '0019', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(20, 1, 'AII', '001', '0020', '2020-04-19 10:00:00', '2020-04-19 10:00:00');

-- --------------------------------------------------------------------------------


-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `iptm` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `makam_id` int(10) UNSIGNED NOT NULL,
  `nomor_iptm` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_iptm` date DEFAULT NULL,
  `masa_berlaku` date DEFAULT NULL,
  `kelurahan_ptsp` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iptm_makam_id_foreign` (`makam_id`),
  CONSTRAINT `iptm_makam_id_foreign` FOREIGN KEY (`makam_id`) REFERENCES `makam` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `iptm`
--

INSERT INTO `iptm` (`id`, `makam_id`, `nomor_iptm`, `tanggal_iptm`, `masa_berlaku`, `kelurahan_ptsp`, `created_at`, `updated_at`) VALUES
(1, 1, '0001', '2017-07-17', '2020-07-17', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(2, 2, '0002', '2017-07-17', '2020-07-17', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(3, 3, '0003', '2017-07-17', '2020-07-17', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(4, 4, '0004', '2017-07-17', '2020-07-17', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(5, 5, '0005', '2017-07-17', '2020-07-17', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(6, 6, '0006', '2017-07-17', '2020-07-17', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(7, 7, '0007', '2017-07-17', '2020-07-17', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(8, 8, '0008', '2017-07-17', '2020-07-17', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(9, 9, '0009', '2017-07-17', '2020-07-17', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(10, 10, '0010', '2017-07-17', '2020-07-17', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(11, 11, '0011', '2020-08-01', '2023-08-01', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(12, 12, '0012', '2020-08-01', '2023-08-01', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(13, 13, '0013', '2020-08-01', '2023-08-01', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(14, 14, '0014', '2020-08-01', '2023-08-01', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(15, 15, '0015', '2020-08-01', '2023-08-01', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(16, 16, '0016', '2020-08-01', '2023-08-01', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(17, 17, '0017', '2020-08-01', '2023-08-01', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(18, 18, '0018', '2020-08-01', '2023-08-01', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(19, 19, '0019', '2020-08-01', '2023-08-01', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26'),
(20, 20, '0020', '2020-08-01', '2023-08-01', 'TANAH ABANG', '2020-04-19 10:00:00', '2020-07-18 10:18:26');


-- --------------------------------------------------------

-- Struktur dari tabel `jadwal_pemakaman`
--

CREATE TABLE IF NOT EXISTS `jadwal_pemakaman` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `iptm_id` int(10) UNSIGNED DEFAULT NULL,
  `tanggal_pemakaman` date DEFAULT NULL,
  `jam_pemakaman` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jadwal_pemakaman_iptm_id_foreign` (`iptm_id`),
  CONSTRAINT `jadwal_pemakaman_iptm_id_foreign` FOREIGN KEY (`iptm_id`) REFERENCES `iptm` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2020_01_23_000001_create_pemakaman_table', 1),
(3, '2020_01_23_000002_create_users_table', 1),
(4, '2020_01_23_000003_ahli_waris_table', 1),
(5, '2020_01_23_000004_makam_table', 1),
(6, '2020_01_23_000005_iptm_table', 1),
(7, '2020_01_23_000006_almarhum_table', 1),
(8, '2020_01_23_000007_peraturan_table', 1),
(9, '2020_01_23_000007_perpanjangan__table', 1),
(10, '2020_01_23_000008_registrasi__table', 1),
(11, '2020_01_23_000009_pemindahan_table', 1),
(12, '2020_01_23_000011_create_tumpangan_table', 1),
(13, '2020_06_28_043346_create_jadwal_pemakaman_table', 2),
(15, '2020_06_29_122215_add_columns_to_table_tumpangan', 3);

-- --------------------------------------------------------

CREATE TABLE `nomorpemesanan` (
  `id` int(10) UNSIGNED NOT NULL,
  `iptm_id` int(10) UNSIGNED NOT NULL,
  `registrasi_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registrasi_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

-- ---------------------------------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS `almarhum` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ahli_waris_id` int(10) UNSIGNED NOT NULL,
  `iptm_id` int(10) UNSIGNED DEFAULT NULL,
  `nama_almarhum` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_wafat` date DEFAULT NULL,
  `nomor_ktp_almarhum` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_ktp_almarhum` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_kk_almarhum` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_kk_almarhum` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_sp_rtrw` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_sp_rtrw` date DEFAULT NULL,
  `file_sp_rtrw` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_sk_kematian_rs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_sk_kematian_rs` date DEFAULT NULL,
  `file_sk_kematian_rs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_sk_kematian_kelurahan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_sk_kematian_kelurahan` date DEFAULT NULL,
  `file_sk_kematian_kelurahan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `almarhum_ahli_waris_id_foreign` (`ahli_waris_id`),
  KEY `almarhum_iptm_id_foreign` (`iptm_id`),
  CONSTRAINT `almarhum_ahli_waris_id_foreign` FOREIGN KEY (`ahli_waris_id`) REFERENCES `ahli_waris` (`id`),
  CONSTRAINT `almarhum_iptm_id_foreign` FOREIGN KEY (`iptm_id`) REFERENCES `iptm` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `almarhum`
--

INSERT INTO `almarhum` (`id`, `ahli_waris_id`, `iptm_id`, `nama_almarhum`, `tanggal_wafat`, `nomor_ktp_almarhum`, `file_ktp_almarhum`, `nomor_kk_almarhum`, `file_kk_almarhum`, `nomor_sp_rtrw`, `tanggal_sp_rtrw`, `file_sp_rtrw`, `nomor_sk_kematian_rs`, `tanggal_sk_kematian_rs`, `file_sk_kematian_rs`, `nomor_sk_kematian_kelurahan`, `tanggal_sk_kematian_kelurahan`, `file_sk_kematian_kelurahan`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Almarhum 1', '2017-07-17', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2017', '2017-07-19', '', '001/SK/RS/2017', '2017-07-18', '', '001/SK/KLR/2017', '2017-07-19', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(2, 2, 2, 'Almarhum 2', '2017-07-17', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2017', '2017-07-19', '', '001/SK/RS/2017', '2017-07-18', '', '001/SK/KLR/2017', '2017-07-19', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(3, 3, 3, 'Almarhum 3', '2017-07-17', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2017', '2017-07-19', '', '001/SK/RS/2017', '2017-07-18', '', '001/SK/KLR/2017', '2017-07-19', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(4, 4, 4, 'Almarhum 4', '2017-07-17', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2017', '2017-07-19', '', '001/SK/RS/2017', '2017-07-18', '', '001/SK/KLR/2017', '2017-07-19', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(5, 5, 5, 'Almarhum 5', '2017-07-17', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2017', '2017-07-19', '', '001/SK/RS/2017', '2017-07-18', '', '001/SK/KLR/2017', '2017-07-19', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(6, 6, 6, 'Almarhum 6', '2017-07-17', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2017', '2017-07-19', '', '001/SK/RS/2017', '2017-07-18', '', '001/SK/KLR/2017', '2017-07-19', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(7, 7, 7, 'Almarhum 7', '2017-07-17', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2017', '2017-07-19', '', '001/SK/RS/2017', '2017-07-18', '', '001/SK/KLR/2017', '2017-07-19', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(8, 8, 8, 'Almarhum 8', '2017-07-17', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2017', '2017-07-19', '', '001/SK/RS/2017', '2017-07-18', '', '001/SK/KLR/2017', '2017-07-19', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(9, 9, 9, 'Almarhum 9', '2017-07-17', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2017', '2017-07-19', '', '001/SK/RS/2017', '2017-07-18', '', '001/SK/KLR/2017', '2017-07-19', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(10, 10, 10, 'Almarhum 10', '2017-07-17', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2017', '2017-07-19', '', '001/SK/RS/2017', '2017-07-18', '', '001/SK/KLR/2017', '2017-07-19', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(11, 11, 11, 'Almarhum 11', '2014-08-01', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2014', '2014-08-02', '', '001/SK/RS/2014', '2014-08-02', '', '001/SK/KLR/2014', '2014-08-02', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(12, 12, 12, 'Almarhum 12', '2014-08-01', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2014', '2014-08-02', '', '001/SK/RS/2014', '2014-08-02', '', '001/SK/KLR/2014', '2014-08-02', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(13, 13, 13, 'Almarhum 13', '2014-08-01', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2014', '2014-08-02', '', '001/SK/RS/2014', '2014-08-02', '', '001/SK/KLR/2014', '2014-08-02', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(14, 14, 14, 'Almarhum 14', '2014-08-01', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2014', '2014-08-02', '', '001/SK/RS/2014', '2014-08-02', '', '001/SK/KLR/2014', '2014-08-02', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(15, 15, 15, 'Almarhum 15', '2014-08-01', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2014', '2014-08-02', '', '001/SK/RS/2014', '2014-08-02', '', '001/SK/KLR/2014', '2014-08-02', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(16, 16, 16, 'Almarhum 16', '2014-08-01', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2014', '2014-08-02', '', '001/SK/RS/2014', '2014-08-02', '', '001/SK/KLR/2014', '2014-08-02', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(17, 17, 17, 'Almarhum 17', '2014-08-01', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2014', '2014-08-02', '', '001/SK/RS/2014', '2014-08-02', '', '001/SK/KLR/2014', '2014-08-02', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(18, 18, 18, 'Almarhum 18', '2014-08-01', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2014', '2014-08-02', '', '001/SK/RS/2014', '2014-08-02', '', '001/SK/KLR/2014', '2014-08-02', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(19, 19, 19, 'Almarhum 19', '2014-08-01', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2014', '2014-08-02', '', '001/SK/RS/2014', '2014-08-02', '', '001/SK/KLR/2014', '2014-08-02', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(20, 20, 20, 'Almarhum 20', '2014-08-01', '3333000011112222', '', '3333000011112233', '', '001/SP/RT/2014', '2014-08-02', '', '001/SK/RS/2014', '2014-08-02', '', '001/SK/KLR/2014', '2014-08-02', '', '2020-04-19 10:00:00', '2020-04-19 10:00:00'),
(21, 22, NULL, 'Almarhum Baru 1', '2020-08-13', '3333000011112222', 'Icon KTP.jpg', '3172031805160020', 'Icon KTP.jpg', NULL, NULL, NULL, '001/SK/RS/2020', '2020-08-13', 'Icon Surat.png', '001/SK/KLR/2020', '2020-08-13', 'Icon Surat.png', '2020-08-12 23:02:53', '2020-08-12 23:02:53'),
(22, 23, NULL, 'SITI ALPIAH', '2020-08-13', '3172055104121000', NULL, '3172031805160020', NULL, NULL, NULL, NULL, '001/SK/RS/2020', NULL, NULL, '001/SK/KLR/2020', NULL, NULL, '2020-08-12 23:29:27', '2020-08-12 23:29:27'),
(23, 25, NULL, 'SITI ALPIAH', '2020-08-13', '3172055104121000', NULL, '3172031805160020', NULL, NULL, NULL, NULL, '001/SK/RS/2020', NULL, NULL, '001/SK/KLR/2020', NULL, NULL, '2020-08-12 23:40:28', '2020-08-12 23:40:28');

-- ---------------------------------------------------------------------------------------------------------------------

-- ---------------------------------------------------------------------------------------------------------------------
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pemakaman_id` int(10) UNSIGNED DEFAULT NULL,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_pemakaman_id_foreign` (`pemakaman_id`),
  CONSTRAINT `users_pemakaman_id_foreign` FOREIGN KEY (`pemakaman_id`) REFERENCES `pemakaman` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `pemakaman_id`, `fullname`, `email`, `password`, `address`, `gender`, `images`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 'dinas', 'admindinas@apps.com', '$2y$10$G9Ls/8FPoLkd4.YnAT0Kpuisyaj0Q/9JCQgGLGuUPgRRmIjO8QvvW', 'jl taman harapan baru', 'male', NULL, 'admin_dinas', 'PF5q83xyvYPXqVNYJMLeubgMIxLBhyeXEn72FGD33KAwTcVlxZYGjZcPuXFr', NULL, NULL),
(2, 1, 'yudha', 'yudha@apps.com', '$2y$10$SDAIjeeXDL5b85u1L0kpOeXqx0YFpbaVD3QJmZrfoAIj/9RHv7biC', 'jl taman harapan baru33', 'male', NULL, 'admin_tpu', 'ah8SCD7NE0pmmrQslywU3PEQQND3oYpgPCNzsBlDn31PUgB3i3DkzIJtwaPg', NULL, '2020-07-08 22:32:29'),
(3, NULL, 'Ilham Ridho A', 'ridho@email.com', '$2y$10$sY.ADpUBXaXAsbhjfIoUZeitam.gFFXLgmlkRQ6XHEEqpTen4SYOC', 'Jln. H Rijin Depok', 'male', NULL, 'member', 'cRuB9g28d2foTA1XiH30kutkQQN5ZJ7LaBOiZUNNHmaC1JvQBeaMVqfW912b', NULL, NULL),
(4, NULL, 'Member 1', 'member1@email.com', '$2y$10$K/qE3YHz4CltEPvHbLOo6uirsWExKw2ovoOeQ02Zeq0GaUbSl9acW', 'Jl Test', 'male', NULL, 'member', 'thrNl5ewfAGo30DpZrZ46w6U5Yo57lihlBKT4VRSoD54eIwJL5CRqjYZn65j', '2020-08-12 22:47:46', '2020-08-12 22:47:46'),
(5, NULL, 'Member 2', 'member2@email.com', '$2y$10$K/qE3YHz4CltEPvHbLOo6uirsWExKw2ovoOeQ02Zeq0GaUbSl9acW', 'Jl Test', 'male', NULL, 'member', 'jd99PPhX9wSn3c6wPcR3nNNq3lHJ8PmzhTQaXWyN3nB3vSeKjA4qQl5s3tXM', '2020-08-12 22:47:46', '2020-08-12 22:47:46'),
(6, NULL, 'Member 3', 'member3@email.com', '$2y$10$K/qE3YHz4CltEPvHbLOo6uirsWExKw2ovoOeQ02Zeq0GaUbSl9acW', 'Jl Test', 'male', NULL, 'member', NULL, '2020-08-12 22:47:46', '2020-08-12 22:47:46'),
(7, NULL, 'Member 4', 'member4@email.com', '$2y$10$K/qE3YHz4CltEPvHbLOo6uirsWExKw2ovoOeQ02Zeq0GaUbSl9acW', 'Jl Test', 'male', NULL, 'member', NULL, '2020-08-12 22:47:46', '2020-08-12 22:47:46'),
(8, NULL, 'Member 5', 'member5@email.com', '$2y$10$K/qE3YHz4CltEPvHbLOo6uirsWExKw2ovoOeQ02Zeq0GaUbSl9acW', 'Jl Test', 'male', NULL, 'member', NULL, '2020-08-12 22:47:46', '2020-08-12 22:47:46'),
(9, NULL, 'Member 6', 'member6@email.com', '$2y$10$K/qE3YHz4CltEPvHbLOo6uirsWExKw2ovoOeQ02Zeq0GaUbSl9acW', 'Jl Test', 'male', NULL, 'member', NULL, '2020-08-12 22:47:46', '2020-08-12 22:47:46'),
(10, NULL, 'Member 7', 'member7@email.com', '$2y$10$K/qE3YHz4CltEPvHbLOo6uirsWExKw2ovoOeQ02Zeq0GaUbSl9acW', 'Jl Test', 'male', NULL, 'member', NULL, '2020-08-12 22:47:46', '2020-08-12 22:47:46'),
(11, NULL, 'Member 8', 'member8@email.com', '$2y$10$K/qE3YHz4CltEPvHbLOo6uirsWExKw2ovoOeQ02Zeq0GaUbSl9acW', 'Jl Test', 'male', NULL, 'member', NULL, '2020-08-12 22:47:46', '2020-08-12 22:47:46'),
(12, NULL, 'Member 9', 'member9@email.com', '$2y$10$K/qE3YHz4CltEPvHbLOo6uirsWExKw2ovoOeQ02Zeq0GaUbSl9acW', 'Jl Test', 'male', NULL, 'member', NULL, '2020-08-12 22:47:46', '2020-08-12 22:47:46'),
(13, NULL, 'Member 10', 'member10@email.com', '$2y$10$K/qE3YHz4CltEPvHbLOo6uirsWExKw2ovoOeQ02Zeq0GaUbSl9acW', 'Jl Test', 'male', NULL, 'member', NULL, '2020-08-12 22:47:46', '2020-08-12 22:47:46');

-- ---------------------------------------------------------------------------------------------------------------------
-- Struktur dari tabel `perpanjangan`


CREATE TABLE IF NOT EXISTS `perpanjangan` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `iptm_id` int(10) UNSIGNED NOT NULL,
  `almarhum_id` int(10) UNSIGNED DEFAULT NULL,
  `ahli_waris_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `nomor_permohonan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_permohonan` date DEFAULT NULL,
  `nomor_surat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_surat` date DEFAULT NULL,
  `nomor_sk_kehilangan_kepolisian` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_sk_kehilangan_kepolisian` date DEFAULT NULL,
  `file_sk_kehilangan_kepolisian` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `daftar_jenazah` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_berlaku_dari` date DEFAULT NULL,
  `tanggal_berlaku_sampai` date DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `perpanjangan_ke` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cetak_oleh` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_iptm_asli` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_iptm` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `perpanjangan_iptm_id_foreign` (`iptm_id`),
  KEY `perpanjangan_almarhum_id_foreign` (`almarhum_id`),
  KEY `perpanjangan_ahli_waris_id_foreign` (`ahli_waris_id`),
  KEY `perpanjangan_user_id_foreign` (`user_id`),
  CONSTRAINT `perpanjangan_iptm_id_foreign` FOREIGN KEY (`iptm_id`) REFERENCES `iptm` (`id`),
  CONSTRAINT `perpanjangan_almarhum_id_foreign` FOREIGN KEY (`almarhum_id`) REFERENCES `almarhum` (`id`),
  CONSTRAINT `perpanjangan_ahli_waris_id_foreign` FOREIGN KEY (`ahli_waris_id`) REFERENCES `ahli_waris` (`id`),
  CONSTRAINT `perpanjangan_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `perpanjangan` (`id`, `iptm_id`, `almarhum_id`, `ahli_waris_id`, `user_id`, `nomor_permohonan`, `tanggal_permohonan`, `nomor_surat`, `tanggal_surat`, `nomor_sk_kehilangan_kepolisian`, `tanggal_sk_kehilangan_kepolisian`, `file_sk_kehilangan_kepolisian`, `daftar_jenazah`, `tanggal_berlaku_dari`, `tanggal_berlaku_sampai`, `status`, `perpanjangan_ke`, `cetak_oleh`, `file_iptm_asli`, `file_iptm`, `catatan`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 21, 4, 'PI-001', '2020-08-13', '001/BIVAK/PI/VII/2020', '2020-08-13', NULL, NULL, NULL, NULL, '2020-08-14', '2020-08-21', 2, '1', NULL, 'Icon Surat.png', '001-BIVAK-PI-VII-2020_1597301159.pdf', NULL, '2020-08-12 22:58:17', '2020-08-12 23:46:00'),
(2, 2, 2, 21, 5, 'PI-002', '2020-08-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, '1', NULL, 'Icon Surat.png', NULL, 'Tes Reject', '2020-08-12 22:58:17', '2020-08-12 23:52:14'),
(3, 3, 3, 21, 6, 'PI-003', '2020-08-13', '001/BIVAK/PI/VII/2020', '2020-08-13', NULL, NULL, NULL, NULL, '2020-08-13', '2020-08-31', 2, '1', NULL, 'Icon Surat.png', '001-BIVAK-PI-VII-2020_1597301190.pdf', NULL, '2020-08-12 22:58:17', '2020-08-12 23:46:30'),
(4, 4, 4, 21, 7, 'PI-004', '2020-08-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '1', NULL, 'Icon Surat.png', NULL, NULL, '2020-08-12 22:58:17', '2020-08-12 22:58:17'),
(5, 5, 5, 21, 8, 'PI-005', '2020-08-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '1', NULL, 'Icon Surat.png', NULL, NULL, '2020-08-12 22:58:17', '2020-08-12 22:58:17'),
(6, 6, 6, 6, 9, 'PI-006', '2020-07-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '1', NULL, 'Icon Surat.png', NULL, NULL, '2020-08-12 22:58:17', '2020-08-12 22:58:17'),
(7, 7, 7, 7, 10, 'PI-007', '2020-07-01', '001/BIVAK/PI/VII/2020', '2020-08-14', NULL, NULL, NULL, 'SITI ALPIAH/PUTRA/PUTRI/', '2020-08-13', '2020-08-31', 2, '1', NULL, 'Icon Surat.png', '001-BIVAK-PI-VII-2020_1597301478.pdf', NULL, '2020-08-12 22:58:17', '2020-08-12 23:51:18'),
(8, 8, 8, 8, 11, 'PI-008', '2020-07-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '1', NULL, 'Icon Surat.png', NULL, NULL, '2020-08-12 22:58:17', '2020-08-12 22:58:17'),
(9, 9, 9, 9, 12, 'PI-009', '2020-06-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '1', NULL, 'Icon Surat.png', NULL, NULL, '2020-08-12 22:58:17', '2020-08-12 22:58:17'),
(10, 10, 10, 10, 13, 'PI-010', '2020-06-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, '1', NULL, 'Icon Surat.png', NULL, 'Data Almarhum Perlu diperbaiki', '2020-08-12 22:58:17', '2020-08-12 23:50:19'),
(11, 1, 1, 24, 2, NULL, '2020-08-13', '001/BIVAK/PI/VII/2020', '2020-08-13', NULL, NULL, NULL, 'SITI ALPIAH/PUTRA/PUTRI', '2020-08-13', '2020-08-31', 2, '1', 'yudha', NULL, '001-BIVAK-PI-VII-2020_1597300518.pdf', NULL, '2020-08-12 23:35:18', '2020-08-12 23:35:19');

-- --------------------------------------------------------------------------------------------------------------------

-- Struktur dari tabel `tumpangan`
--

CREATE TABLE IF NOT EXISTS `tumpangan` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `iptm_id` int(10) UNSIGNED NOT NULL,
  `almarhum_id` int(10) UNSIGNED DEFAULT NULL,
  `ahli_waris_id` int(10) UNSIGNED NOT NULL,
  `nomor_permohonan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_permohonan` date DEFAULT NULL,
  `nomor_surat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_surat` date DEFAULT NULL,
  `nomor_sk_kehilangan_kepolisian` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_sk_kehilangan_kepolisian` date DEFAULT NULL,
  `daftar_jenazah` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_berlaku_dari` date DEFAULT NULL,
  `tanggal_berlaku_sampai` date DEFAULT NULL,
  `file_sk_kehilangan_kepolisian` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_iptm_asli` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_iptm` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `cetak_oleh` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tumpangan_iptm_id_foreign` (`iptm_id`),
  KEY `tumpangan_almarhum_id_foreign` (`almarhum_id`),
  KEY `tumpangan_ahli_waris_id_foreign` (`ahli_waris_id`),
  KEY `perpanjangan_user_id_foreign` (`user_id`),
  CONSTRAINT `tumpangan_iptm_id_foreign` FOREIGN KEY (`iptm_id`) REFERENCES `iptm` (`id`),
  CONSTRAINT `tumpangan_almarhum_id_foreign` FOREIGN KEY (`almarhum_id`) REFERENCES `almarhum` (`id`),
  CONSTRAINT `tumpangan_ahli_waris_id_foreign` FOREIGN KEY (`ahli_waris_id`) REFERENCES `ahli_waris` (`id`),
  CONSTRAINT `tumpangan_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `tumpangan` (`id`, `user_id`, `iptm_id`, `almarhum_id`, `ahli_waris_id`, `nomor_permohonan`, `tanggal_permohonan`, `nomor_surat`, `tanggal_surat`, `nomor_sk_kehilangan_kepolisian`, `tanggal_sk_kehilangan_kepolisian`, `daftar_jenazah`, `tanggal_berlaku_dari`, `tanggal_berlaku_sampai`, `file_sk_kehilangan_kepolisian`, `file_iptm_asli`, `file_iptm`, `status`, `cetak_oleh`, `catatan`, `created_at`, `updated_at`) VALUES
(1, 4, 11, 21, 22, 'TP-001', '2020-06-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Icon Surat.png', NULL, 1, NULL, NULL, '2020-08-12 23:02:53', '2020-08-12 23:02:54'),
(2, 5, 12, 2, 22, 'TP-002', '2020-07-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Icon Surat.png', NULL, 1, NULL, NULL, '2020-08-12 23:02:53', '2020-08-12 23:02:54'),
(3, 6, 13, 3, 3, 'TP-003', '2020-06-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Icon Surat.png', NULL, 1, NULL, NULL, '2020-08-12 23:02:53', '2020-08-12 23:02:54'),
(4, 7, 14, 4, 4, 'TP-004', '2020-06-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Icon Surat.png', NULL, 1, NULL, NULL, '2020-08-12 23:02:53', '2020-08-12 23:02:54'),
(5, 8, 15, 5, 5, 'TP-005', '2020-08-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Icon Surat.png', NULL, 1, NULL, NULL, '2020-08-12 23:02:53', '2020-08-12 23:02:54'),
(6, 9, 16, 6, 6, 'TP-006', '2020-08-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Icon Surat.png', NULL, 1, NULL, NULL, '2020-08-12 23:02:53', '2020-08-12 23:02:54'),
(7, 2, 1, 22, 23, NULL, NULL, '001/BIVAK/PI/VII/2020', '2020-08-13', NULL, NULL, NULL, '2020-08-13', '2020-08-13', NULL, NULL, '001-BIVAK-PI-VII-2020_1597300169.pdf', 2, 'yudha', NULL, '2020-08-12 23:29:27', '2020-08-12 23:29:29'),
(8, 2, 2, 23, 25, NULL, NULL, '002/BIVAK/TP/VII/2020', '2020-08-13', NULL, NULL, 'SITI ALPIAH/PUTRA/PUTRI', '2020-08-13', '2020-08-31', NULL, NULL, '002-BIVAK-TP-VII-2020_1597300829.pdf', 2, 'yudha', NULL, '2020-08-12 23:40:29', '2020-08-12 23:40:29'),
(9, 10, 17, 7, 7, 'TP-007', '2020-08-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Icon Surat.png', NULL, 1, NULL, NULL, '2020-08-12 23:02:53', '2020-08-12 23:02:54'),
(10, 11, 18, 8, 8, 'TP-008', '2020-08-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Icon Surat.png', NULL, 1, NULL, NULL, '2020-08-12 23:02:53', '2020-08-12 23:02:54'),
(11, 12, 19, 9, 9, 'TP-009', '2020-08-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Icon Surat.png', NULL, 1, NULL, NULL, '2020-08-12 23:02:53', '2020-08-12 23:02:54'),
(12, 13, 20, 10, 10, 'TP-010', '2020-08-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Icon Surat.png', NULL, 1, NULL, NULL, '2020-08-12 23:02:53', '2020-08-12 23:02:54');

-- ---------------------------------------------------------------------------------------------------------------------

