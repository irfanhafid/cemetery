-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 19 Jul 2020 pada 02.23
-- Versi Server: 10.1.29-MariaDB
-- PHP Version: 7.1.12

/*SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";*/


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- -------------------------------------------------------
DROP TABLE IF EXISTS `perpanjangan`;
DROP TABLE IF EXISTS `tumpangan`;
DROP TABLE IF EXISTS `almarhum`;
DROP TABLE IF EXISTS `ahli_waris`;
DROP TABLE IF EXISTS `jadwal_pemakaman`;
DROP TABLE IF EXISTS `nomorpemesanan`;
DROP TABLE IF EXISTS `iptm`;
DROP TABLE IF EXISTS `makam`;
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `peraturan`;
DROP TABLE IF EXISTS `pemakaman`;
DROP TABLE IF EXISTS `migrations`;
DROP TABLE IF EXISTS `password_resets`;
-- --------------------------------------------------------


CREATE TABLE IF NOT EXISTS `ahli_waris` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nomor_ktp_ahliwaris` bigint(20) DEFAULT NULL,
  `file_ktp_ahliwaris` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_ahliwaris` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat_ahliwaris` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rt_ahliwaris` int(11) DEFAULT NULL,
  `rw_ahliwaris` int(11) DEFAULT NULL,
  `kelurahan_ahliwaris` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kecamatan_ahliwaris` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kota_ahliwaris` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telepon_ahliwaris` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hubungan_ahliwaris` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------------------------------------------------
-- Struktur dari tabel `pemakaman`

CREATE TABLE IF NOT EXISTS `pemakaman` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kelurahan_pemakaman` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kecamatan_pemakaman` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kota_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provinsi_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telepon_pemakaman` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kodepos_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jumlah_makam` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `luas_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deskripsi_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kepala_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nip_kepala_pemakaman` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pemakaman`
--

INSERT INTO `pemakaman` (`id`, `nama_pemakaman`, `alamat_pemakaman`, `kelurahan_pemakaman`, `kecamatan_pemakaman`, `kota_pemakaman`, `provinsi_pemakaman`, `telepon_pemakaman`, `kodepos_pemakaman`, `email_pemakaman`, `jumlah_makam`, `luas_pemakaman`, `deskripsi_pemakaman`, `photo_pemakaman`, `kepala_pemakaman`, `nip_kepala_pemakaman`, `created_at`, `updated_at`) VALUES
(1, 'Karet Bivak', 'Jl. KH. Mas Mansyur Raya', 'Karet Tengsin', 'Tanah Abang', 'Jakarta', 'DKI Jakarta', '(021)5734785', '10250', 'tpubivak@gmail.com', 'Kira-kira 48,000', 'Kira-kira 16,2 hektare', 'TPU BIVAK tidak melayani pengajuan izin penggunaan tanah makam baru', 'ttd.png', 'Saiman S.Ag.S.AP', '197609032008011027', '2020-07-17 10:00:00', '2020-08-03 21:03:08');

-- ----------------------------------------------------------------------

-- ----------------------------------------------------------------------
-- Struktur dari tabel `makam`

CREATE TABLE IF NOT EXISTS `makam` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pemakaman_id` int(10) UNSIGNED NOT NULL,
  `blok` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blad` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `petak` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `makam_pemakaman_id_foreign` (`pemakaman_id`),
  CONSTRAINT `makam_pemakaman_id_foreign` FOREIGN KEY (`pemakaman_id`) REFERENCES `pemakaman` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `iptm` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `makam_id` int(10) UNSIGNED NOT NULL,
  `nomor_iptm` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_iptm` date DEFAULT NULL,
  `masa_berlaku` date DEFAULT NULL,
  `kelurahan_ptsp` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iptm_makam_id_foreign` (`makam_id`),
  CONSTRAINT `iptm_makam_id_foreign` FOREIGN KEY (`makam_id`) REFERENCES `makam` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Struktur dari tabel `jadwal_pemakaman`
--

CREATE TABLE IF NOT EXISTS `jadwal_pemakaman` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `iptm_id` int(10) UNSIGNED DEFAULT NULL,
  `tanggal_pemakaman` date DEFAULT NULL,
  `jam_pemakaman` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jadwal_pemakaman_iptm_id_foreign` (`iptm_id`),
  CONSTRAINT `jadwal_pemakaman_iptm_id_foreign` FOREIGN KEY (`iptm_id`) REFERENCES `iptm` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2020_01_23_000001_create_pemakaman_table', 1),
(3, '2020_01_23_000002_create_users_table', 1),
(4, '2020_01_23_000003_ahli_waris_table', 1),
(5, '2020_01_23_000004_makam_table', 1),
(6, '2020_01_23_000005_iptm_table', 1),
(7, '2020_01_23_000006_almarhum_table', 1),
(8, '2020_01_23_000007_peraturan_table', 1),
(9, '2020_01_23_000007_perpanjangan__table', 1),
(10, '2020_01_23_000008_registrasi__table', 1),
(11, '2020_01_23_000009_pemindahan_table', 1),
(12, '2020_01_23_000011_create_tumpangan_table', 1),
(13, '2020_06_28_043346_create_jadwal_pemakaman_table', 2),
(15, '2020_06_29_122215_add_columns_to_table_tumpangan', 3);

-- --------------------------------------------------------

CREATE TABLE `nomorpemesanan` (
  `id` int(10) UNSIGNED NOT NULL,
  `iptm_id` int(10) UNSIGNED NOT NULL,
  `registrasi_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registrasi_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

-- ---------------------------------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS `almarhum` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ahli_waris_id` int(10) UNSIGNED NOT NULL,
  `iptm_id` int(10) UNSIGNED DEFAULT NULL,
  `nama_almarhum` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_wafat` date DEFAULT NULL,
  `nomor_ktp_almarhum` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_ktp_almarhum` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_kk_almarhum` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_kk_almarhum` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_sp_rtrw` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_sp_rtrw` date DEFAULT NULL,
  `file_sp_rtrw` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_sk_kematian_rs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_sk_kematian_rs` date DEFAULT NULL,
  `file_sk_kematian_rs` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nomor_sk_kematian_kelurahan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_sk_kematian_kelurahan` date DEFAULT NULL,
  `file_sk_kematian_kelurahan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `almarhum_ahli_waris_id_foreign` (`ahli_waris_id`),
  KEY `almarhum_iptm_id_foreign` (`iptm_id`),
  CONSTRAINT `almarhum_ahli_waris_id_foreign` FOREIGN KEY (`ahli_waris_id`) REFERENCES `ahli_waris` (`id`),
  CONSTRAINT `almarhum_iptm_id_foreign` FOREIGN KEY (`iptm_id`) REFERENCES `iptm` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ---------------------------------------------------------------------------------------------------------------------
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pemakaman_id` int(10) UNSIGNED DEFAULT NULL,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_pemakaman_id_foreign` (`pemakaman_id`),
  CONSTRAINT `users_pemakaman_id_foreign` FOREIGN KEY (`pemakaman_id`) REFERENCES `pemakaman` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `pemakaman_id`, `fullname`, `email`, `password`, `address`, `gender`, `images`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Admin Sistem', 'adminsistem@email.com', '$2y$10$G9Ls/8FPoLkd4.YnAT0Kpuisyaj0Q/9JCQgGLGuUPgRRmIjO8QvvW', '', 'male', NULL, 'admin_dinas', 'YKS8mgLuZ3uHji9VSdMdLCs6Ssd3HsCH7Fl0dSfM8XCq4O754XiT9NKmNqk9', NULL, NULL),
(2, 1, 'Admin TPU', 'admintpu@email.com', '$2y$10$SDAIjeeXDL5b85u1L0kpOeXqx0YFpbaVD3QJmZrfoAIj/9RHv7biC', '', 'male', NULL, 'admin_tpu', 'uPxJAOMGFDbiPKGloSTzXyBNbYxmoSAgKhkz0JSEZweXxPP9jM0ul1B2Q3m5', NULL, '2020-07-08 15:32:29'),
(3, NULL, 'Member', 'member@email.com', '$2y$10$sY.ADpUBXaXAsbhjfIoUZeitam.gFFXLgmlkRQ6XHEEqpTen4SYOC', '', 'male', NULL, 'member', 'tmDRtzDkp69VdHNvgmL6p7EzNEjlhm0hYz6u8rUShm7oNKsooI8MELqxZYc7', NULL, NULL);

-- ---------------------------------------------------------------------------------------------------------------------
-- Struktur dari tabel `perpanjangan`


CREATE TABLE IF NOT EXISTS `perpanjangan` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `iptm_id` int(10) UNSIGNED NOT NULL,
  `almarhum_id` int(10) UNSIGNED DEFAULT NULL,
  `ahli_waris_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `nomor_permohonan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_permohonan` date DEFAULT NULL,
  `nomor_surat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_surat` date DEFAULT NULL,
  `nomor_sk_kehilangan_kepolisian` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_sk_kehilangan_kepolisian` date DEFAULT NULL,
  `file_sk_kehilangan_kepolisian` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `daftar_jenazah` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_berlaku_dari` date DEFAULT NULL,
  `tanggal_berlaku_sampai` date DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `perpanjangan_ke` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cetak_oleh` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_iptm_asli` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_iptm` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `perpanjangan_iptm_id_foreign` (`iptm_id`),
  KEY `perpanjangan_almarhum_id_foreign` (`almarhum_id`),
  KEY `perpanjangan_ahli_waris_id_foreign` (`ahli_waris_id`),
  KEY `perpanjangan_user_id_foreign` (`user_id`),
  CONSTRAINT `perpanjangan_iptm_id_foreign` FOREIGN KEY (`iptm_id`) REFERENCES `iptm` (`id`),
  CONSTRAINT `perpanjangan_almarhum_id_foreign` FOREIGN KEY (`almarhum_id`) REFERENCES `almarhum` (`id`),
  CONSTRAINT `perpanjangan_ahli_waris_id_foreign` FOREIGN KEY (`ahli_waris_id`) REFERENCES `ahli_waris` (`id`),
  CONSTRAINT `perpanjangan_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Struktur dari tabel `tumpangan`
--

CREATE TABLE IF NOT EXISTS `tumpangan` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `iptm_id` int(10) UNSIGNED NOT NULL,
  `almarhum_id` int(10) UNSIGNED DEFAULT NULL,
  `ahli_waris_id` int(10) UNSIGNED NOT NULL,
  `nomor_permohonan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_permohonan` date DEFAULT NULL,
  `nomor_surat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_surat` date DEFAULT NULL,
  `nomor_sk_kehilangan_kepolisian` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_sk_kehilangan_kepolisian` date DEFAULT NULL,
  `daftar_jenazah` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_berlaku_dari` date DEFAULT NULL,
  `tanggal_berlaku_sampai` date DEFAULT NULL,
  `file_sk_kehilangan_kepolisian` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_iptm_asli` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_iptm` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `cetak_oleh` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tumpangan_iptm_id_foreign` (`iptm_id`),
  KEY `tumpangan_almarhum_id_foreign` (`almarhum_id`),
  KEY `tumpangan_ahli_waris_id_foreign` (`ahli_waris_id`),
  KEY `perpanjangan_user_id_foreign` (`user_id`),
  CONSTRAINT `tumpangan_iptm_id_foreign` FOREIGN KEY (`iptm_id`) REFERENCES `iptm` (`id`),
  CONSTRAINT `tumpangan_almarhum_id_foreign` FOREIGN KEY (`almarhum_id`) REFERENCES `almarhum` (`id`),
  CONSTRAINT `tumpangan_ahli_waris_id_foreign` FOREIGN KEY (`ahli_waris_id`) REFERENCES `ahli_waris` (`id`),
  CONSTRAINT `tumpangan_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

